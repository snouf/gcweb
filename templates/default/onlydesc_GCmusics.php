<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page qui affiche la liste des éléments de la collection
 *   - Les variables du tableau $info, $collec et $items peuvent être utilisées.
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="onlydesc">

       <div class="element">
            <div class="box">
                <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menuOptionsAff.php'; ?>

                <table>
                    <tr class='title'>
                        <th>
                            <?php echo __('Artiste') ?>
                            <a <?php if (isSortKey('artistASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('artistASC') ?>">↓</a><a <?php if (isSortKey('artistDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('artistDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Titre') ?>
                            <a <?php if (isSortKey('titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleASC') ?>">↓</a><a <?php if (isSortKey('titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Sortie') ?>
                            <a <?php if (isSortKey('releaseASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('releaseASC') ?>">↓</a><a <?php if (isSortKey('releaseDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('releaseDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Pistes') ?>
                        </th>
                    </tr>
                    <?php
                    $paire = False;
                    foreach ($bdd as $item) {
                        if ($paire)     {   $paire=False;   $class='paire';     }
                        else            {   $paire=True;    $class='impaire';   }
                        ?>
                        <tr id="id_<?php aff($item['id']) ?>" class="line_<?php echo $class ?>">
                            <th>
                                <a href="<?php aff_hrefitem($item) ?>" onmouseover="javascript:load_img('<?php echo URL_GCWEB.'index.php'; aff_image($item['cover'],160,120) ?>', 'parent_image_<?php aff($item['id'])?>'); show('parent_image_<?php aff($item['id'])?>')" onmouseout="javascript:hide('parent_image_<?php aff($item['id'])?>');">
                                    <?php aff($item['artist']) ?></a>

                                <span id="parent_image_<?php aff($item['id']) ?>" class="image"></span>
                            </th>
                            <td><?php aff_filter('title==',$item['title'],'',', ','','onlydesc') ?></td>
                            <td><?php aff_filter('release==',$item['release'],'',', ','','onlydesc') ?></td>
                            <td>
                                <?php
                                $tracks = array();
                                foreach ($item['tracks'] as $track)
                                    $tracks[] = $track[1]; //Ne prend que le titre de la piste
                                aff($tracks);
                                ?>
                            </td>
                        </tr>
                    <?php }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>