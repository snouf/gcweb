<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Partie se trouvant en haut de chaque page
 *   - Seul les variables du tableau $info peuvent être utilisées
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
    <title><?php aff($info['title']) ?></title>
    <meta http-equiv="Content-Type" content="application/x-php;charset=UTF-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <link rel="icon" type="image/png" href="<?php echo URL_GCWEB ?>templates/<?php echo $conf['template'] ?>/img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_GCWEB ?>templates/<?php echo $conf['template'] ?>/style.css" media="screen, print" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_GCWEB ?>templates/<?php echo $conf['template'] ?>/style_print.css" media="print" />
    <!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="<?php echo URL_GCWEB ?>templates/<?php echo $conf['template'] ?>/style_ie.css" media="screen, print" /><![endif]-->
    <?php if ($modelname == 'main') {
        foreach ($info['collections'] as $collectmp ) {
            if (!$collectmp['private'])
                echo '     <link rel="alternate" type="application/rss+xml" title="'.$info['title'].' - '.$collectmp['title'].' (Toute la collection)" href="'.hrefmodel('rss').'" />';
        }
    } else { ?>
        <link rel="alternate" type="application/rss+xml" title="<?php echo $info['title'].' - '.$collec['title'] ?> (Toute la collection)" href="<? aff_hrefmodel('rss') ?>" />
        <link rel="alternate" type="application/rss+xml" title="<?php echo $info['title'].' - '.$collec['title'] ?> (Éléments affichés)" href="<? aff_hrefmodel('rss',True) ?>" />
        <script type="text/javascript">
        <!--
            FuncOL = new Array();
            function addFuncOL(fct) {
                FuncOL[FuncOL.length] = fct;
            }

            window.onload = function() {
                for(var i = 0, longueur = FuncOL.length; i < longueur; i++)
                    FuncOL[i]();
            };

            addFuncOL(function() {
                hide('chargement');
            });

            if (window.innerWidth <= 1100) {
                document.write('<link href="<?php echo DIR_GCWEB ?>templates/<?php echo $conf['template'] ?>/styleLowRes.css" rel="stylesheet" type="text/css" media="screen, print, handheld" />');
            }
        //-->
        </script>
    <?php }
    if ((strpos($_SERVER['HTTP_USER_AGENT'],'Gecko') | strpos($_SERVER['HTTP_USER_AGENT'],'WebKit')) && !(strpos($_SERVER['HTTP_USER_AGENT'],'Konqueror') | strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')))
        // Javascript amélioré pour firefox et compatible (safari, midori ...)
        echo '<script type="text/javascript" src="'.TEMPLATE_MODEL_URL_GCWEB.'/javascript.js"></script>';
    else
        echo '<script type="text/javascript" src="'.TEMPLATE_MODEL_URL_GCWEB.'/javascript_light.js"></script>';

    echo join("\n",$info['array_add_header']) ?>
</head>

<body class="type_<?php echo $collec['type'] ?>">
    <div id="title">
        <div class="box">
            <h1><a href="<?php echo URLRACINE_GCWEB ?>"><?php aff($info['title']) ?></a></h1>
            <p><?php aff($info['description']) ?></p>
            <fieldset id="collec-selector"><legend><?php echo __('Collections') ?></legend>
                <?php aff_chooseCollec() ?>
            </fieldset>
        </div>
    </div>

    <?php aff_noIE() ?>

    <noscript>
    <div id="noscript" class="box">
        <?php echo __('Vous avez désactivé javascript. Certaines parties du site seront inaccessibles.') ?>
    </div>
    </noscript>

    <!--Affichage des éventuels messages d'erreur ou d'avertissement -->
    <?php if ($msg) { ?><div id="error" class="box">
        <?php aff($msg); ?>
        <p>[<a href="#" onclick="javascript:hide('error')"><?php echo __('Fermer') ?></a>]</p>
    </div><?php } ?>
