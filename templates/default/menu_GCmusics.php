<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>
<div id="menu">

        <div id="infoCollec" class="box">
            <h2><?php aff($collec['title']) ?></h2>
            <p class="description"><?php aff($collec['description']) ?></p>
        </div>

        <div id="navigation" class="box">
            <h2><?php echo __('Navigation') ?></h2>
            <ul>
                <li><a href="<?php echo URLRACINE_GCWEB ?>"><?php echo __('Page d\'accueil') ?></a></li>
                <li><a href="<?php aff_hrefModel('list') ?>"><?php echo __('Toute la collection') ?></a></li>
            </ul>
            <ul>
                <li><a href="<?php aff_hrefModel('cloud') ?>#artist"><?php echo __('Artistes') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#genre"><?php echo __('Genres') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#origin"><?php echo __('Origines') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#composer"><?php echo __('Compositeurs') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#producer"><?php echo __('Producteurs') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#label"><?php echo __('Labels') ?></a></li>
            </ul>
        </div>

        <div id="classer" class="box">
            <h2><?php echo __('Classer par') ?></h2>
            <ul>

                <li><?php echo __('Titre') ?>
                    <a <?php if (isSortKey('titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleASC') ?>">↓</a><a <?php if (isSortKey('titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Artiste') ?>
                    <a <?php if (isSortKey('artistASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('artistASC') ?>">↓</a><a <?php if (isSortKey('artistDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('artistDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Format') ?>
                    <a <?php if (isSortKey('formatASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('formatASC') ?>">↓</a><a <?php if (isSortKey('formatDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('formatDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Durée') ?>
                    <a <?php if (isSortKey('runningASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('runningASC') ?>">↓</a><a <?php if (isSortKey('runningDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('runningDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Label') ?>
                    <a <?php if (isSortKey('labelASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('labelASC') ?>">↓</a><a <?php if (isSortKey('labelDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('labelDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Compositeur') ?>
                    <a <?php if (isSortKey('composerASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('composerASC') ?>">↓</a><a <?php if (isSortKey('composerDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('composerDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Producteur') ?>
                    <a <?php if (isSortKey('producerASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('producerASC') ?>">↓</a><a <?php if (isSortKey('producerDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('producerDSC') ?>">↑</a>
                </li>

                <li><?php echo __('Genre') ?>
                    <a <?php if (isSortKey('genreASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('genreASC') ?>">↓</a><a <?php if (isSortKey('genreDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('genreDSC') ?>">↑</a>
                </li>

            </ul>
        </div>

        <div id="menusearch" class="box">
            <?php aff_search('start') ?>
            <h2><?php echo __('Chercher') ?></h2>
            <p><?php /*afficher la liste de champs de recherche*/ aff_search('keyword') ?></p>
            <p><?php echo __('dans') ?> :
            <?php /*afficher la liste de champs de recherche*/
                aff_search('in_champs',array(
                        array('title=%s,|artist=%s,|label=%s,|composer=%s,|genre=%s,|tracks=%s,|tags=%s',__('tous')),
                        array('title=%s',__('titre')),
                        array('artist=%s',__('artiste')),
                        array('label=%s',__('label')),
                        array('composer=%s',__('compositeur')),
                        array('genre=%s',__('genre')),
                        array('tracks=%s',__('piste')),
                        array('tags=%s',__('tags'))
                    ))
            ?>
            </p>
            <p><?php aff_search('submit') ?></p>
            <?php aff_search('end') ?>
            <p><a href="<?php aff_hrefModel('search') ?>"><?php echo __('Recherche avancée') ?></a></p>
        </div>

        <div id="rss" class="box">
            <h2><?php echo __('S\'abonner') ?></h2>
            <ul class="rss">
                <li><a title="<?php echo __('S\'abonner au flux RSS') ?>" href="<?php aff_hrefModel('rss',False) ?>"><?php echo __('Toute la collection') ?></a></li>
                <li><a title="<?php echo __('S\'abonner au flux RSS') ?>" href="<?php aff_hrefModel('rss',True) ?>"><?php echo __('Éléments affichés') ?></a></li>
            </ul>
        </div>

        <div id="info" class="box">
            <h2><?php echo __('Informations') ?></h2>
            <ul>
                <li>
                <?php
                    if ($collec['nbItems'] == 0)    echo __('aucun élément affiché');
                    elseif ($collec['nbItems'] == 1)echo __('un élément affiché');
                    else                            printf(__('%d éléments affichés'),$collec['nbItems']);
                    echo '<br />';
                    $nbfiltre = $collec['nbItemsBDD'] - $collec['nbItems'];
                    if ($nbfiltre == 0)             echo __('aucun élément filtré');
                    elseif ($nbfiltre == 1)         echo __('un élément filtré');
                    else                            printf (__('%d éléments filtrés'),$nbfiltre);
                ?>
                </li>

                <li><?php printf (__('Collection mise à jour<br />le %s</li>'),strftime(__('%e/%m/%y'),filemtime(DIR_GCWEB.'collections/'.$collec['dir'].$collec['xml']))) ?>
            </ul>
        </div>

    </div>
    <!-- Fin du menu -->
