<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page affiche le menu et la liste de items
 *   - Les variables du tableau $info, $collec et $items peuvent être utilisées.
 */
?>
<div id="chargement" class="box">
    <p><?php echo __('Veuillez patienter le chargement de cette page peut être long') ?>.</p>

    <p>[<a href="#" onclick="javascript:hide('chargement')"><?php echo __('masquer ce message') ?></a>]</p>
</div>

<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="mosaique">

        <div class="element">

            <div class="box">

                <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menuOptionsAff.php';

                foreach ($bdd as $item)
                { ?>
                    <div id="id_<?php aff($item['id']) ?>" class="element" style="width:160px; height:160px;" onmouseover="javascript:changeInfo('info_<?php aff($item['id'])?>')" onmouseout="javascript:hide('info_<?php aff($item['id'])?>')">
                        <div class='legend' id="info_<?php aff($item['id']) ?>" style="margin-top: 160px;">
                            <h3><a href="<?php aff_hrefitem($item)?>"><?php aff($item['title']) ?></a></h3>
                            <ul>
                                <?php if (test($item['artist']))    {?> <li><span class="label"><?php echo __('Artiste') ?> :   </span><span class="info"><?php aff_filter('artist==',$item['artist']) ?></span></li><?php } ?>
                                <?php if (test($item['origin']))    {?> <li><span class="label"><?php echo __('Origine') ?> :   </span><span class="info"><?php aff_filter('origin==',$item['origin']) ?></span></li><?php } ?>
                                <?php if (test($item['genre']))     {?> <li><span class="label"><?php echo __('Genre') ?> :   </span><span class="info"><?php aff_filter('genre==',$item['genre']) ?></span></li><?php } ?>
                                <?php if (test($item['web']))       {?> <li><a href="<?php aff($item['web'])?>"><?php echo __('Lien web') ?></a></li><?php } ?>
                            </ul>
                            <?php echo join("\n", $item['array_add_to_all_pages']); ?>
                        </div>

                        <span class="image" style="height:160px;">
                            <a style="height:160px;" href="<?php aff_hrefitem($item)?>" onmouseover="javascript:changeInfo('info_<?php aff($item['id'])?>')">

                                <img alt="<?php aff($item['title']) ?>" src="<?php aff_image($item['cover'],160,160) ?>" />


                            </a>
                        </span>
                    </div><?php
                }?>

                <div class="foot"> </div>
            </div>
        </div>
    </div>
</div>