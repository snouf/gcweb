<?php
/*
 *      File generated by GCweb generator a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page qui affiche les infomration détaillée d'un élement.
 * Variable disponible : $info, $collec, $item
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="item">
        <div class="nav">
            <div class="box">

                <?php $maxkey = count ($bdd) -1;

                //Items précédentes
                if ($item['key'] > 0) {
                    foreach (array_slice($bdd, max($item['key']-2, 0), min($item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['name']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['boxpic'],'auto',80) ?>" <?php aff_attrsize_image($tmpitem['boxpic'],1000,80) ?> alt="name" />
                        </a><?php
                    }
                }

                //Item courant
                ?><a id="id_<?php aff($item['id']) ?>" title="<?php aff($item['name']) ?>" href="<?php aff_hrefitem($item)?>">
                <img class="image" src="<?php aff_image($item['boxpic'],'auto',120) ?>" <?php aff_attrsize_image($item['boxpic'],1000,120) ?> alt="<?php aff($item['name']) ?>" />
                </a><?php

                //Item suivant
                if ($item['key'] < $maxkey) {
                    foreach (array_slice($bdd, min($item['key']+1, $maxkey), min($maxkey - $item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['name']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['boxpic'],'auto',80) ?>" <?php aff_attrsize_image($tmpitem['boxpic'],1000,80) ?> alt="name" />
                        </a><?php
                    }
                }
                ?><br />

                <a title="Parcourir" href="<?php aff_hrefPage($item['key']/$conf['itemsPage']+.5) ?>#id_<?php aff($item['id']) ?>">↑</a> &nbsp;
                <?php
                aff_prevItem(5,' - ');
                aff_currentItem();
                aff_nextItem(5,' - ');
                ?>
            </div>
        </div>

        <div class="element">
            <div class="box">
                <span class="image">
                    <?php if (test($item['backpic'])) { ?>
                        <a href="<?php aff_image($item['boxpic']) ?>">
                            <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" src="<?php aff_image($item['boxpic'],280,320) ?>" <?php aff_attrsize_image($item['boxpic'],280,320) ?> alt="<?php aff($item['name'])?>" />
                        </a>
                        <a id='img2' style="display:none" href="<?php aff_image($item['backpic'],280,320) ?>">
                            <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" src="<?php aff_image($item['backpic'],280,320) ?>" <?php aff_attrsize_image($item['backpic'],280,320) ?>  alt="<?php aff($item['name'])?>" />
                        </a>
                        <a class='backpic' title="<?php echo __('Suivant') ?>" href="#" onclick="switchimg(2);return false;">↻</a>
                    <?php } else { ?>
                        <a id='img1' href="<?php aff_image($item['boxpic']) ?>">
                            <img src="<?php aff_image($item['boxpic'],280,320) ?>" <?php aff_attrsize_image($item['boxpic'],280,320) ?> alt="<?php aff($item['name'])?>" />
                        </a>
                    <?php } ?>

                </span>
                <h2><?php aff($item['name'])?></h2>

                <?php if (test($item['description']))            {?>
                    <p class="description"><?php aff($item['description']) ?></p>
                <?php } ?>

                <span class="starNote"><?php aff_star($item['rating']) ?></span>

                <ul>
                    <?php if (test($item['platform']))  {?> <li><span class="label"><?php echo __('Plateforme') ?> :   </span><span class="info"><?php aff_filter('platform==',$item['platform']) ?></span></li><?php } ?>
                    <?php if (test($item['genre']))     {?> <li><span class="label"><?php echo __('Genre') ?> :   </span><span class="info"><?php aff_filter('genre==',$item['genre']) ?></span></li><?php } ?>
                    <?php if (test($item['editor']))    {?> <li><span class="label"><?php echo __('Éditeurs') ?> :   </span><span class="info"><?php aff_filter('editor==',$item['editor']) ?></span></li><?php } ?>
                    <?php if (test($item['developer'])) {?> <li><span class="label"><?php echo __('Développeur') ?> :   </span><span class="info"><?php aff_filter('developer==',$item['developer']) ?></span></li><?php } ?>
                    <?php if (test($item['players']))   {?> <li><span class="label"><?php echo __('Joueurs') ?> : </span><span class="info"><?php aff($item['players']) ?></span></li><?php } ?>
                    <?php if (test($item['released']))  {?> <li><span class="label"><?php echo __('Date de sortie') ?> : </span><span class="info"><?php aff($item['released']) ?></span></li><?php } ?>
                    <li>
                        <?php if (test($item['web']))   {?> <a href="<?php aff($item['web'])?>"><?php echo __('Source') ?></a><?php } ?>
                    </li>
                </ul>

                <?php if (test($item['screenshot1']) | test($item['screenshot1'])) { ?>
                <h3><?php echo __('Captures d\'écran') ?></h3>
                <span class="screenshot"> <?php
                    if (test($item['screenshot1'])) echo '
                        <a href="'.image($item['screenshot1']).'">
                            <img src="'.image($item['screenshot1'],'auto',180).'" '.attrsize_image($item['screenshot1'],'auto',180).' alt="'.__('Capture d\'écran').' 1" />
                        </a>';
                    if (test($item['screenshot2'])) echo '
                        <a href="'.image($item['screenshot2']).'">
                            <img src="'.image($item['screenshot2'],'auto',180).'" '.attrsize_image($item['screenshot2'],'auto',180).' alt="'.__('Capture d\'écran').' 2" />
                        </a>';
                ?></span>
                <?php } ?>

                <?php if (isset($conf['commercialLink'])) {?>
                    <?php aff($conf['commercialLink']) ?>
                <?php }

                echo join("\n", $item['array_add_to_all_pages']);
                echo join("\n", $item['array_add_to_page_detail']);
                ?>
                <div class="foot"></div>
            </div>
        </div>
    </div>
</div>