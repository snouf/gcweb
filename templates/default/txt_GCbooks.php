<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


//Exporter fichier csv
header("Content-disposition: attachment; filename=".str_replace(' ','_',$info['title'].'_-_'.$collec['title']).".txt");
header("Content-Type: text/plain");
header("Content-Transfer-Encoding: utf-8\n"); // Surtout ne pas enlever le \n

$array = array();
foreach($bdd as $item)
    $array[] = convert($item['title']).' '.__('par').' '.convert($item['authors']).' ('.convert($item['publisher']).' '.convert($item['year']).')';

echo join("\n",$array);
?>
~~ NOHEAD ~~
~~ NOFOOT ~~