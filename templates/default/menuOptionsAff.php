<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>
<ul id="menuAffExport">
    <li class="niv1"><a href="#" onclick="javascript:showMenu('affichage');return false;" onmouseover="javascript:changeMenu('affichage');return false;" onblur="javascript:hideoldMenu();return false;"><?php echo __('Affichage') ?></a>
        <ul id="affichage" onmouseover="javascript:blockVisibilityMenu();return false;" onmouseout="javascript:unblockVisibilityMenu();return false;">
            <li><a href="<?php aff_hrefmodel('list',True,False) ?>"><?php echo __('Standard par pages') ?></a></li>
            <li><a href="<?php aff_hrefmodel('listall',True,False) ?>" title="<?php echo __('Le chargement peut être long') ?>"><?php echo __('Standard tous') ?></a></li>
            <li><a href="<?php aff_hrefmodel('onlydesc',True,False)?>"><?php echo __('Description') ?></a></li>
            <li><a href="<?php aff_hrefmodel('mosaique',True,False) ?>" title="<?php echo __('Le chargement peut être long') ?>"><?php echo __('Mosaïque') ?></a></li>
        </ul>
    </li>
    <li class="niv1"><a href="#" onclick="javascript:showMenu('exportation');return false;" onmouseover="javascript:changeMenu('exportation');return false;" onblur="javascript:hideoldMenu();return false;"><?php echo __('Exportation') ?></a>
        <ul id="exportation" onmouseover="javascript:blockVisibilityMenu();return false;" onmouseout="javascript:unblockVisibilityMenu();return false;">
            <li><a href="<?php aff_hrefmodel('csv',True) ?>"><?php echo __('CSV (classeur)') ?></a></li>
            <li><a href="<?php aff_hrefmodel('txt',True) ?>"><?php echo __('Texte brut') ?></a></li>
        </ul>
    </li>
</ul>
