<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Partie centale de la page qui affiche la liste des élémet de la collection
 *   - Les variables du tableau $info, $collec et $items peuvent être utilisées.
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="list">
        <div class="nav">
            <div class="box">
                <p class="navpage">
                    <?php aff_prevPage(5,' - ')?>
                      <?php aff_currentPage() ?>
                    <?php aff_nextPage(5,' - ') ?>
                </p>
                <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menuOptionsAff.php'; ?>
            </div>
        </div>


        <?php
        /*************** Debut de la boucle sur le items *********************/
        foreach ($items as $item) {
        ?>
        <div id="id_<?php aff($item['id']) ?>" class="element<?php if(test($item['borrower']) & (convert($item['borrower']) != 'none')) echo ' lent lent_'.$info['lang']; ?>">
            <div class="box">
                <a title="Détail" href="<?php aff_hrefitem($item)?>">
                    <img class="image" src="<?php aff_image($item['image'],'auto',120) ?>" <?php aff_attrsize_image($item['image'],'auto',120) ?> alt="<?php printf(__('Couverture de %s'),convert($item['title'])) ?>" />
                </a>
                <div class="scroll">
                    <h3><a title="<?php echo __('Détail') ?>" href="<?php aff_hrefitem($item)?>"><?php aff($item['title']) ?></a></h3>
                    <a title="<?php echo __('Plus d\'info') ?>" href="<?php aff_hrefitem($item)?>"><span class="starNote"><?php aff_star($item['rating']) ?></span></a>
                    <ul>
                        <?php if (test($item['director']))  {?> <li><span class="label" style="letter-spacing:-.06em"><?php echo __('Réalisateur') ?> :</span>  <span class="info"><?php aff_filter('director==',$item['director']) ?></span></li><?php } ?>
                        <?php if (test($item['actors_without_roles']))    {?> <li><span class="label"><?php echo __('Acteurs') ?> :</span>    <span class="info"><?php aff_filter('actors_without_roles==',$item['actors_without_roles']) ?></span></li><?php } ?>
                        <?php if (test($item['country']))   {?> <li><span class="label"><?php echo __('Pays') ?> :</span>       <span class="info"><?php aff_filter('country==',$item['country']) ?> </span></li><?php } ?>
                        <?php if (test($item['genre']))     {?> <li><span class="label"><?php echo __('Genre') ?> :</span>      <span class="info"><?php aff_filter('genre==',$item['genre']) ?></span></li><?php } ?>
                        <?php if (test($item['webPage']))   {?> <li><a href="<?php aff($item['webPage'])?>"><?php echo __('Source') ?></a></li><?php } ?>
                    </ul>
                    <?php echo join("\n", $item['array_add_to_all_pages']); ?>
                </div>
            </div>
        </div>
        <?php
        }
        /**************** Fin de la boucle sur les items *********************/
        ?>

        <div class="nav">
            <div class="box">
                <p class="navpage">
                    <?php aff_prevPage(5,' - ')?>
                    <?php aff($collec['page']) ?>
                    <?php aff_nextPage(5,' - ') ?>
                </p>
            </div>
        </div>
    </div>
</div>

