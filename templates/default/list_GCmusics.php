<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>

<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="list">
        <div class="nav">
            <div class="box">
                <p class="navpage">
                    <?php aff_prevPage(5,' - ') ; aff_currentPage() ; aff_nextPage(5,' - ') ?>
                </p>
                <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menuOptionsAff.php'; ?>
            </div>
        </div>


        <?php
        /*************** Debut de la boucle sur le items *********************/
        foreach ($items as $item) {
        ?>
        <div id="id_<?php aff($item['id']) ?>" class="element">
            <div class="box">

                <a href="<?php aff_hrefitem($item)?>">
                    <img class="image" src="<?php aff_image($item['cover'],160,120) ?>" <?php aff_attrsize_image($item['cover'],1000,120) ?> alt="<?php aff($item['title']) ?>" />
                </a>

                <div class="scroll">
                    <h3><a href="<?php aff_hrefitem($item)?>"><?php aff($item['title']) ?></a></h3>
                    <a title="<?php echo __('Plus d\'info') ?>" href="<?php aff_hrefitem($item)?>">
                        <span class="starNote"><?php aff_star($item['rating']) ?></span>
                    </a>
                    <ul>
                        <?php if (test($item['artist']))    {?> <li><span class="label"><?php echo __('Artiste') ?> :</span><span class="info"><?php aff_filter('artist==',$item['artist']) ?></span></li><?php } ?>
                        <?php if (test($item['genre']))     {?> <li><span class="label"><?php echo __('Genre') ?> :</span>  <span class="info"><?php aff_filter('genre==',$item['genre']) ?></span></li><?php } ?>
                        <?php if (test($item['release']))   {?> <li><span class="label"><?php echo __('Sortie') ?> :</span> <span class="info"><?php aff($item['release']) ?></span></li><?php } ?>
                        <?php if (test($item['running']))   {?> <li><span class="label"><?php echo __('Durée') ?> :</span>  <span class="info"><?php aff($item['running']) ?></span></li><?php } ?>
                        <?php if (test($item['web']))       {?> <li><a href="<?php aff($item['web'])?>"><?php echo __('Lien web') ?></a></li><?php } ?>
                    </ul>
                    <?php echo join("\n", $item['array_add_to_all_pages']); ?>
                </div>
            </div>
        </div>
        <?php
        }
        /**************** Fin de la boucle sur les items *********************/
        ?>

        <div class="nav">
            <div class="box">
                <p class="navpage">
                    <?php aff_prevPage(5,' - ') ; aff($collec['page']) ; aff_nextPage(5,' - ') ?>
                </p>
            </div>
        </div>
    </div>
</div>
