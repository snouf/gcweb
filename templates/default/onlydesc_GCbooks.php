<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page qui affiche la liste des éléments de la collection
 *   - Les variables du tableau $info, $collec et $items peuvent être utilisées.
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="onlydesc">

       <div class="element">
            <div class="box">
                <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menuOptionsAff.php'; ?>

                <table>
                    <tr class='title'>
                        <th>
                            <?php echo __('Titres') ?>
                            <a <?php if (isSortKey('titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleASC') ?>">↓</a><a <?php if (isSortKey('titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleDSC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Auteurs') ?>
                            <a <?php if (isSortKey('authorsASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('authorsASC') ?>">↓</a><a <?php if (isSortKey('authorsASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('authorsASC') ?>">↑</a>
                        </th>
                        <th>
                            <?php echo __('Éditeurs') ?>
                            <a <?php if (isSortKey('publisherASC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('publisherASC,titleASC') ?>">↓</a><a <?php if (isSortKey('publisherDSC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('publisherDSC,titleDSC') ?>">↑</a>
                        </th>
                    </tr>
                    <?php
                    $paire = False;
                    foreach ($bdd as $item) {
                        if ($paire)     {   $paire=False;   $class='paire';     }
                        else            {   $paire=True;    $class='impaire';   }
                        ?>
                        <tr id="id_<?php aff($item['id']) ?>" class="line_<?php echo $class;  if(array_key_exists('borrower', $item) && test($item['borrower']) && (convert($item['borrower']) != 'none')) echo ' lent lent_'.$info['lang'];?>">
                            <th>
                                <a class="title" href="<?php aff_hrefitem($item) ?>" onmouseover="javascript:load_img('<?php echo URL_GCWEB.'index.php'; aff_image($item['cover'],'auto',120) ?>', 'parent_image_<?php aff($item['id'])?>'); show('parent_image_<?php aff($item['id'])?>')" onmouseout="javascript:hide('parent_image_<?php aff($item['id'])?>');">
                                    <?php aff($item['title']) ?></a>
                                <span class="image" id="parent_image_<?php aff($item['id']) ?>"></span>
                            </th>
                            <td><?php aff_filter('authors==',$item['authors'],'',', ','','onlydesc') ?></td>
                            <td><?php aff_filter('publisher==',$item['publisher'],'',', ','','onlydesc') ?> (<?php aff_filter('year==',$item['year'],'',', ','','onlydesc') ?>)</td>
                        </tr>
                    <?php }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
