<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Partie centale de la page qui affiche les informations détaillées d'un élément.
 * Variable disponible : $info, $collec, $item
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="item">
        <div class="nav">
            <div class="box">
                <?php $maxkey = count ($bdd) -1;

                //Items précédentes
                if ($item['key'] > 0) {
                    foreach (array_slice($bdd, max($item['key']-2, 0), min($item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['title']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['cover'],'auto',80) ?>"  <?php aff_attrsize_image($tmpitem['cover'],'auto',80) ?> alt="-2" />
                        </a><?php
                    }
                }

                //Item courant
                ?><a id="id_<?php aff($item['id']) ?>" title="<?php aff($item['title']) ?>" href="<?php aff_hrefitem($item)?>">
                <img class="image" src="<?php aff_image($item['cover'],'auto',120) ?>" <?php aff_attrsize_image($item['cover'],'auto',120) ?> alt="<?php aff($item['title']) ?>" />
                </a><?php

                //Item suivant
                if ($item['key'] < $maxkey) {
                    foreach (array_slice($bdd, min($item['key']+1, $maxkey), min($maxkey - $item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['title']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['cover'],'auto',80) ?>" <?php aff_attrsize_image($tmpitem['cover'],'auto',80) ?> alt="-2" />
                        </a><?php
                    }
                }
                ?><br />
                <a title="<?php echo __('Parcourir') ?>" href="<?php aff_hrefPage($item['key']/$conf['itemsPage']+.5) ?>#id_<?php aff($item['id']) ?>">↑</a> &nbsp;
                <?php
                aff_prevItem(5,' - ');
                aff_currentItem();
                aff_nextItem(5,' - ');
                ?>
            </div>
        </div>
        <div class="element<?php if(array_key_exists('borrower', $item) && test($item['borrower']) && (convert($item['borrower']) != 'none')) echo ' lent lent_'.$info['lang']; ?>">
            <div class="box">
                <span class="image">
                <?php if (test($item['backpic'])) { ?>
                    <a id='img1' href="<?php aff_image($item['cover']) ?>">
                        <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" src="<?php aff_image($item['cover'],'auto',360) ?>" <?php aff_attrsize_image($item['cover'],'auto',360) ?> alt="<?php printf(__('Couverture de %s'),convert($item['title'])) ?>" />
                    </a>
                    <a id='img2' style="display:none" href="<?php aff_image($item['backpic'],'auto',360) ?>">
                        <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" class="image" src="<?php aff_image($item['backpic'],'auto',360) ?>" <?php aff_attrsize_image($item['backpic'],'auto',360) ?> alt="<?php echo sprintf(__('Autres images de %s'), convert($item['title']))?>" />
                    </a>
                    <a class='backpic' title="<?php echo __('Suivant') ?>" href="#" onclick="switchimg(2);return false;">↻</a>
                <?php } else { ?>
                    <a id='img1' href="<?php aff_image($item['cover']) ?>">
                        <img src="<?php aff_image($item['cover'],'auto',360) ?>" <?php aff_attrsize_image($item['cover'],'auto',360) ?> alt="<?php printf(__('Couverture de %s'),convert($item['title'])) ?>" />
                    </a>
                <?php } ?>
                </span>

                <?php if(array_key_exists('borrower', $item) && test($item['borrower']) && (convert($item['borrower']) != 'none')) echo '<span id="lent"></span>'; ?>

                <h2><?php aff($item['title'])?></h2>
                <?php if (test($item['description']))         {?><p class="description"><?php aff($item['description']) ?></p> <?php } ?>
                <span class="starNote"><?php aff_star($item['rating']) ?></span>
                <ul>
                    <?php
                        if (test($item['serie']))         echo '<li><span class="label">'.__('Collection').' :</span><span class="info">'.filter('serie==',$item['serie']).'</span></li>';
                        if (test($item['authors']))       echo '<li><span class="label">'.__('Auteurs').' :   </span><span class="info">'.filter('authors==',$item['authors']).'</span></li>';
                        if (test($item['publisher']))     echo '<li><span class="label">'.__('Éditeur').' :   </span><span class="info">'.filter('publisher==',$item['publisher']).'</span></li>';
                        if (test($item['genre']))         echo '<li><span class="label">'.__('Genre').' :     </span><span class="info">'.filter('genre==',$item['genre']).'</span></li>';
                        if (test($item['isbn']))          echo '<li><span class="label">'.__('ISBN').' :      </span><span class="info">'.convert($item['isbn']).'</span></li>';
                        if (test($item['pages']))         echo '<li><span class="label">'.__('Pages').' :     </span><span class="info">'.convert($item['pages']).'</span></li>';
                        if (test($item['language']))      echo '<li><span class="label">'.__('Langue').' :    </span><span class="info">'.convert($item['language']).'</span></li>';
                        if (test($item['publication']))   echo '<li><span class="label">'.__('Publié le').' : </span><span class="info">'.convert($item['publication']).'</span></li>';
                        if (test($item['added']))         echo '<li><span class="label">'.__('Ajouté le').' : </span><span class="info">'.convert($item['added']).'</span></li>';
                        if (test($item['web']))           echo '<li><a href="'.convert($item['web']).'">'.__('Source').'</a></li>';
                    ?>
                </ul>
                <?php if (isset($conf['commercialLink'])) {?>
                    <?php aff($conf['commercialLink']) ?>
                <?php } ?>

                <?php if (test($item['comments'])) { ?>
                <h3><?php echo __('Commentaires') ?></h3>
                <p class="comments"><?php aff($item['comments']) ?></p>
                <?php }
                echo join("\n", $item['array_add_to_all_pages']);
                echo join("\n", $item['array_add_to_page_detail']);
                ?>

                <div class="foot"></div>
            </div>
        </div>
    </div>
</div>
