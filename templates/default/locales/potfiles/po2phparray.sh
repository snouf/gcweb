#! /bin/sh
# commentaire
# Auteur : Jonas Fourquier

mypath=$(dirname $0)
cd "$mypath/.."

for pofile in $(find ./ -name '*.po')
    do
    phpfile=${pofile%.po}.php
    echo "$pofile -> $phpfile"
    potfiles/po2phparray.py $pofile > $phpfile
    echo ".................. fait"
done
