#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def main(argv):
    if '-h' in argv :       usage()
    elif '--help' in argv : usage()
    elif len(argv) == 1 :   po2phparray(argv[0])
    else :                  usage()


def po2phparray(file):
    fp_in = open(file)
    print ('''<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(''')
    key = []
    value = []
    what = ''


    for line in fp_in.readlines() :

        line = line.strip()

        if line[0:5] == 'msgid' :
            if len(key) != 0 :
                print (format (key,value))
            key = []
            value = []
            what = ''

        if line[0:1] == '#' or line[0:9] == 'msgid "%%' :
            pass
        elif line[0:8] == 'msgid ""' :
            '''debut d'une msgid multiligne'''
            what = 'key_multiline'

        elif line[0:5] == 'msgid' :
            '''debut d'une msgid monoligne'''
            key.append(line[6:].strip()[1:-1])
            what = 'key_singleline'

        elif line[0:9] == 'msgstr ""' :
            '''debut d'une msgstr multiligne'''
            what = 'value_multiline'

        elif line[0:6] =='msgstr':
            '''debut d'une msgstr monoligne'''
            value.append(line[7:].strip()[1:-1])
            what = 'value_singleline'

        elif what == 'key_multiline':
            '''continuation msgid multiligne'''
            key.append(line.strip()[1:-1])

        elif what == 'value_multiline':
            '''continuation msgstr multiligne'''
            value.append(line.strip()[1:-1])

    print (format (key,value))

    fp_in.close()

    print ('''));
?>''')


def format(key,value) :
    keystr = ''.join(key)
    keystr = keystr.replace('\\"','"')
    keystr = keystr.replace('\'','\\\'')
    keystr = keystr.replace('\\n','\n')

    valuestr = ''.join(value)
    valuestr = valuestr.replace('\\"','"')
    valuestr = valuestr.replace('\'','\\\'')
    valuestr = valuestr.replace('\\n','\n')

    if valuestr != '':
        return "'%s'\n => '%s',"%(keystr,valuestr.strip())
    else :
        return "/*'%s'\n => '%s',*/"%(keystr,valuestr.strip())


def usage():
    '''Aide'''
    print ('''po2phparray.py

usage : po2phparray.py file.po > file.php


Copyright 2007 Jonas Fourquier <jonas.tuxfamily.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.''')


main(sys.argv[1:])
