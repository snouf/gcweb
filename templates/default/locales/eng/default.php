<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Informations'
 => 'Information',
'Type de collection'
 => 'Type of collection',
'Nombre d\'éléments'
 => 'Count of entries',
'Dernière mise à jour'
 => 'Last update',
'%A %e %B %Y'
 => '%A %e %B %Y',
'Voir la collection'
 => 'Display the collection',
'Derniers éléments ajoutés'
 => 'Last added entries',
'S\'abonner (RSS 2.0)'
 => 'Subscribe (RSS 2.0)',
'Collection inconnue pour le thème'
 => 'Unknown collection for the theme',
'Les fichiers de thème de cette collection n\'ont pas encore été créés. <a href="%s&amp;generator=True">Cliquez ici</a> pour les créer'
 => 'The theme files for this collection were not created yet. <a href="%s&amp;generator=True">Click here</a> to create',
'Erreur : La page demandée n\'existe pas'
 => 'Error : The requested page does not exist',
'Collections'
 => 'Collections',
'Vous avez désactivé javascript. Certaines parties du site seront inaccessibles.'
 => 'You have Javascript disabled. Some parts of the site will not be reachable.',
'Fermer'
 => 'Close',
'<span class="strong">Note :</span> Certaines informations peuvent provenir de sources externes. Pour les connaître, cliquez sur le lien "source" de l\'élément'
 => '<span class="strong">Note :</span> Some information may come from external sources. Click on the "Source" link of the entry to access them.',
'Propulsé par'
 => 'Powered by',
'Affichage'
 => 'Display',
'Standard par pages'
 => 'Standard per pages',
'Le chargement peut être long'
 => 'The download may take some time',
'Standard tous'
 => 'Standard all',
'Description'
 => 'Description',
'Mosaïque'
 => 'Mosaic',
'Exportation'
 => 'Export',
'CSV (classeur)'
 => 'CSV (file)',
'Texte brut'
 => 'Raw text',
'Genres'
 => 'Genres',
'Langues'
 => 'Language',
'Sous-titres'
 => 'Subtitles',
'Séries'
 => 'Series',
'Auteurs'
 => 'Authors',
'Editeurs'
 => 'Editors',
'Mécanismes'
 => 'Mechanisms',
'Éditeurs'
 => 'Editors',
'Années de publication'
 => 'Year of publication',
'Scénaristes'
 => 'Scriptwriters',
'Dessinateurs'
 => 'Drawing by',
'Coloristes'
 => 'Colourists',
'Réalisateurs'
 => 'Directors',
'Acteurs'
 => 'Actors',
'Pays'
 => 'Countries',
'Années de sortie'
 => 'Year of publication',
'Plateformes'
 => 'Platforms',
'Développeurs'
 => 'Developers',
'Artistes'
 => 'Artist',
'Origines'
 => 'Origins',
'Compositeurs'
 => 'Composers',
'Producteurs'
 => 'Producers',
'Labels'
 => 'Labels',
'Cliquez dans le coin inférieur droit pour voir une autre image'
 => 'Click on the back right corner to display another image',
'Genre'
 => 'Genre',
'Langue'
 => 'Language',
'Sous-titre'
 => 'Subtitles',
'Saison'
 => 'Season',
'Épisode'
 => 'Episode',
'Source'
 => 'Source',
'Auteur(s)'
 => 'Author(s)',
'Illustrateur(s)'
 => 'Illustrator(s)',
'Editeur'
 => 'Editor',
'Joueurs'
 => 'Players',
'Durée'
 => 'Duration',
'Mécanisme(s)'
 => 'Mechanism(s)',
'Extension de'
 => 'Extension of',
'Parcourir'
 => 'Search',
'Couverture de %s'
 => 'Cover page for %s',
'Autres images de %s'
 => 'Other images for %s',
'Suivant'
 => 'Next',
'Série'
 => 'Series',
'Éditeur'
 => 'Editor',
'ISBN'
 => 'ISBN',
'Pages'
 => 'Pages',
'Publié le'
 => 'Published by',
'Ajouté le'
 => 'Added by',
'Commentaires'
 => 'Comments',
'Scénario'
 => 'Storyboard',
'Dessin'
 => 'Drawing',
'Couleurs'
 => 'Colours',
'Collection'
 => 'Collection',
'Source Web'
 => 'Source',
'Réalisateur'
 => 'Director',
'Sortie'
 => 'Publication date',
'Plateforme'
 => 'Platform',
'Développeur'
 => 'Developer',
'Date de sortie'
 => 'Release Date',
'Captures d\'écran'
 => 'Screenshots',
'Capture d\'écran'
 => 'Screenshot',
'Artiste'
 => 'Artist',
'Label'
 => 'Label',
'Compositeur'
 => 'Compositor',
'Producteur'
 => 'Producer',
'Format'
 => 'Format',
'Origine'
 => 'Origin',
'Emplacement'
 => 'Location',
'Tags'
 => 'Tags',
'Lien web'
 => 'Link',
'Plus d\'info'
 => 'More information',
'Publié en'
 => 'Published by',
'Détail'
 => 'Detail',
'URL'
 => 'URL',
'Veuillez patienter le chargement de cette page peut être long'
 => 'Please wait the loading of the page may take some time',
'masquer ce message'
 => 'hide this message',
'Navigation'
 => 'Navigation',
'Page d\'accueil'
 => 'Welcome page',
'Toute la collection'
 => 'Entire collection',
'Classer par'
 => 'Sorted by',
'Diffusé'
 => 'Released',
'Chercher'
 => 'Search',
'dans'
 => 'in',
'Titre'
 => 'Title',
'Recherche avancée'
 => 'Advanced search',
'S\'abonner'
 => 'Subscribe',
'S\'abonner au flux RSS'
 => 'Subscribe to RSS',
'Éléments affichés'
 => 'Displayed entries',
'aucun élément affiché'
 => 'no filtered entry',
'un élément affiché'
 => '%d displayed entries',
'%d éléments affichés'
 => '%d displayed entries',
'aucun élément filtré'
 => 'no filtered entry',
'un élément filtré'
 => 'one filtered entry',
'%d éléments filtrés'
 => '%d filtered entries',
'Collection mise à jour<br />le %s</li>'
 => 'Collection updated <br />on %s</li>',
'%e/%m/%y'
 => '%e/%m/%y',
'Les séries'
 => 'series',
'Les auteurs'
 => 'Authors',
'Les éditeurs'
 => 'Editors',
'Les genres'
 => 'Genres',
'Auteur'
 => 'Author',
'Notation'
 => 'Notation',
'Date de publication'
 => 'Publication date',
'Date d\'ajout'
 => 'Date added',
'tous'
 => 'all',
'titre'
 => 'title',
'série'
 => 'Series',
'auteur'
 => 'author',
'éditeur'
 => 'editor',
'genre'
 => 'genre',
'Les scénaristes'
 => 'Scriptwriters',
'Les dessinateurs'
 => 'Designers',
'Les coloristes'
 => 'Colourists',
'Les collections'
 => 'Collections',
'Nom'
 => 'Name',
'Scénariste'
 => 'Series',
'Dessinateur'
 => 'Drawing by',
'Coloriste'
 => 'Colorist',
'Les réalisateurs'
 => 'The realisators',
'Les acteurs'
 => 'The actors',
'réalisateur'
 => 'realisator',
'acteur'
 => 'author',
'Editor'
 => 'Editor',
'Tous'
 => 'all',
'plateforme'
 => 'Platform',
'développeur'
 => 'Developer',
'artiste'
 => 'Artist',
'label'
 => 'label',
'compositeur'
 => 'compositor',
'piste'
 => 'track',
'tags'
 => 'tags',
'Année'
 => 'Year',
'Nationalité'
 => 'Nationality',
'Titres'
 => 'Titles',
'Tome'
 => 'Volume',
'Synopsis'
 => 'Synopsis',
'Pistes'
 => 'Tracks',
'Publier en'
 => 'Published on',
'Titre original'
 => 'Original title',
'Multicritères'
 => 'Multi criteria',
'Les critères vides seront ignorés.'
 => 'The empty field will be ignored',
'Les dates peuvent être entrée sous la forme %s, 2007 (considéré comme 1er janvier 2007), ...'
 => 'Date entries can be input in the format %s, 2007 (considered as January 1st 2007), ...',
'Dans les listes à choix multiples pour séléctionner plusieurs éléments maintenez pressé la touche contrôle.'
 => 'For multiple selections on a list, press and hold the "control" key.',
'Requête'
 => 'Request',
'Il est possible de réaliser une requête composée de plusieurs conditions afin d\'effectuer une recherche encore plus précise'
 => 'It is possible to to create a request made of multiple conditions to generate a more specific search condition',
'Chercher dans'
 => 'Search on',
'toute la base'
 => 'Entire database',
'les résultats précédents'
 => 'the previous results',
'ex:'
 => 'eg:',
'Champs contenant du texte'
 => 'Fields with text',
'contient'
 => 'contains',
'ne contient pas'
 => 'does not contain',
'est (chaîne exacte)'
 => 'is (exact string)',
'n\'est pas (chaîne exacte)'
 => 'is not (exact string)',
'Champs numériques et dates'
 => 'Numerical fields and dates',
'égal'
 => 'equal',
'différent'
 => 'different',
'plus petit'
 => 'smaller',
'plus petit ou égal'
 => 'smaller or equal',
'plus grand'
 => 'greater',
'plus grand ou égal'
 => 'greater or equal',
'Ajouter la condition'
 => 'Add the condition',
'Illustrateur'
 => 'Illustrator',
'Mécanisme'
 => 'Mechanism',
'les resultats précédents'
 => 'the previous results',
'Ajouté à la collection entre (dates)'
 => 'Added to the collection between (dates)',
'Publié entre (dates)'
 => 'Published between (dates)',
'Nombre de pages entre'
 => 'Number of pages',
'Note entre'
 => 'Genres',
'Informations générales'
 => 'General informations',
'Note (0 à 10)'
 => 'Note (0 to 10)',
'Informations diverses'
 => 'Miscellaneous informations',
'Nombre de pages'
 => 'Number of pages',
'Édition'
 => 'Edition',
'Traducteur'
 => 'Translator',
'Format du livre'
 => 'Book format',
'Source des informations'
 => 'Source of informations',
'Commentaire'
 => 'Comment',
'Dates'
 => 'Dates',
'd\'ajout'
 => 'of acquisition',
'de publication'
 => 'of publication',
'année de publication'
 => 'Year of publication',
'Status/emprunts'
 => 'State/lending',
'Lu ?'
 => 'Read ?',
'Emprunté ?'
 => 'Lent ?',
'Emprunteur'
 => 'Borrower',
'Date d\'emprunt'
 => 'Out since',
'Acteur'
 => 'Actor',
'Rôle'
 => 'Role',
'Sorti entre (dates)'
 => 'Released between (dates)',
'Titre Original'
 => 'Original title',
'Rôles'
 => 'Roles',
'Nombre'
 => 'Volume',
'Identifiant'
 => 'Identifier',
'Age minimum'
 => 'Minimum Age',
'Format Vidéo'
 => 'Video Format',
'Région'
 => 'Region',
'Langues Audio'
 => 'Language',
'Sous Titre'
 => 'Subtitle',
'de sortie'
 => 'Publication date',
'Vu ?'
 => 'Read ?',
'Note'
 => 'Publication date',
'Nom de la piste'
 => 'Track name',
'Format de l\'album'
 => 'Book format',
'par'
 => 'by',
'joueur(s)'
 => 'Autors',
));
?>
