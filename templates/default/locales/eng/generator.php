<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Un "nuage" (cloud) est réalisé avec l\'ensemble des valeurs d\'un champ.
    Plus la valeur est présente dans votre collection plus elle est mise en avant
    (exemple : le champ "auteur" affichera l\'ensemble des auteurs et mettera en
    avant celui dons vous procédez le plus d\'ouvrage).
    <a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#cloud">Plus d\'explications</a>.'
 => 'A cloud is generated with all values of a field.
    The more it is present the higher it is in the cloud
    (eg : the "author" field will display all the authors and the one for which
    you have many books will appear at front).
    <a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#cloud">More details</a>.',
'Attention! Le modèle "menu" doit être créé avant cette page'
 => 'Warning ! The "menu" model must be created before this page',
'Liste des nuages'
 => 'List of clouds',
'Titre du nuage'
 => 'Cloud title',
'champ avec lequel faire le nuage'
 => 'field to be used for the cloud',
'Permet d\'exporter la liste des élément sous la forme d\'un fichier cvs
    (facillement importable dans un tableur type openoffice calc).
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#csv">Plus d\'explications</a>).</p>

    <p style="color:red">Attention la prévisualisation de cette page n\'est pas possible
    de plus lors de l\'enregistrement un message comme quoi la page contient des erreurs va
    s\'afficher. Celui-ci est sans importance.'
 => 'Allow to export the list of entries as a csv file
   (easily exported in a spreadsheet like calc openoffice one).
   (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#csv">More details</a>).</p>

   <p style="color:red">Warning : the previsulation of this page is not possible
   furthermore while saving a message about error in the page will be displayed.
   This last message will not be crititcal.',
'Séléctionnez les champs devant être précédé par l\'url de votre site (tels que les images)'
 => 'Select the fields that have to be prefixed by the url of your site (like the images)',
'Cette page affiche le détails d\'un élement (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#item">Plus d\'explications</a>)'
 => 'This page display the details of an entry(<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#item">More details</a>)',
'Titre de l\'élément'
 => 'Title of the entry',
'Saisie obligatoire'
 => 'Mandatory field',
'Note'
 => 'Note',
'(certainement un chiffre entre 0 et 10)'
 => '(most probably a number between 0 and 10)',
'Image représentative'
 => 'Representative image',
'Autre image'
 => 'Other image',
'Taille des images en pixel (largeur max x hauteur max, les proportions sont conservées)'
 => 'Size of the images as number of pixel (max width x max height, the ratio is kept',
'de l\'élément courant dans la barre de navigation'
 => 'of the current entry in the navigation bar',
'Valeurs conseillées pour une image plus haute que large'
 => 'Suggested values for an image higher than larger',
'Les proportions de l\'image seront conservées'
 => 'The image ratio will not change',
'des autres éléments dans la barre de navigation'
 => 'the other entries in the navigation bar',
'de l\'élément courant à coté de la description'
 => 'the current entry  close to the description',
'Champs avec beaucoup de texte (généralement la description) à mettre avant les informations'
 => 'Fields with long text (generaly the description) to be placed before the informations',
'Information avec un lien vers un filtre (quand on clique dessus, GCweb affichera tous les éléments ayant cette valeur)'
 => 'Information with a link to a filtering (when you select it, GCweb will display all the entries with this value)',
'Label'
 => 'Label',
'Champ'
 => 'Field',
'Autres informations sans lien'
 => 'Other information with no link',
'Liste de lien'
 => 'List of links',
'url'
 => 'url',
'Autres champs avec beaucoup de texte'
 => 'Other fields with a lot of text',
'Cliquez dans le coin inférieur droit pour voir une autre image'
 => 'Click on the bottom right corner to display another image',
'Suivant'
 => 'Next',
'Cette page liste les éléments de la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#list">plus d\'explications</a>)'
 => 'This page lists the entries of the collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#list">more details</a>)',
'Certainement un chiffre entre 0 et 10'
 => 'Most probably a 0 to 10 number',
'Image réprésentative'
 => 'Representative image',
'Taille de cette image'
 => 'Size of this image',
'Information avec un lien vers un filtre (quand on clique dessus, GCweb affichera tous
            les éléments ayant cette valeur). Ne sélectionnez pas trop d\'éléments car le place
            est limitée.'
 => 'Information with a link to a filter (when we select it, GCweb will display all the entries
           with this value). Do not select too many fields because of limited space.',
'Autres informations sans lien (idem pour le nombre d\'éléments)'
 => 'Other informations with no link (same as the number of entries)',
'Plus d\'info'
 => 'More details',
'Affiche tous les élément de la collection sur une seule page (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#listall">plus d\'explications</a>)'
 => 'Display all the entries of the collection on one page(<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#listall">more details</a>)',
'<strong>Enregistrer simplement cette page</strong> (il n\'y a aucun champ à compléter)'
 => '<strong>Save only this page</strong> (there is no field to fill)',
'Attention! Les modèles "menu" et "list" doivent être crées avant cette page'
 => 'Warning ! the "menu" and "list" models have to be created before this page',
'Veuillez patienter le chargement de cette page peut être long'
 => 'Please wait, the loading of the page may last some time',
'masquer ce message'
 => 'Hide this message',
'Cette partie s\'intégre dans la page d\'accueil et propose un petit aperçu de la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#main">plus d\'explications</a>)'
 => 'This section will be part of the welcome page and give a summary of the collection  (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#main">more details</a>)',
'L\'aperçu de cette page ne s\'affiche pas correctement dans le générateur
    (alignement des images, informations complémentaires affichée de façon
    permanente ...) ceci est "normal" car la page d\'accueil n\'est pas affichée
    dans sont intégralitée'
 => 'The summary of this page do not display correctly in the generator
   (images alignement, complementary informations permanently displayed ...)
   this is "normal"  because the welcome page is not entirely displayed.',
'Taille max de celle-ci'
 => 'It\'s max size',
'Information avec un lien vers un filtre (quand on clique dessus, GCweb affiche tout
            les éléments ayant cette valeur)'
 => 'Information with a link to a filter (when you click on it, GCweb display all
           the entries with this value)',
'Menu de gauche commun à la plupart des pages (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#menu">plus d\'explications</a>)'
 => 'Left menu common to almost all pages  (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#menu">more details</a>)',
'Section "tri des éléments"'
 => 'Section "Sort entries"',
'champ selon lequel trier'
 => 'field to be used for sorting',
'Section "recherche rapide"'
 => 'Section "quick search"',
'Label du champ de recherche'
 => 'Label of searching field',
'champ dans lequel chercher'
 => 'field in which to search',
'Navigation'
 => 'Navigation',
'Page d\'accueil'
 => 'Welcome page',
'Toute la collection'
 => 'The entire collection',
'Filtres "nuages"'
 => '"Clouds" filters',
'Classer par'
 => 'Sort by',
'Chercher'
 => 'Search',
'dans'
 => 'in',
'Recherche avancée'
 => 'Advanced search',
'S\'abonner'
 => 'Register',
'S\'abonner au flux RSS'
 => 'Register to RSS',
'Éléments affichés'
 => 'Displayed entries',
'Informations'
 => 'Informations',
'aucun élément affiché'
 => 'no filtered entries',
'un élément affiché'
 => '%d displayed entries',
'%d éléments affichés'
 => '%d displayed entries',
'aucun élément filtré'
 => 'no filtered entries',
'un élément filtré'
 => 'one filtered entry',
'%d éléments filtrés'
 => '%d filtered entries',
'Collection mise à jour<br />le %s</li>'
 => 'Collection updated <br />on %d</li>',
'%e/%m/%y'
 => '%e/%m/%y',
'Cette page affiche toutes les images de la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#mosaique">plus d\'explications</a>)'
 => 'This page displays all the images of the collection(<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#mosaique">more details</a>)',
'L\'image n\'est pas obligatoir mais ce type d\'affichage n\'a aucun intéret sans'
 => 'The image is not required but this type of display is not interesting without it',
'Hauteur'
 => 'Height',
'<code>120</code> est conseillé pour des images plus haute que large'
 => '<code>120</code> is suggested for images with height greater than width',
'Largeur standard'
 => 'Standard width',
'<code>80</code> est conseillé. Les images plus large seront rognées, 2 bandes noire compléteront les plus étroite'
 => '<code>80</code> is suggested. The larger images will be truncated,  2 black side bars will be added for smaller ones',
'Information avec un lien vers un filtre (affiché lors du survole de l\'élément'
 => 'Information with a link to a filter (displayed while moving above the entry',
'Cette page liste tous les éléments de la collection sous la forme d\'un tableau
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#onlydesc">plus d\'explications</a>)'
 => 'This page displays all the entries of the collection as an array
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#onlydesc">more details</a>)',
'Dimension maximum'
 => 'Maximum size',
'ex:'
 => 'eg:',
'1ère colonne (généralemment le "titre" de l\'élément'
 => '1st column (generally the "title" of the entry',
'saisie obligatoire'
 => 'mandatory field',
'Nom de la colonne'
 => 'Column name',
'Champ à afficher'
 => 'Field to be displayed',
'2ème colonne'
 => '2nd column',
'3ème colonne'
 => '3rd column',
'4ème colonne (ça commence à faire beaucoup de colonne sur des écrans de faible résolution)'
 => '4th column (starting to be too much for a small resolution screen)',
'5ème colonne (au-delà, c\'est trop !)'
 => '5th column (greater is too much !)',
'Champs à afficher'
 => 'Fields to be dsplayed',
'Les flux de syntication RSS permettent aux visiteurs de suivre vos récente acquisition
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#rss">Plus d\'explications</a>)'
 => 'The RSS feeds allow readers to subscribe to your last acquired entries
    (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#rss">More details</a>)',
'L\'aperçu ne fonctionne pas. Il vous faut enregistrer le modèle puis tester avec
    votre lecteur de flux habituel'
 => 'The pre-display do not work. You have to save the template and to test with
    your usual feed reader',
'Conseillé : 1000x120, les proportions seront conservées'
 => 'Suggested : 1000x120, the ratios will be kept',
'Catégorie(s)'
 => 'Categor(y/ies)',
'Source'
 => 'Source',
'source'
 => 'source',
'Informations diverses'
 => 'Miscellaneous informations',
'Texte long (description)'
 => 'Long text (description)',
'description'
 => 'description',
'Cette page permet de faire des recherches avancées dans la collection (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#search">plus d\'explications</a>)'
 => 'This page allow to make advanced search in the collection(<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#search">more details</a>)',
'Champ pour les recherches de type chaine de caractère (contient le mot, est ..)'
 => 'Field for the searchs of string (word contain, is ...)',
'champ'
 => 'field',
'Champ pour les recherches de type liste (affichera une liste multichoix permetant de choisir un ou plusieurs éléments)'
 => 'Field to for the searchs of list type (will display a pulldown list allowing to choose an item)',
'Champ pour les recherches de type numérique (permet de faire des recherches dans un intervale)'
 => 'Field for the searchs of number (to be able to make search like smaller than, greater than ...)',
'Multicritères'
 => 'Multi criteria',
'Les critères vides seront ignorés.'
 => 'The empty fields will be ignored',
'Les dates peuvent être entrée sous la forme %s, 2007 (considérer comme 1er janvier 2007), ...'
 => 'Date entries can be input in the format %s, 2007 (considered as January 1st 2007), ...',
'Dans les listes à choix multiples pour séléctionner plusieurs éléments maintenez pressé la touche contrôle.'
 => 'For multiple selections on a list, press and hold the "control" key.',
'Requête'
 => 'Request',
'Il est possible de réaliser une requête composée de plusieurs conditions afin d\'effectuer une recherche encore plus précise'
 => 'It is possible to create a multi conditions request to be able to have a more specific search',
'Chercher dans'
 => 'Search in',
'toute la base'
 => 'the all base',
'les resultats précédents'
 => 'previous results',
'Champs contenant du texte'
 => 'Fields with text string',
'contient'
 => 'contains',
'ne contient pas'
 => 'do not contain',
'est (chaîne exacte)'
 => 'is (exact string)',
'n\'est pas (chaîne exacte)'
 => 'is not (exact string)',
'Champs numériques et dates'
 => 'Numeric fields and dates',
'égal'
 => 'equal',
'différent'
 => 'different',
'plus petit'
 => 'smaller',
'plus petit ou égal'
 => 'smaller or equal',
'plus grand'
 => 'greater',
'plus grand ou égal'
 => 'greater or equal',
'Ajouter la condition'
 => 'Add the condition',
'Permet d\'exporter le liste sous forme de fichier texte brut (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#txt">plus d\'explications</a>)'
 => 'Allow to export the list as a raw text file (<a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/generateur_de_theme#txt">more details</a>)',
'Lors de la prévisualisation et de l\'enregistrement des messages du type %s ainsi que des "%s" et "~%s" vont s\'afficher <strong>ceux-ci n\'ont aucune importance</strong>'
 => 'While pre-display or saving some messages of %s type and "%s" or "~%s" will be displayed <strong>ignore them</a>',
'Ligne type (certain champs peuvent être vides, les valeurs des menus déroulants et le textes ne seront pas séparés par un espace si vous n\'en avez pas mis.)'
 => 'Line exemple (some fields may be empty, the values of pulldown menus and the texts will not be splitted by space if you did not add any.)',
));
?>
