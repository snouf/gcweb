<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centrale de la page d'accueil du site
 * Variable disponible : $info, $collec (de la 1ère collection), $items (de la 1ère collection)
 */
?>
<div id="content">

    <div id="main">
        <?php
        foreach ($info['collections'] as $collec) {
            /*** BOUCLE SUR LES COLLECTIONS ******************************/

            $collec['filter'] = 'none';
            $collec['sort'] = 'none';

            if (!$collec['private']) { #regarde si c'est une collection privée
                if (file_exists(dirname(__FILE__).'/main_'.$collec['type'].'.php')) {
                    //Chargement de information de la collection
                    $itemsSortByDate = bdd::filterSortSliceAndCache('none','addedDSC',0,10,True);
                    ?>
                    <div class="element">
                        <div class="box">
                            <div class="collecdesc">
                                <h2><a name="collec<?php aff($collec['id'])?>" href="<?php aff_hrefmodel('list') ?>"><?php aff($collec['title']) ?></a></h2>

                                <p><?php aff($collec['description']) ?></p>

                                <h3><?php echo __('Informations') ?></h3>
                                    <ul>
                                        <li><?php echo __('Type de collection') ?> : <?php aff($collec['type']) ?></li>
                                        <li><?php echo __('Nombre d\'éléments') ?> : <?php aff($collec['nbItemsBDD']) ?></li>
                                        <li><?php echo __('Dernière mise à jour') ?> : <?php aff(strftime(__('%A %e %B %Y'),filemtime(DIR_GCWEB.'collections/'.$collec['dir'].$collec['xml']))) ?></li>
                                    </ul>

                                <p>→ <a href="<?php aff_hrefModel('list') ?>"><?php echo __('Voir la collection') ?></a></p>
                            </div>


                            <div class="colleclastadd">
                                <div class="label"><?php echo __('Derniers éléments ajoutés') ?> <a class="rss" title="<?php echo __('S\'abonner (RSS 2.0)') ?>" href="<?php aff_hrefModel('rss')?>"><span>RSS</span></a> :</div>
                                <div class="img">
                                    <?php
                                    foreach ($itemsSortByDate as $lastItem) {
                                        include dirname(__FILE__).'/main_'.$collec['type'].'.php';
                                    } ?>
                                </div>
                            </div>
                            <div class="collecfoot">
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo '
                        <div class="element">
                            <div class="box">
                                <h2>'.__('Collection inconnue pour le thème').'</h2>
                                <p>'.
                                sprintf(__('Les fichiers de thème de cette collection n\'ont pas encore été créés. <a href="%s&amp;generator=True">Cliquez ici</a> pour les créer'),hrefmodel('main'))
                                .'</p>
                            </div>
                        </div>';
                }
            }
        } ?>
    </div>
</div>
