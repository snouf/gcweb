<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Partie centale de la page qui affiche les infomration détaillée d'un élement.
 * Variable disponible : $info, $collec, $item
 */
?>
<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="item">
        <div class="nav">
            <div class="box">
                <?php $maxkey = count ($bdd) -1;

                //Items précédentes
                if ($item['key'] > 0) {
                    foreach (array_slice($bdd, max($item['key']-2, 0), min($item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['title']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['image'],'auto',80) ?>" <?php aff_attrsize_image($tmpitem['image'],'auto',80) ?> alt="-2" />
                        </a><?php
                    }
                }

                //Item courant
                ?><a id="id_<?php aff($item['id']) ?>" title="<?php aff($item['title']) ?>" href="<?php aff_hrefitem($item)?>">
                <img class="image" src="<?php aff_image($item['image'],'auto',120) ?>"<?php aff_attrsize_image($item['image'],'auto',120) ?>  alt="<?php aff($item['title']) ?>" />
                </a><?php

                //Item suivant
                if ($item['key'] < $maxkey) {
                    foreach (array_slice($bdd, min($item['key']+1, $maxkey), min($maxkey - $item['key'], 2)) as $tmpitem) {?>
                        <a id="id_<?php aff($tmpitem['id']) ?>" title="<?php aff($tmpitem['title']) ?>" href="<?php aff_hrefitem($tmpitem['id'])?>">
                        <img class="image" src="<?php aff_image($tmpitem['image'],'auto',80) ?>"<?php aff_attrsize_image($tmpitem['image'],'auto',80) ?>  alt="-2" />
                        </a><?php
                    }
                }
                ?><br />
                <a title="<?php echo __('Parcourir') ?>" href="<?php aff_hrefPage($item['key']/$conf['itemsPage']+.5) ?>#id_<?php aff($item['id']) ?>">↑</a> &nbsp;
                <?php
                aff_prevItem(5,' - ');
                aff_currentItem();
                aff_nextItem(5,' - ');
                ?>
            </div>
        </div>
        <div class="element<?php if(test($item['borrower']) & (convert($item['borrower']) != 'none')) echo ' lent lent_'.$info['lang']; ?>">
            <div class="box">
                <span class="image">
                <?php if (test($item['backpic'])) { ?>
                    <a id='img1' href="<?php aff_image($item['image']) ?>">
                        <img src="<?php aff_image($item['image'],'auto',360) ?>" <?php aff_attrsize_image($item['image'],'auto',360) ?> alt="<?php printf(__('Couverture de %s'),convert($item['title'])) ?>" />
                    </a>
                    <a id='img2'style="display:none"  href="<?php aff_image($item['backpic'],'auto',360) ?>">
                        <img title="<?php echo __('Cliquez dans le coin inférieur droit pour voir une autre image') ?>" class="image" src="<?php aff_image($item['backpic'],'auto',360) ?>" <?php aff_attrsize_image($item['backpic'],'auto',360) ?> alt="<?php echo sprintf(__('Couverture de %s'),convert($item['title'])) ?>" />
                    </a>
                    <a class='backpic' title="<?php echo __('Suivant') ?>" href="#" onclick="switchimg(2);return False;">↻</a>
                <?php } else { ?>
                    <a href="<?php aff_image($item['image']) ?>">
                        <img class="image" src="<?php aff_image($item['image'],'auto',360) ?>" <?php aff_attrsize_image($item['image'],'auto',360) ?> alt="<?php printf(__('Couverture de %s'),convert($item['title'])) ?>" />
                    </a>
                <?php } ?>
                </span>

                <?php if(test($item['borrower']) & (convert($item['borrower']) != 'none')) echo '<span id="lent"></span>'; ?>

                <h2><?php aff($item['title'])?></h2>
                <?php if (test($item['original'])){?> <h3 class="aka">a.k.a. <?php aff($item['original'])?></h3> <?php } ?>

                <?php if ($item['synopsis'] != $info['champVide'])   {?><p><?php aff($item['synopsis']) ?></p> <?php } ?>
                <span class="starNote"><?php aff_star($item['rating']) ?></span>
                <ul>
                    <?php
                        if (test($item['director']))    echo '<li><span class="label">'.__('Réalisateur').' :   </span><span class="info-item">'.filter('director==',$item['director']).'</span></li>';
                        if (test($item['genre']))       echo '<li><span class="label">'.__('Genre').' :         </span><span class="info-item">'.filter('genre==',$item['genre']).'</span></li>';
                        if (test($item['date']))        echo '<li><span class="label">'.__('Sortie').' :        </span><span class="info-item">'.filter('date==',$item['date']).'</span></li>';
                        if (test($item['country']))     echo '<li><span class="label">'.__('Pays').' :          </span><span class="info-item">'.filter('country==',$item['country']).'</span></li>';
                        if (test($item['actors'])) {
                            if (is_array($item['actors'][0])) {
                                echo '<li>
                                    <span class="label">'.__('Acteurs').' :</span>
                                    <table class="info">';
                                foreach ($item['actors'] as $actor) {
                                    echo '
                                        <tr>
                                            <td>'.filter('actors_without_roles==',$actor[0]).'</td>
                                            <td>'.(isset($actor[1]) ? convert($actor[1]) : '').'</td>
                                        </tr>';
                                    }
                                echo '
                                        </table>
                                    </li>';
                            } else {
                                echo '<li><span class="label">'.__('Acteurs').' :       </span><span class="info-item">'.filter('actors_without_roles==',$item['actors_without_roles']).'</span></li>';
                            }
                        }
                        $link = array();
                        if (test($item['webPage']))     $link[] = '<a href="'.$item['webPage'].'">'.__('Source').'</a>';
                        if (test($item['original']))    $link[] = '<a title="Internet Movie Database" id="imdb_link" href="http://imdb.com/find?s=tt&amp;q='.str_replace('$nbsp$','+',$item['original']).'">IMDb</a>';
                        elseif (test($item['title']))   $link[] = '<a title="Internet Movie Database" id="imdb_link" href="http://imdb.com/find?s=tt&amp;q='.str_replace('$nbsp$','+',$item['title']).'">IMDb</a>';

                        if (count($link) != 0)          echo '<li>'.convert($link,', ',', ').'</li>';
                    ?>
                </ul>
                <?php if (isset($conf['commercialLink'])) {?>
                    <?php aff($conf['commercialLink']) ?>
                <?php } ?>

                <?php if (test($item['comment'])) { ?>
                <h3><?php echo __('Commentaires') ?></h3>
                <p class="comments"><?php aff($item['comment']) ?></p>
                <?php }

                echo join("\n", $item['array_add_to_all_pages']);
                echo join("\n", $item['array_add_to_page_detail']);
                ?>
                <div class="foot">

                </div>
            </div>
        </div>
    </div>
</div>
