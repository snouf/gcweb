<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page de recherche.
 * Variable disponible : $info, $collec, $item
 */
?>

<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="search">

        <div class="element">
            <div class="box">

                <h2><?php echo __('Recherche avancée') ?></h2>

                <fieldset><legend><?php echo __('Multicritères') ?></legend>

                <?php aff_search('start') ?>

                <ul>
                    <li><?php echo __('Les critères vides seront ignorés.') ?></li>
                    <li><?php printf(__('Les dates peuvent être entrée sous la forme %s, 2007 (considéré comme 1er janvier 2007), ...'),date(strtolower($conf['fomatDate']))) ?></li>
                    <li><?php echo __('Dans les listes à choix multiples pour séléctionner plusieurs éléments maintenez pressé la touche contrôle.') ?></li>
                </ul>

                <div class="searchgroup searchstring">
                    <fieldset><legend><?php echo __('Titre') ?></legend>
                        <p><?php aff_search('title','str') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Nom de la piste') ?></legend>
                        <p><?php aff_search('tracks','str') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Commentaires') ?></legend>
                        <p><?php aff_search('comments','str') ?></p>
                    </fieldset>
                </div>

                <div class="searchgroup searchlistmultiple">
                    <fieldset><legend>
                            <?php echo __('Artiste') ?></legend>
                        <p><?php aff_search('artist','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Genre') ?></legend>
                        <p><?php aff_search('genre','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Format') ?></legend>
                        <p><?php aff_search('format','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Origine') ?></legend>
                        <p><?php aff_search('origin','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Compositeur') ?></legend>
                        <p><?php aff_search('composer','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Producteur') ?></legend>
                        <p><?php aff_search('producer','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Emplacement') ?></legend>
                        <p><?php aff_search('location','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Tags') ?></legend>
                        <p><?php aff_search('tags','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Label') ?></legend>
                        <p><?php aff_search('label','listmultiple') ?></p>
                    </fieldset>
                </div>

                <div class="searchgroup searchbetween">
                    <fieldset><legend><?php echo __('Ajouté à la collection entre (dates)') ?></legend>
                        <p><?php aff_search('added','>='); aff_search('&amp;added','<=') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Note entre') ?></legend>
                        <p><?php aff_search('rating','>=','',array('0*',1,2,3,4,5,6,7,8,9,10));  aff_search('&rating','<=','',array(0,1,2,3,4,5,6,7,8,9,'10*')) ?></td></p>
                    </fieldset>
                </div>

                <div class="searchgroup searchsubmit">
                    <p><?php aff_search('submit') ?></p>
                </div>

                <?php aff_search('end') ?>
                </fieldset>

                <fieldset><legend><?php echo __('Requête') ?> :</legend>
                <p><?php echo __('Il est possible de réaliser une requête composée de plusieurs conditions afin d\'effectuer une recherche encore plus précise') ?>.</p>
                <p>
                    <?php echo __('Chercher dans') ?>
                    <select id="js_and_or">
                        <option value="|"><?php echo __('toute la base') ?></option>
                        <option value="&amp;"><?php echo __('les resultats précédents') ?></option>
                    </select>
                    <select id="js_champ">
                      <optgroup label="<?php echo __('Informations générales') ?>">
                          <option value="title"><?php echo __('Titre') ?></option>
                          <option value="artist"><?php echo __('Artiste') ?></option>
                          <option value="label"><?php echo __('Label') ?></option>
                          <option value="composer"><?php echo __('Compositeur') ?></option>
                          <option value="producer"><?php echo __('Producteur') ?></option>
                          <option value="release"><?php echo __('Sortie') ?></option>
                          <option value="rating"><?php echo __('Note (0 à 10)') ?></option>
                      </optgroup>
                      <optgroup label="<?php echo __('Informations diverses') ?>">
                          <option value="origin"><?php echo __('Origine') ?></option>
                          <option value="location"><?php echo __('Emplacement') ?></option>
                          <option value="tags"><?php echo __('Tags') ?></option>
                          <option value="genre"><?php echo __('Genre') ?></option>
                          <option value="format"><?php echo __('Format de l\'album') ?></option>
                          <option value="source"><?php echo __('Source des informations') ?></option>
                          <option value="comment"><?php echo __('Commentaire') ?></option>
                      </optgroup>
                      <optgroup label="<?php echo __('Dates') ?>">
                          <option value="added"><?php echo __('d\'ajout') ?></option>
                          <option value="publication"><?php echo __('de publication') ?></option>
                          <option value="year"><?php echo __('année de publication') ?></option>
                      </optgroup>
                      <optgroup label="<?php echo __('Status/emprunts') ?>">
                          <option value="read"><?php echo __('Lu ?') ?></option>
                          <option value="borrowings"><?php echo __('Emprunté ?') ?></option>
                          <option value="borrower"><?php echo __('Emprunteur') ?></option>
                          <option value="lentDate"><?php echo __('Date d\'emprunt') ?></option>
                      </optgroup>
                  </select>
                    <select id="js_cond">
                        <optgroup label="<?php echo __('Champs contenant du texte') ?>">
                            <option value="="><?php echo __('contient') ?></option>
                            <option value="!="><?php echo __('ne contient pas') ?></option>
                            <option value="=="><?php echo __('est (chaîne exacte)') ?></option>
                            <option value="!=="><?php echo __('n\'est pas (chaîne exacte)') ?></option>
                        </optgroup>
                        <optgroup label="<?php echo __('Champs numériques et dates') ?>">
                            <option value="==">= <?php echo __('égal') ?></option>
                            <option value="!==">&ne; <?php echo __('différent') ?></option>
                            <option value="&lt;">&lt; <?php echo __('plus petit') ?></option>
                            <option value="&lt;=">&le; <?php echo __('plus petit ou égal') ?></option>
                            <option value="&gt;">&gt; <?php echo __('plus grand') ?></option>
                            <option value="&gt;=">&ge; <?php echo __('plus grand ou égal') ?></option>
                        </optgroup>
                    </select>
                    <input id="js_value" type="text" />
                    <input type="button" value="<?php echo __('Ajouter la condition') ?>" onclick="search_addcond('request')" />
                </p>
                <?php aff_search('start') ?>
                <p><?php aff_search('request','','rows="60" cols="20" id="request"') ?></p>
                <p><?php aff_search('submit') ?></p>
                <?php aff_search('end') ?>
                </fieldset>
            </div>
        </div>
    </div>
</div>
