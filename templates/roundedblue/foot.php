<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie se trouvant en bas de chaque page
 *   - Cette partie n'est pas mise en cache
 *   - Seul les variables du tableau $info peuvent être utilisé
 */
?>

    <div id="foot">
        <div class="box">
            <div class="left">
                <p>
                    <?php echo __('<span class="strong">Note :</span> Certaines informations peuvent provenir de sources externes. Pour les connaître, cliquez sur le lien "source" de l\'élément') ?>.
                </p>
                <p>
                    <?php echo __('Propulsé par') ?> <a href="http://jonas.tuxfamily.org/wiki/gcweb">GCweb</a>
                </p>
            </div>
            <div class="right">
                <p>
                <a href="http://validator.w3.org/check?uri=referer"><img
                    src="<?php echo URL_GCWEB ?>templates/<?php echo $conf['template'] ?>/img/valid-xhtml10-blue.png"
                    alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
                </p>
            </div>
        </div>
    </div>
</body>
</html>
