<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Page précédente'
 => 'Previous page',
'Page suivante'
 => 'Next page',
'Précédent'
 => 'Back',
'Suivant'
 => 'Next',
'Erreur : champ "%s" introuvable'
 => 'Error : unknown "%s" field',
'Erreur : ce champs comporte plusieurs lignes ET plusieurs colonnes. Le filtrage sur ce type éléments est impossible'
 => 'Error : This field has more than one line AND more than one column. Filtering this type of element is impossible.',
'Erreur : le tableau des liens et le tableau des textes à afficher ne comporte pas le même nombre d\'élément  (%1$d contre %2$d)'
 => 'Error : table of links and table of texts which should be displayed do not have the same number of elements (%1$d versus %2$d)',
'Erreur : champ introuvable'
 => 'Error : unknown field',
'Erreur : image de remplacement non trouvée'
 => 'Error : spare image not found',
'chercher'
 => 'search',
'Le 2ème argument de search doit être un array(...)'
 => 'Second argument of "search" function must be of type "array(...)"',
'contient'
 => 'contains',
'ne contient pas'
 => 'does not contain',
'est'
 => 'is',
'n\'est pas'
 => 'is not',
'Non'
 => 'No',
'Oui'
 => 'Yes',
'Autres'
 => 'Others',
'Vous semblez utiliser Internet Explorer. Ce navigateur respectant
                        très mal les standards du web il est possible que les pages de ce site
                        ne s’affichent pas correctement.<br />
                        Merci de prendre le temps d\'essayer
                        <a href="http://fr.wikipedia.org/wiki/Liste_de_navigateurs_web">un autre navigateur</a>
                        (par exemple <a href="http://www.mozilla-europe.org/fr/products/firefox/">firefox</a>).'
 => 'It seems that you are using Internet Explorer. This browser is not respectful
                      of web standards, so it is possible that the
                      web pages will not be displayed properly.<br />
                      Please take the time to try
                       <a href="http://en.wikipedia.org/wiki/List_of_web_browsers">another browser</a>
                     (eg : <a href="http://www.mozilla-europe.org/en/products/firefox/">Firefox</a>).',
));
?>
