<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Utilisateur ou mot de passe incorrect'
 => 'Incorrect username or password',
'Le nom d\'utilisateur et le mot de passe sont obligatoires'
 => 'Username and password are mandatory fields',
'Le fichier <code>/conf/config.php</code> et/ou son
            dossier <code>/conf</code> n\'est pas accessible en écriture. Changez les droits
            ou créez manuellement ce fichier en vous basant sur <code>/conf/config.example.php</code>'
 => 'The <code>/conf/config.php</code> file and/or <code>/conf</code> directory is not writable.
Change permissions or create this file manually (see <code>/conf/config.example.php</code>
for example).',
'Erreur dans le fichier de configuration'
 => 'Configuration file error',
'Enregistrement impossible'
 => 'Unable to save',
'Le fichier de configuration a été enregistré'
 => 'The configuration file has been saved',
'Le fichier de configuration n\'a pas été enregistré'
 => 'The configuration file has not been saved',
'n\'est pas une chaîne'
 => 'is not a string',
'n\'est pas un chiffre'
 => 'is not a digit',
'n\'est pas un vrai/faux'
 => 'is not a boolean',
'n\'est pas une liste au correctement formatée'
 => 'is not a correctly formatted list',
'n\'est pas d\'un type connu'
 => 'is not a known type',
'Fichiers de mise en cache des pages suprimées'
 => 'Cache files of pages deleted',
'Fichiers de mise en cache des images suprimées'
 => 'cache files deleted for the images',
'Le dossier racine n\'est pas inscriptible le renomage de
                    <code>config.php</code> ne peux être effectué. Renommez ce fichier à l\'aide de
                    votre client ftp.'
 => 'Root Folder Read/Write acces denied. Cannot rename file
               <code>config.php</code>     
You have to manually rename this file with your FTP Client.',
'Attention : nom de page invalide, renommage abandonné'
 => 'Warning: invalid page name, stop renaming',
'Configuration de GCWeb'
 => 'GCweb Configuration',
'Vos collections'
 => 'Your collections',
'wiki de GCweb'
 => 'GCweb wiki',
'thèmes pour GCweb'
 => 'GCweb themes',
'plugins pour GCweb'
 => 'GCweb plugins',
'actualités de GCweb'
 => 'GCweb news',
'Bienvenue dans la page de configuration de GCweb.'
 => 'Welcome to the GCweb configuration page',
'<h3>Avertissement</h3>

                <p>GCstar récupère des informations sur des sites internet divers, ce qui pour une
                utilisation privée ne pose généralement pas de problème. Cependant, en affichant
                ces informations sur votre site web, GCweb rend publiques ces informations.</p>

                <p>La personne installant GCweb doit vérifier que les informations mises en ligne
                ne sont pas protégées par des règles sur la propriété intellectuelle interdisant
                ce type d\'utilisation.</p>

                <p>Note : avec les thèmes officiels les sources sont citées par l\'intermédiaire du lien "sources".
                Cependant, avec des thèmes alternatifs, cette information peut être masquée.</p>'
 => '<h3>Disclaimer</h3>

     <p>GCstar is pulling informations from many Internet websites,     for personal usage this is not an issue.
     However, by displaying those data on your own website     GCweb makes it public.</p>

     <p>The person installing GCweb must check that the information     put online are not protected by any intellectual property laws     which would keep from this way of use.</p>

     <p>Note : with the official themes the information sources     are accessible via the "sources" link.
     However, with the alternative themes, the sources may be hidden</p>',
'En cochant cette case vous attestez avoir pris connaissance de ces informations'
 => 'By selecting this check box you confirm that you agree with this disclaimer',
'Prérequis'
 => 'Prerequisite',
'Version de PHP (5 ou plus)'
 => 'PHP version (5 or above)',
'votre version est'
 => 'you version is',
'Consultez
                    la documentation de votre hébergeur, vous pouvez probablement
                    activer PHP5'
 => 'Check
                 the documentation of your ISP, you would probably be able
                 to activate PHP5',
'Les "magic_quotes_runtime()" semble être activé et leur désactivation
                    est impossible. Le générateur de fichier de configuration (cette page) et le générateur
                    de thème ne fonctionneront pas mais les autres fonctionnalités (utilisation normale) ne
                    poseront pas de problème. Pour créer vos fichier de configuration consultez
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Ffichier_de_configuration">
                        la documentation
                    </a>.'
 => 'The "magic_quotes_runtime()" seem to be activated and it is not possible
   to deactivate them.
   The configuration file generator (this page) and the theme   generator will not work but the other functionalities    (normal usage) will work properly.
   To be able to create your configuration files refer to
   <a href="http://jonas.tuxfamily.org/wiki/gcweb/documentation/fichier_de_configuration">the documentation</a>',
'Les bibliotèques GD sont présentes et permettront de redimensionner les images de type'
 => 'The GD libraries are available and so will allow to resize the images of type',
'Les bibliotèques GD sont absentes, GCweb ne pourra pas fonctionner'
 => 'The GD libraries are not available. GCweb will not work',
'Le dossier "<code>/conf</code>" n\'est pas inscriptibles. Le fichier de
                    configuration ne pourra pas être enregistré'
 => 'The  "<code>/conf</code>" folder is not writable.
                  The configuration file will not be saved',
'L\'un ou plusieurs de ces fichiers ne sont pas inscriptible :'
 => 'One ore more of the following files are read-only :',
'Le dossiers "<code>/cache</code>" et ses sous-dossiers doivent être inscriptible sinon GCweb
                    ne pourra pas fonctionner (sauf si vous désactivé le complêtement le cache mais ceci est déconseillé)'
 => 'The  "<code>/cache</code>" folder is not write access. GCweb will not
                 be able to work (except if you are completely deactivating the cache, but this is not suggested',
'Aucun fichier de thème n\'a été trouvé'
 => 'No theme file found',
'Aucun fichier de sauvegarde GCstar n\'a été trouvé.
                    Envoyez votre/vos fichier/s de sauvergarde dans le dossier
                    <code>/collections</code> à l\'aide de votre client ftp favori'
 => 'No GCstar backup file was found
                  Send your backup file(s) in the the <code>/collections</code>
                  folder with help of your prefered ftp client.',
'Aucun dossier d\'images n\'a été trouvé, envoyez votre/vos
                    <strong>dossier/s</strong> contenant les images de votre/vos collection/s
                    dans le dossier <code>/collections</code> à l\'aide de votre client ftp
                    favori'
 => 'No images folder was found, send your images <strong>folder(s)</strong>
                  in the <code>/collections</code> folder with your prefered ftp client',
'Attention : Le model de collection %s est inconnu de GCweb. Veuillez copier <a href="./?redirect=http%%3A%%2F%%2Fwiki.gcstar.org%%2Ffr%%2Fuser_models"> modèle de collection GCstar</a> dans le dossier <code>/conf/GCmodels/</code> de GCweb et réenregistrer cette page.'
 => 'Warning : Collection Template %s is unknown to GCweb. Please copy
<a href="./?redirect=http%:%/%/wiki.gcstar.org%/fr%/user_models"> GCstar Collection Template</a> to GCweb\'s folder <code>/conf/GCmodels/</code> and save this page again.',
'Revérifier'
 => 'Recheck',
'Fichier de configuration consultable'
 => 'Accessible configuration file',
'<p>Un fichier <code>config.php</code> se trouve à la racine de
                l\'installation de GCweb. Afin d\'empêcher la consultation de cette page et
                d\'augmenter la sécurité de GCweb, il est conseillé de renommer ce fichier.
                Cette page sera ensuite consultable via l\'url
                <code>http://votre_site.tdl/gcweb/nom_du_fichier.php</code>.<br />
                (Notez que pour encore plus de sécurité vous pouvez supprimer ce fichier
                une fois la configuration terminée).</p>'
 => '<p>a <code>config.php</code> file is present at the root of GCweb installation.
              To avoid access to this page and to improve the GCweb security, it is
               suggested  to rename this file.
              This page will still be accessible via the url
                <code>http://votre_site.tdl/gcweb/New_name.php</code>.<br />
               (Note that for higher security you should delete the file
                 after configuration completion).</p>',
'Nouveau nom'
 => 'New name',
'Entrez le nom la page <strong>sans</strong> l\'extention <code>.php</code> et
                    <strong>sans</strong> caractères spéciaux'
 => 'Give the page name <strong>without</strong> the <code>.php</code> extension
             and <strong>without</strong> any special character',
'Général'
 => 'General',
'Titre du site'
 => 'Site title',
'Description'
 => 'Description',
'Code html possible'
 => 'html code allowed',
'Utilisateur'
 => 'User id',
'Mot de passe'
 => 'Password',
'laissez-le vide pour ne pas le changer'
 => 'keep it empty to not change it',
'Langue de l\'interface'
 => 'Language of the interface',
'Thème'
 => 'Theme',
'Visionner et télécharger des thèmes'
 => 'View and download themes',
'Nombre d\'éléments par page'
 => 'Number of elements per page',
'Champs vide'
 => 'Empty fields',
'Quand un élément n\'a pas été complété dans GCstar. GCweb le remplacera
                    par cette chaîne'
 => 'When an element is empty in GCstar.
GCWeb will replace it with this string',
'Non à MSIE'
 => 'no-MSIE',
'Affiche un message sur internet explorer précisant que
                    ce navigateur respecte très mal les standards du web et qu\'il est possible que les
                    pages du site s\'affichent mal. Vous pouvez remplacer le message par défaut en
                    rédigeant votre propre message ci-dessous'
 => 'Display a message on internet explorer to specify that
                    this browser is not respectful of web standards and that it is possible that the
                   web pages will not be displayed properly. You are able to replace the default message
                   by your own message text below',
'Texte personnalisé NonMSIE'
 => 'NonMSIE personal text',
'Générateur'
 => 'Generator',
'Permet de créer les pages de thème pour un type de collection non
                    supportée ou d\'en remplacer une qui ne vous plait pas
                    (<a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fconfiguration%23Ordre_de_tri+par_defaut">plus d\'information</a>).
                    Activez cette fonctionnalité uniquement quand vous en avez
                    l\'utilité'
 => 'Allow to create theme pages for a collection type that is not supported by default
                 Enable this functionality only when you need it',
'Cette collection serra supprimée lors de l\'enregistrement'
 => 'This collection will be deleted at saving time',
'Annuler la suppression'
 => 'Stop deletion',
'Supprimer cette collection'
 => 'Delete this collection',
'La collection ne serra plus affichée par GCweb mais
                        les fichiers composant celle-ci ne seront pas supprimés'
 => 'The collection will not be displayed by GCweb but
                      the related files will not be deleted',
'Collection'
 => 'Collection',
'Titre de la collection'
 => 'Collection Title',
'Nom du fichier de sauvegarde'
 => 'Name of the backup file',
'Collection privée'
 => 'Private collection',
'Si cochée, la collection sera cachée mais toujours accessible via'
 => 'If checked, the collection will be hidden but still accessible via',
'son url direct'
 => 'its direct url',
'Attention, les visiteurs peuvent deviner très facilement cette url !'
 => 'Warning, a user may guess very easily this url !',
'Dossier contenant les images'
 => 'Folder containing the images',
'Ordre de tri par défaut'
 => 'Default sort order',
'idASC fonctionnera avec tous les types de collection et correspond
                        à l\'ordre dans lequel vous avez ajouté les éléments. Utilisez
                        idDSC pour les avoir dans l\'ordre inverse. Il est égallement possible de les trier
                        selon <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fgenerateur_de_theme">
                        un ou plusieurs autres champs</a>'
 => 'idASC will work with all types of collection and is associated
                     to the order used when the elements were added.
                     Use idDSC to have them sorted in reverse order. It is also possible to sort
                     as per <a href="http://jonas.tuxfamily.org/wiki/gcweb/configuration#Ordre_de_tri_par_defaut">
                     one or more other fields</a>',
'Ajouter une collection'
 => 'Add a collection',
'Avancée'
 => 'Advanced',
'Paramètres avancés de GCweb. Si vous ne savez pas ce que vous faites n\'y touchez pas !'
 => 'GCweb advanced parameters. If you do not know what to do, do nothing !',
'Paramètres de la mise en cache de la base de donnée'
 => 'Cache setting parameters for the database',
'Le traitement (filtrage et organisation) du fichier d\'enregistrement
                est une opération lourde. Afin d\'éviter que GCweb  nerefasse ce travail quand l\'utilisateur
                passe à la page suivante, la recherche est mise en cache'
 => 'The processing (filtering and ordering) of the saving file
                is a high  cpu load operation. To avoid GCweb to do this work when the user
                is moving to next page, the request is cached',
'Purger automatique tous les'
 => 'Purge automatically every',
'minutes les fichiers de cache de la base de donnée qui non pas servis depuis le
                    nombre de minutes spécifié ci-dessous'
 => 'minutes the cache files of the database that were not used for
                    the number of minutes specified below',
'Les fichiers non utilisés depuis'
 => 'The files not used since',
'minutes'
 => 'minutes',
'Ne pas utiliser de cache'
 => 'Do not use cache',
'Cocher cette case est déconseillé (économise l\'espace disque
                    mais consomme plus de ressources et affiche moins rapidement les pages)'
 => 'Checking this box is not suggested (reduce disk space usage
                but consume more resources and increase the time for page display)',
'Paramètres des images'
 => 'Images parameters',
'Qualitée des jpeg'
 => 'jpeg quality',
'De 0 - qualité médiocre et poids très faible - à 100 - excellente
                    qualitée, images lourdes'
 => 'From 0 poor quality and light files,
                  to 100 good quality, heavy files',
'Ne pas mettre en cache'
 => 'Do not use cache',
'Si activé, les images ne seront pas mises en cache. Il est fortement
                    déconseillé de cocher cette case. Vous économiserez de l\'espace-disque, mais
                    les ressources serveur seront très sollicitées et les images s\'afficheront très lentement,
                    voire partiellement'
 => 'If enabled, the images will not be placed in cache. It is strongly
                  discourage to check this box. You will gain disk-space, but
                  the server resources will be highly used and the images
                  will be displayed very slowly, or even only partially',
'Purge automatique tous les'
 => 'Automatically purge every',
'jours'
 => 'days',
'Les images non utilisées depuis'
 => 'The not used images since',
'Rééchantillonner les images'
 => 'Re sample the images',
'Si activé, crée des images de meilleure qualité.
                    Cependant cette fonction n\'est pas disponible chez tous
                    les hébergeurs. Désactivez cette option si la création des images ne
                    fonctionne pas ou est trop lente'
 => 'If enabled, generate images of improved quality
                However this function is not available for all providers
                Disable this option if images creation do not work or is too slow',
'Paramètres divers'
 => 'Miscellaneous parameters',
'Chaîne à ignorer'
 => 'String to be ignored',
'Liste des chaînes à ignorer lors du tri. ATTENTION
                    respectez rigoureusement la syntaxe (chaîne entre guillemet simple
                    séparée par une virgule et n\'oubliez pas de mettre l\'espace qui suit le déterminant
                    (n\'écrivez pas "<code>le</code>" mais écrivez "<code>le </code>",
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fconfiguration_avancee%23chaines_a_ignoree">plus d\'information</a>).'
 => 'List of the strings to be ignored while sorting. WARNING
            syntax to be strictly applied (string between simple quotes
            separated by comma, and do not forget to place an ending space character
            (for example do not write "<code>the</code>" but write "<code>the </code>",
                 <a href="http://jonas.tuxfamily.org/wiki/gcweb/configuration#Ordre_de_tri par_defaut">more details</a>).',
'Localisation'
 => 'Localisation',
'fr_FR.UTF8'
 => 'en_US.UTF8',
'Tapez le code de localisation (encodé en UTF-8) de votre pays. Ce code dépend du
                    système d\'exploitation sur lequel tourne le serveur de votre site web.
                    Vous pouvez laisser ce champs vide pour utiliser la paramêtre pas défaut
                    de votre serveur (<a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ffunction.setlocale.php">plus d\'information</a>).'
 => 'Enter the localisation code (UTF-8 encoding) of your country. This code is function of
                 the operating system that is running on the server of your web site.
                 You can keep this field empty to be able to use the default setting
                 of your server (<a href="http://fr.php.net/manual/fr/function.setlocale.php">
                  more details</a>).',
'Fuseau horaire'
 => 'Time zone',
'Europe/Paris'
 => 'Europe/Paris',
'Selon la <a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ftimezones.php">
                    liste des fuseaux horaires</a> supportée par PHP.'
 => 'As per<a href="http://fr.php.net/manual/fr/timezones.php">
           list of time zones</a> supported by PHP.',
'Format des dates GCstar'
 => 'Format of date in GCstar',
'D/M/Y'
 => 'Y/M/D',
'GCstar entregistre la date des éléments sous forme de chaînes (jj/mm/aa par exemple).
                    Sur cette base, le tri par ordre chronologique est impossible. GCweb peut essayer d\'interpréter
                    ces dates.'
 => 'GCstar saves an entry\'s date as a String (e.g dd/mm/yy)
Based on this, it is impossible to sort entries on their date.
GCweb tries to guess these dates.',
'Purge des fichiers de mise en cache'
 => 'Purge the cache files',
'Supprimera les fichiers de mise en cache lors de l\'enregistrement'
 => 'Will delete the cache files at saving time',
'De la base de donnée'
 => 'For the database',
'Des images'
 => 'For the images',
'Enregistrement'
 => 'Saving',
'Sauver les paramètres'
 => 'Save the parameters',
));
?>
