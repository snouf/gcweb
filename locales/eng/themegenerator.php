<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Erreur'
 => 'Error',
'Générateur désactivé ! Activez le via la page de configuration'
 => 'Generator disabled! Enable it trough the configuration page',
'Balise de fermeture &lt;/champ&gt; non trouvée'
 => 'Closing tag </field> not found',
'début de chaine :'
 => 'early string :',
'Attention, la page contient des erreurs, celle-ci ne sont
                peut-être sans importance mais vérifier que la page s\'affiche
                correctement en masquant le générateur.'
 => 'Warning, the page contains errors, they may be 
                irrelevant but make sure the page properly hide 
                the generator.',
'<p><strong>Attention</strong> : une ou plusieurs erreurs ont été
                détectée lors de l\'execution de la page créer par le générateur.</p>

                <p>Si une ou plusieurs lignes du type &ldquo;<code>...
                <strong>eval()\'d code</strong> on line <strong>&lt;no&gt;</no></strong>
                </code>&rdquo; s\'affichent repporté le numéro &lt;no&gt; dans le code
                ci-dessous pour trouvé l\'erreur.</p>

                <p>Si vous n\'arrivez pas à résoudre votre problème posez votre question sur le
                forum de <a href="http://jonas.tuxfamily.org">http://jonas.tuxfamily.org</a>.</p>'
 => '<p><strong>Warning</strong> : one or more errors were detected
               while executing the page created by the generator.</p>

               <p>If one or more lines similar to “<code>...
               <strong>eval()\'d code</strong> on line <strong><no></no></strong>
                </code>” appear, go to line <no> in the code
                below to find the error.</p>

               <p>If you can not resolve your problem, ask questions to
the forum <a href="http://jonas.tuxfamily.org">http://jonas.tuxfamily.org</a>.</p>',
'Dernière erreur interceptée :'
 => 'Last error caught :',
'Code php évalué'
 => 'evaluated php code',
'Tableau des valeurs postées'
 => 'Table of posted values',
'La page à été enregistrée avec succès.'
 => 'The page was successfully saved',
'Mot de passe ou utilisateur non valide.'
 => 'Incorrect password or username.',
'Afficher le générateur'
 => 'Display the generator',
'Générateur de modèles de pages'
 => 'Page templates generator',
'Le générateur à été lancé car ce thème n\'est pas adapté à ce type
        de collection. Pour l\'adapter créer toute les fichiers ci-dessous en
        complétant de simples formulaires.'
 => 'The generator was started because this theme do not match this type
       of collection. You have to create all files below by filling
       simple forms.',
'Information sur les différent modèle d\'affichage du thème (cliquez dessus pour les (re)créer).'
 => 'Information about the different templates display of the theme (click it to (re)create).',
'recréer ce modèle'
 => 'recreate this template',
'créer ce modèle'
 => 'create this template',
'pas de fichier model'
 => 'no model file',
'Informations techniques sur le modèle d\'affichage en cours de création :'
 => 'Technical informations on the currently generated template of display :',
'Modèle d\'affichage'
 => 'Template of display',
'Collection de type'
 => 'Collection of type',
'Basé sur le modèle'
 => 'Based on the template',
'À enregistrer dans'
 => 'To save',
'Création du model "%s"'
 => 'Creation of the "%s" model',
'Attention ! prévisualisation de la page. Elle n\'est pas encore enregistrée.'
 => 'Warning ! preview page. It is not yet saved.',
'Les champs vide seront ignoré lors de la génération de la page'
 => 'Empty fields will be ignored during page generation',
'Attention la page contient des erreurs'
 => 'Warning page has errors',
'masquer le générateur'
 => 'hide the generator',
));
?>
