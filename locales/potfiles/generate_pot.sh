#! /bin/sh
mypath=$(dirname $0)
cd "$mypath/../.."
mkdir -p locales/potfiles

deflist='inc/render.php inc/bdd.php'
mylist='config inc/func4tpl default inc/themegenerator'

for file in $mylist
    do
    filename=`basename "$file"`
    echo "Fichier $filename"
    if [ "x$file" = 'xdefault' ]; then
        xgettext -k__ -kN__ --no-wrap --from-code UTF-8 $deflist  -o locales/potfiles/default.pot
    else
        xgettext -k__ -kN__ --no-wrap --from-code UTF-8 $file.php -o locales/potfiles/$filename.pot
    fi
    for lang in "eng" "de" #add your language hier example : "eng" "de"
        do
        mkdir -p locales/$lang
        if [ -e locales/$lang/$filename.po ]; then
            msgmerge -U locales/$lang/$filename.po locales/potfiles/$filename.pot
        else
            msginit -i locales/potfiles/$filename.pot -o locales/$lang/$filename.po
        fi
    done
done

echo "

Bonne traduction !

Une fois celle-ci terminée ou pour tester executez po2phparray.sh"
