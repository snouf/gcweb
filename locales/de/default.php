<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'La collection %d n\'existe pas'
 => 'Die Sammlung %d existiert nicht',
'erreur : impossible de charger le xml %s.'
 => 'Fehler : Laden der xml-Datei %s nicht möglich.',
'Attention ! Il est impossible de trier un tableau
                selon plus de 6 clefs. Le tableau a été trié selon les 6 premières clefs'
 => 'Achtung ! Es ist nicht möglich eine Tabelle mit mehr
                als 6 Bedingungen zu sortieren. Die Tabelle wurde anhand der 6 ersten Bedingungen sortiert.',
'Erreur de syntaxe sur la condition "%s"'
 => 'Syntax-Fehler in der Bedingung "%s"',
'Le champs "%s" n\'existe pas, filtrage avec celui-ci impossible'
 => 'Das Feld "%s" existiert nicht, die Suche ist nicht möglich',
'Aucun élément n\'a été trouvé.'
 => 'Kein Objekt gefunden.',
'jan'
 => 'jan',
'janvier'
 => 'januar',
'fev'
 => 'feb',
'fevrier'
 => 'februar',
'mar'
 => 'mär',
'mars'
 => 'märz',
'avr'
 => 'apr',
'avril'
 => 'april',
'mai'
 => 'mai',
'juin'
 => 'juni',
'juil'
 => 'jul',
'juillet'
 => 'juli',
'aout'
 => 'august',
'sept'
 => 'sept',
'septembre'
 => 'september',
'oct'
 => 'okt',
'octobre'
 => 'oktober',
'nov'
 => 'nov',
'novembre'
 => 'november',
'dec'
 => 'dez',
'decembre'
 => 'dezember',
));
?>
