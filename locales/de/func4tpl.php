<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Page précédente'
 => 'Vorherige Seite',
'Page suivante'
 => 'Nächste Seite',
'Précédent'
 => 'Zurück',
'Suivant'
 => 'Nächste',
'Erreur : champ "%s" introuvable'
 => 'Fehler : Feld "%s" nicht gefunden',
'Erreur : ce champs comporte plusieurs lignes ET plusieurs colonnes. Le filtrage sur ce type éléments est impossible'
 => 'Fehler : Das Feld enthält mehrere Zeilen UND mehrere Spalten. Die Suche nach diesen Typ Element ist nicht möglich',
'Erreur : le tableau des liens et le tableau des textes à afficher ne comporte pas le même nombre d\'élément  (%1$d contre %2$d)'
 => 'Fehler : Die Tabelle der Links und die Tabelle der anzuzeigenden Texte enthalten nicht die gleiche Anzahl an Objekte (%1$d gegen %2$d)',
'Erreur : champ introuvable'
 => 'Fehler : Feld nicht gefunden',
'Erreur : image de remplacement non trouvée'
 => 'Fehler : Ersatzbild nicht gefunden',
'chercher'
 => 'Suchen',
'Le 2ème argument de search doit être un array(...)'
 => 'Das zweite Argument der  Suche muss ein array(...) sein',
'contient'
 => 'enthält',
'ne contient pas'
 => 'enthält nicht',
'est'
 => 'ist',
'n\'est pas'
 => 'ist nicht',
'Non'
 => 'Nein',
'Oui'
 => 'Ja',
'Autres'
 => 'Andere',
'Vous semblez utiliser Internet Explorer. Ce navigateur respectant
                        très mal les standards du web il est possible que les pages de ce site
                        ne s’affichent pas correctement.<br />
                        Merci de prendre le temps d\'essayer
                        <a href="http://fr.wikipedia.org/wiki/Liste_de_navigateurs_web">un autre navigateur</a>
                        (par exemple <a href="http://www.mozilla-europe.org/fr/products/firefox/">firefox</a>).'
 => 'Sie verwenden den Internet Explorer. Dieser Browser unterstützt
                        die Internet Standards nur bedingt. Es ist möglich dass der Inhalt
                        dieser Website nicht korrekt angezeigt wird.<br />
                        Bitte nehmen Sie sich die Zeit einen anderen Browser auszuprobieren
                        <a href="https://de.wikipedia.org/wiki/Liste_von_Webbrowsern">weiterere Browser</a>
                        (z.B: <a href="http://www.mozilla.com">firefox</a>).',
));
?>
