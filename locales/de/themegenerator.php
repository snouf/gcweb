<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Erreur'
 => 'Fehler',
'Générateur désactivé ! Activez le via la page de configuration'
 => 'Generator ausgeschaltet ! Aktivieren sie ihn auf der Konfigurationsseite',
'Balise de fermeture &lt;/champ&gt; non trouvée'
 => 'Schliessungstag &lt;/feld&gt; nicht gefunden',
'début de chaine :'
 => 'Anfang des Strings:',
'Attention, la page contient des erreurs, celle-ci ne sont
                peut-être sans importance mais vérifier que la page s\'affiche
                correctement en masquant le générateur.'
 => 'Achtung, die Seite enthält Fehler, diese können vielleicht
                unwichtig sein. Überprüfen Sie aber trotzdem die korrekte Anzeige
                der Seite indem sie den Generator verbergen.',
'<p><strong>Attention</strong> : une ou plusieurs erreurs ont été
                détectée lors de l\'execution de la page créer par le générateur.</p>

                <p>Si une ou plusieurs lignes du type &ldquo;<code>...
                <strong>eval()\'d code</strong> on line <strong>&lt;no&gt;</no></strong>
                </code>&rdquo; s\'affichent repporté le numéro &lt;no&gt; dans le code
                ci-dessous pour trouvé l\'erreur.</p>

                <p>Si vous n\'arrivez pas à résoudre votre problème posez votre question sur le
                forum de <a href="http://jonas.tuxfamily.org">http://jonas.tuxfamily.org</a>.</p>'
 => '<p><strong>Achtung</strong> : ein oder mehrere Fehler wurden bei
                der Erstellung dieser Seite durch den Generator gefunden.</p>

                <p>Falls eine oder mehrere Zeilen mit “<code>...
                <strong>eval()\'d code</strong> on line <strong>&lt;no&gt;&lt;/no&gt;</strong>
                </code>” angezeigt werden, notieren sie die Nummer &lt;no&gt; der Zeile
                um den Fehler zu finden.</p>

                <p> Falls sie das problem nicht lösen können fragen sie in unserem Forum
                <a href="http://jonas.tuxfamily.org">http://jonas.tuxfamily.org</a> nach.</p>',
'Dernière erreur interceptée :'
 => 'Letzte erhaltene Fehlermeldung',
'Code php évalué'
 => 'Ausgewerteter PHP-Code',
'Tableau des valeurs postées'
 => 'Tabelle der geschriebenen Werte',
'La page à été enregistrée avec succès.'
 => 'Die Seite wurde erfolgreich abgespeichert.',
'Mot de passe ou utilisateur non valide.'
 => 'Benutzer oder Passwort falsch',
'Afficher le générateur'
 => 'Generator anzeigen',
'Générateur de modèles de pages'
 => 'Musterseiten Generator',
'Le générateur à été lancé car ce thème n\'est pas adapté à ce type
        de collection. Pour l\'adapter créer toute les fichiers ci-dessous en
        complétant de simples formulaires.'
 => 'Der Generator wurde gestartet da diesem Design der Typ
        der Sammlung nicht bekannt ist. Um dieses anzupassen erstellen sie
        alle hier unten aufgelisteten Seiten indem sie die Formulare anpassen.',
'Information sur les différent modèle d\'affichage du thème (cliquez dessus pour les (re)créer).'
 => 'Informationen über die unterschiedliche Vorlagenanzeige des Designs (klicken Sie darauf, um sie (neu) zu erstellen).',
'recréer ce modèle'
 => 'Dieses Muster wieder erstellen',
'créer ce modèle'
 => 'Dieses Muster erstellen',
'pas de fichier model'
 => 'keine Musterdatei',
'Informations techniques sur le modèle d\'affichage en cours de création :'
 => 'Technische Informationen des Anzeigemuster werden erstellt',
'Modèle d\'affichage'
 => 'Anzeigemuster',
'Collection de type'
 => 'Typ der Sammlung',
'Basé sur le modèle'
 => 'Basiert auf dem Muster',
'À enregistrer dans'
 => 'Speichern unter',
'Création du model "%s"'
 => 'Erstellen des Musters "%s"',
'Attention ! prévisualisation de la page. Elle n\'est pas encore enregistrée.'
 => 'Achtung ! Vorschau der Seite. Diese ist noch nicht gespeichert.',
'Les champs vide seront ignoré lors de la génération de la page'
 => 'Leere Felder werden während der Erstellung der Seite ignoriert',
'Attention la page contient des erreurs'
 => 'Achtung die Seite enthält Fehler',
'masquer le générateur'
 => 'Generator verbergen',
));
?>
