<?php
/*
 *      This file is a part of GCstarWeb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Purgateur des fichiers cache
 */

$touchCacheBDD = PATH_GCWEB.'/cache/lastpurgebdd';
$touchCacheImg = PATH_GCWEB.'/cache/lastpurgeimg';
$time = time();
///*pour log*/$log = '';
if (!isset($forcePurgeCacheBDD))   $forcePurgeCacheBDD = False;
if (!isset($forcePurgeCacheImg))    $forcePurgeCacheImg  = False;

if (!$conf['noCacheBDD'] || $forcePurgeCacheBDD) {
    //Purge des pages en cache
    if (!file_exists($touchCacheBDD))  touch($touchCacheBDD);

    if (((filemtime($touchCacheBDD) + $conf['purgeCacheBDDDelay']*60) < $time) || $forcePurgeCacheBDD) {
        //Date de la dernière purge expirée, scan des fichiers
        touch($touchCacheBDD);
        ///*pour log*/$log .= date("c").' : purge cache pages  : ';
        $dir = PATH_GCWEB.'/cache/bdd/';
        $ls = scandir($dir);
        $unlinkpage = array();
        foreach ($ls as $file) {
            if (
                is_file($dir.$file) //c'est un fichier
                && (
                    $forcePurgeCacheBDD //demande de purge
                    || (
                        filectime($dir.$file) + $conf['purgeCacheBDDAge']*60 < $time //date périmée
                        && !in_array(substr($file,0,-3),$conf['nopurgeCacheBDD']) //et pas dans la liste des fichiers a ne pas purger
                        && !(substr($file,-9,-3) == 'static')  //et aucune indication de non purge dans le nom du fichier cache
                    )
                )
            ) {
                if (unlink($dir.$file)) $unlinkpage[] = $file;
                else                    $unlinkpage[] = "error $file can't delete";
            }
        }
        ///*pour log*/$log .= join(", ",$unlinkpage)."\n";
    }
}

if (!$conf['noCacheImg'] || $forcePurgeCacheImg) {
    //Purge des images en cache
    if (!file_exists($touchCacheImg))  touch($touchCacheImg);

    if (((filemtime($touchCacheImg) + $conf['purgeCacheImgDelay']*24*3600) < $time) || $forcePurgeCacheImg) {
        //Date de la dernière purge expirée, scan des fichiers
        touch($touchCacheImg);
        ///*pour log*/$log .= date("c").' : purge cache images : ';
        $dir = PATH_GCWEB.'/cache/images/';
        $ls = scandir($dir);
        $unlinkimage = array();
        foreach ($ls as $file) {
            if (is_file($dir.$file) && (((filectime($dir.$file) + $conf['purgeCacheImgAge']*24*3600) < $time) || $forcePurgeCacheImg)) {
                if (unlink($dir.$file)) $unlinkimage[] = $file;
                else                    $unlinkimage[] = "error $file can't delete";
            }
        }
        ///*pour log*/$log .= join(", ",$unlinkimage)."\n";
    }
}

//Si vous souhaitez un log des fichiers effacer decommantez ce qui suit et toute les ligne "///*pour log*/" qui se trouve dans la code ci-dessus
/*
if ($log != '') {
    //ecriture d'un fichier de log
    $fp = fopen(PATH_GCWEB.'/cache/log.txt','a');
    fwrite($fp,$log);
    fclose($fp);
}
*/
?>
