<?php
/*
 *      This file is a part of GCweb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

load_l10n('themegenerator');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
    <meta http-equiv="Content-Type" content="application/x-php;charset=UTF-8" />
<?php if (!$conf['generator']) {
    exit ('<title>'.__('Erreur').'</title>\n</head>
        <body><div>'.__('Générateur désactivé ! Activez le via la page de configuration').'</div></body>
        </html>');
} else { ?>
    <style type="text/css">
        #generator div#ask, #generator div#savepreview, #generator div#hide, div#showgenerator {
            position:fixed;
            background : #FFF;
            z-index : 100;
        }

        div#showgenerator {
            top:0; left:0;
            border-bottom:2px gray solid;
            opacity: .6;
            -moz-border-radius: 0 0 2em 0;
        }

        #showgenerator p {
            margin : 0.5em 1em;
        }

        #generator div#ask {
            bottom: 7em; right: 10%;
            left:0; top: 0em;
            padding: 1em;
            border-right: 2px gray solid;
            border-bottom: 1px gray solid;
            overflow:auto;
            z-index : 101;
        }

        #generator div#savepreview, #generator div#hide {
            height: 3em;
            border-bottom:2px gray solid;
            padding-top: 4em;
            padding-bottom: 1px;
            -moz-border-radius: 0 0 2em 0;
            bottom: 4.6em; right: 10%;
            z-index : 100;
        }

        #generator div#savepreview {
            left:0;
        }

        #generator div#hide, div#showgenerator {
            border-right: 2px gray solid;
        }

        #generator #hide p, #generator #savepreview p {
            margin:1em; padding:0;
        }


        #generator ul {
            margin-top : .2em;
        }
        #generator li {
            clear:both;
            min-height : 2em;
            margin-top: .5em;
        }

        #generator li.listfile {
            min-height : 0;
            margin: .2em 0;
        }

        #generator .generator_label {
            min-width: 14em;
            float: left;
            margin-top:.2em;
        }
        #generator .generator_label_large {
            min-width: 30em;
            float: left;
            margin-top:.2em;
        }

        #generator .listfile span {
            position:absolute; left:15em;
        }

        #generator span.form {
            position: absolute;
        }

        .ko, .ok {border-width: 2px ; border-style: solid; padding: 1em; margin:1em;-moz-border-radius : 2em;}
        .ko {border-color:red   ; background:#FFDBDB;}
        .ok {border-color:green ; background:#BAFFBA;}
    </style>
    <?php
    load_l10n('generator',$conf['lang']);
    load_template_l10n('generator',$conf['lang']);
    //import du contenu head du thème
    if (file_exists(PATH_GCWEB.'/templates/'.$conf['template'].'/head.php')){
        $fp = fopen(PATH_GCWEB.'/templates/'.$conf['template'].'/head.php', 'r');
        $fpsize = filesize(PATH_GCWEB.'/templates/'.$conf['template'].'/head.php');
    } else {
        $fp = fopen(PATH_GCWEB.'/templates/default/head.php', 'r');
        $fpsize = filesize(PATH_GCWEB.'/templates/default/head.php');
    }
    $headfile = fread($fp,$fpsize);
    fclose($fp);
    $start = strpos($headfile,'<head')+5;
    $start = strpos($headfile,'>',$start)+1;
    $end = strpos($headfile,'<body')+5;
    $end = strpos($headfile,'>',$end)+1;
    $head = substr($headfile, $start, $end-$start);
    echo eval('?>'.$head.'<?php ;');
    ?>

<?php
$msgGenerator = '';
/**********************************************************************
 * Fonction diverse
 **********************************************************************/
function generatorTag2inputHtml($str, $i='') {
    /*
     * Convertir les champs du formulaire du thème ("%%nom_champ%%") d'une
     * chaine en input/select pour formulaire.
     *
     * $str :   chaine à convertir
     * $i :     optionnel, pour les champs à numéroté
     */
    global $optionsSelect;

    if ("$i" != "") $i = "_$i";

    $txtstr = '';
    $end = 0;
    $start = strpos($str,'<txt>');
    while ($start) {
        $txtstr .= substr($str,$end,$start-$end);
        $start += 5; //fin du tag marquant le début
        $end = strpos($str,'</txt>',$start);
        $name = substr($str,$start,$end-$start);
        $explode=explode('=',$name,2);
        $name = $explode[0];
        //Si $_POST['nom_champ'] est définit le récupère
        if (isset($_POST[$name.$i]))
            $value = $_POST[$name.$i];
        elseif (isset($explode[1]))
            $value = $explode[1];
        else
            $value = '';
        $txtstr .= "<input style='width:10em' type='text' value='".htmlspecialchars($value,ENT_QUOTES)."' name='$name$i' />";
        $end += 6; //fin du tag marquant la fin
        $start = strpos($str,'<txt>',$end);
    }
    $txtstr .= substr($str,$end);

    $formstr = '';
    $end = 0;
    $start = strpos($txtstr,'<champ>');
    while ($start) {
        $formstr .= substr($txtstr,$end,$start-$end);
        $start += 7; //fin du tag marquant le début
        $end = strpos($txtstr,'</champ>',$start);
        if (!$end) exit('<p style="color:red">'.__('Balise de fermeture &lt;/champ&gt; non trouvée').' ('.__('début de chaine :').' <i>'.htmlentities(substr($txtstr,$start,50)).'...</i>).</p><pre>'.htmlentities($txtstr).'</pre>');
        $name = substr($txtstr,$start,$end-$start);
        $formstr .= "<select style='width:10em' name='$name$i'>";

        if (isset($_POST[$name.$i])) {
            //Le champ a déjà été defini, recupération ce celui-ci
            $formstr .= "
                    <option value=''>-</option>";
            foreach ($optionsSelect as $champ => $value) {
                if ($champ == $_POST[$name.$i]) $formstr .= "
                    <option value='$champ' selected='selected'>$champ - ".htmlspecialchars($value,ENT_QUOTES)."</option>";
                else  $formstr .= "
                    <option value='$champ'>$champ - ".htmlspecialchars($value,ENT_QUOTES)."</option>";
            }
        } else {
            //Le champs n'est pas encore définit, l'option séléctionner est "None"
            $formstr .= "
                    <option value='' selected='selected'>-</option>";
            foreach ($optionsSelect as $champ => $value) {
                $formstr .= "
                    <option value='$champ'>$champ - ".htmlspecialchars($value,ENT_QUOTES)."</option>";
            }
        }
        $formstr .= "</select>";

        $end += 8; //fin du tag marquant la fin
        $start = strpos($txtstr,'<champ>',$end);
    }
    $formstr .= substr($txtstr,$end);

    return $formstr;
}

function optionsSelect_set($item) {
    //Liste des options pour select
    global $optionsSelect;
    $optionsSelect = array();
    foreach ($item as $champ => $value) {
        if ($champ != 'key') {
            if (is_array($value))
                $value = "liste : ".join('; ',$value);
            $value = convstr::motor2xml($value);
            if (strlen($value)>50)
                $value = substr($value,0,48).'...';
                //$value = substr($value,0,max(48,strpos(($value),"<br />"))).'...';
            $value = htmlspecialchars($value,ENT_QUOTES);
            $optionsSelect [$champ] = "$value";
        }
    }
}

function addquote($str) {
    //echape certain élement
    return str_replace(array('\\','\''),array('\\\\','\\\''),$str);
}


// Lecture du fichier du thème
$generatorfile = PATH_GCWEB.'/templates/'.$conf['template'].'/'.$modelname.'_generator.php';
if (!file_exists($generatorfile)) {
    $generatorfile = PATH_GCWEB.'/templates/default/'.$modelname.'_generator.php';
    $outfile = PATH_GCWEB.'/templates/default/'.$model;
} else {
    $outfile = PATH_GCWEB.'/templates/'.$conf['template'].'/'.$model;
}

$fp = fopen($generatorfile, "r");
$generator = fread($fp, filesize ($generatorfile));
fclose($fp);


if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc()) {
    foreach ($_POST as $key => $value)
        if (!in_array($key,array('user','pswd')))
            $_POST[$key] = stripslashes($value);
}

//Site de champs pour les inputs select
if (isset ($item))
    optionsSelect_set($item);
else
    optionsSelect_set(current($bdd));






/**********************************************************************
 * Génération du code de la page pour aperçu ou enregistrement
 **********************************************************************/
if (isset($_POST['preview']) || isset($_POST['save'])) {
    //Réduction des questions du générateur
    echo "
        <script type='text/javascript'>
            document.getElementById('generator').style.visibility='hidden';
            exit();
        </script>";

    //Partie "ocde" du fichier de thème
    $start = strpos($generator,'<generatorcode>')+15;
    $end = strpos($generator,'</generatorcode>');
    $codeSource = substr($generator, $start, $end-$start);

    //Recherche des boucles
    $endTagLoop = 0;
    $startTagLoop = strpos($codeSource,'<loop ');
    $codeWithLoop = ''; //code de la page question avec les boucles
    while ($startTagLoop) {
        $codeWithLoop .= substr($codeSource,$endTagLoop,$startTagLoop-$endTagLoop);
        $startLoop = strpos($codeSource,'>',$startTagLoop) + 1;
        $nameLoop = substr($codeSource, $startTagLoop + 6, $startLoop-$startTagLoop - 7);
        $endLoop = strpos($codeSource, '</loop>', $startLoop);
        $codeLoop = substr($codeSource, $startLoop, $endLoop-$startLoop);

        //Répète la boucle le nombre de fois indiqué par le poste caché de la question
        for ($nbloop = 0; $nbloop < $_POST["nbloop_$nameLoop"]; $nbloop++) {
            //echo "<br />$nameLoop $nbloop/".$_POST["nbloop_$nameLoop"];
            $nbchiff = count($nbloop);
            $codeLoopTmp = $codeLoop;
            foreach ($_POST as $key => $value) {
                if (substr($key,-$nbchiff) == $nbloop && $value != "") {
                    //si le champ POST termine par le même nombre que $nbloop et qu'il n'est pas vide :
                    //  → essaye d'y remplacer dans la boucle
                    $keyname = substr($key,0,-$nbchiff-1);
                    $codeLoopTmp = str_replace("%%$keyname%%",addquote($value),$codeLoopTmp);
                }
            }
            if ($codeLoopTmp!=$codeLoop)
                $codeWithLoop .= $codeLoopTmp;
        }



        //Recherche le début de la prochaine boucle
        $endTagLoop = $endLoop + 7;
        $startTagLoop = strpos($codeSource,'<loop ', $endTagLoop);
    }
    $codeWithLoop .= substr($codeSource,$endTagLoop); //ajout du code après la dernière boucle
    unset($codeSource,$startTagLoop,$endTagLoop);



    //Recherche des parties a mettre uniquement si il une des variable à été compléter dans la formulaire
    $endTagIfexists = 0;
    $startTagIfexists = strpos($codeWithLoop,'<ifexists ');
    $codeWithIfexists = ''; //code de la page avec ou sans le ifexists
    while ($startTagIfexists) {
        $codeWithIfexists .= substr($codeWithLoop,$endTagIfexists,$startTagIfexists-$endTagIfexists);
        $startIfexists = strpos($codeWithLoop,'>',$startTagIfexists) + 1;
        $nameIfexists = substr($codeWithLoop, $startTagIfexists + 10, $startIfexists-$startTagIfexists - 11);
        $endIfexists = strpos($codeWithLoop, '</ifexists>', $startIfexists);
        $codeIfexists = substr($codeWithLoop, $startIfexists, $endIfexists-$startIfexists);
        if (isset($_POST[$nameIfexists])) {
            if ($_POST[$nameIfexists] != "") {
                foreach ($_POST as $key => $value)
                    $codeIfexists = str_replace("%%$key%%",addquote($value),$codeIfexists);
                 $codeWithIfexists .= $codeIfexists;
            }
        }

        //Recherche le début de la prochaine boucle
        $endTagIfexists = $endIfexists + 11;
        $startTagIfexists = strpos($codeWithLoop,'<ifexists ', $endTagIfexists);
    }
    $codeWithIfexists .= substr($codeWithLoop,$endTagIfexists); //ajout du code après la dernière boucle



    //Recherche des champs a mettre uniquement si une des variable n'a pas été complétée dans la formulaire
    $endTagIfNOTexists = 0;
    $startTagIfNOTexists = strpos($codeWithIfexists,'<ifnotexists ');
    $codeWithIfNOTexists = ''; //code de la page avec ou sans le ifexists
    while ($startTagIfNOTexists) {
        $codeWithIfNOTexists .= substr($codeWithIfexists,$endTagIfNOTexists,$startTagIfNOTexists-$endTagIfNOTexists);
        $startIfNOTexists = strpos($codeWithIfexists,'>',$startTagIfNOTexists) + 1;
        $nameIfNOTexists = substr($codeWithIfexists, $startTagIfNOTexists + 13, $startIfNOTexists-$startTagIfNOTexists - 14);
        $endIfNOTexists = strpos($codeWithIfexists, '</ifnotexists>', $startIfNOTexists);
        $codeIfNOTexists = substr($codeWithIfexists, $startIfNOTexists, $endIfNOTexists-$startIfNOTexists);
        if (!isset($_POST[$nameIfNOTexists]))   $NOTexists = True;
        elseif ($_POST[$nameIfNOTexists] == '') $NOTexists = True;
        else                                    $NOTexists = False;

        if ($NOTexists) {
            foreach ($_POST as $key => $value)
                $codeIfNOTexists = str_replace("%%$key%%",addquote($value),$codeIfNOTexists);
            $codeWithIfNOTexists .= $codeIfNOTexists;
        }

        //Recherche le début de la prochaine boucle
        $endTagIfNOTexists = $endIfNOTexists + 14;
        $startTagIfNOTexists = strpos($codeWithIfexists,'<ifnotexists ', $endTagIfNOTexists);
    }
    $codeWithIfNOTexists .= substr($codeWithIfexists,$endTagIfNOTexists); //ajout du code après la dernière boucle



    //Remplacement des champs se trouvant ni dans une boucle ni dans un ifexist
    foreach ($_POST as $key => $value)
        $codeWithIfNOTexists = str_replace("%%$key%%",addquote($value),$codeWithIfNOTexists);


    $codeFinal = trim($codeWithIfNOTexists);

    ini_set('track_errors',True); //Enregistre les erreur
    error_reporting(15);    //Affiche les notice
    ob_start();
        eval('?>'.$codeFinal.'<?php ');
    $html = ob_get_clean();

    if (isset($php_errormsg)) {
        //Si des erreur php on été trouvé, affiche le code de la page

        /*** Affichage du code html ***/
        //no de ligne
        $ligneshtml = '';
        for ($lignehtml = 1; $lignehtml <= substr_count($html, "\n"); $lignehtml++)
        {
            $ligneshtml.= "$lignehtml\n";
        }

        $msgGenerator .= '
            <div class="ko">
                <p>'.__('Attention, la page contient des erreurs, celle-ci ne sont
                peut-être sans importance mais vérifier que la page s\'affiche
                correctement en masquant le générateur.').'</p>
            </div>';

        echo '
            <div class="ko">
                '.__('<p><strong>Attention</strong> : une ou plusieurs erreurs ont été
                détectée lors de l\'execution de la page créer par le générateur.</p>

                <p>Si une ou plusieurs lignes du type &ldquo;<code>...
                <strong>eval()\'d code</strong> on line <strong>&lt;no&gt;</no></strong>
                </code>&rdquo; s\'affichent repporté le numéro &lt;no&gt; dans le code
                ci-dessous pour trouvé l\'erreur.</p>

                <p>Si vous n\'arrivez pas à résoudre votre problème posez votre question sur le
                forum de <a href="http://jonas.tuxfamily.org">http://jonas.tuxfamily.org</a>.</p>').'

                <p><strong>'.__('Dernière erreur interceptée :').'</strong><br /><code>'.$php_errormsg.'</code></p>
            </div>';


        /*** Affichage du code php ***/
        //no de ligne, compte les \n afin de svoir combien il y a de ligne
        $lignesphp = '';
        for ($lignephp = 1; $lignephp < substr_count($codeFinal, "\n"); $lignephp++)
        {
            $lignesphp.= "$lignephp\n";
        }
        $arraypost = array();
        foreach ($_POST as $label => $value) {
            if (!in_array($label,array('user','pswd','preview')))
                $arraypost[] = "[$label] => $value";
        }

        echo '
            <div style="clear:both"> </div>

            <fieldset><legend>'.__('Code php évalué').'</legend>
            <div style="margin: auto; width: 99%; overflow:auto; max-height: 60em; padding: 0; border: 1px gray solid;background: #F5F5F5;">
                <pre style="float:left; margin: 0; text-align: right; padding: 0 10px; background: #AAA;">'.$lignesphp.'</pre>
                <pre style="margin: 0; overflow: visible">'.htmlspecialchars($codeFinal).'</pre>
            </div>
            </fieldset>

            <fieldset><legend>'.__('Tableau des valeurs postées').'</legend>
            <div style="margin: auto; width: 99%; overflow:auto; max-height: 60em; padding: 0; border: 1px gray solid;background: #F5F5F5;">
            <pre>'.join("\n",$arraypost).'</pre>
            </div>
            </fieldset>';
    } /*else {*/
        if (isset($_POST['save'])) {
            /*** Sauvergarde de la page ***/
            include dirname(__FILE__).'/auth.php';
            if (auth::check_user($_POST['user'],$conf['user']) && auth::check_password($_POST['pswd'], $conf['password'])) {
                $fout = fopen($outfile, "w");
                fwrite($fout,$codeFinal);
                fclose($fout);
                $msgGenerator .= '
                    <div class="ok">
                    <p>'.__('La page à été enregistrée avec succès.').'</p>
                    </div>';
            } else {
                $msgGenerator .= '
                    <div class="ko">
                    <p>'.__('Mot de passe ou utilisateur non valide.').'</p>
                    </div>';
            }
        //}
    }
}
unset($_GET['user']); unset($_GET['pswd']);



/**********************************************************************
 * Affichage des questions
 **********************************************************************/

/*if (!isset($_POST['save'])) {*/
    //Affichage question
    ?>

    <div id="showgenerator">
        <p>[<a href='javascript:document.getElementById("generator").style.visibility="visible";exit()'><?php echo __('Afficher le générateur') ?></a>]</p>
    </div>
    <div id="generator">
    <form method='post' action='<?php echo htmlentities($_SERVER['REQUEST_URI']); if(!isset($_GET['generator'])) echo '&amp;generator=true'; ?>'>
        <div id="ask">
        <h1><?php echo __('Générateur de modèles de pages') ?></h1>

        <p><?php echo __('Le générateur à été lancé car ce thème n\'est pas adapté à ce type
        de collection. Pour l\'adapter créer toute les fichiers ci-dessous en
        complétant de simples formulaires.') ?></p>

        <?php echo $msgGenerator ?>


        <ul>
            <li><?php echo __('Information sur les différent modèle d\'affichage du thème (cliquez dessus pour les (re)créer).') ?>
                <ul>
                    <?php
                    foreach ($listModelName as $modelNameTmp) {
                        if (file_exists(PATH_GCWEB.'/templates/'.$conf['template'].'/'.$modelNameTmp.'_generator.php')) {
                            if (file_exists(PATH_GCWEB.'/templates/'.$conf['template'].'/'.$modelNameTmp.'_'.$collec['type'].'.php'))
                                echo '<li class="listfile"><a href="'.hrefModel($modelNameTmp).'&amp;generator=true">'.$modelNameTmp.' <span style="color:black; position:absolute; left:15em">'.__('recréer ce modèle').'</span></a></li>';
                            else
                                echo '<li class="listfile"><a href="'.hrefModel($modelNameTmp).'&amp;generator=true">'.$modelNameTmp.' <span style="color:red; position:absolute; left:15em">'.__('créer ce modèle').'</span></a></li>';
                        } elseif (file_exists(PATH_GCWEB.'/templates/default/'.$modelNameTmp.'_generator.php')) {
                            if (file_exists(PATH_GCWEB.'/templates/default/'.$modelNameTmp.'_'.$collec['type'].'.php'))
                                echo '<li class="listfile"><a href="'.hrefModel($modelNameTmp).'&amp;generator=true">'.$modelNameTmp.' <span style="color:black; position:absolute; left:15em">'.__('recréer ce modèle').'</span></a></li>';
                            else
                                echo '<li class="listfile"><a href="'.hrefModel($modelNameTmp).'&amp;generator=true">'.$modelNameTmp.' <span style="color:red; position:absolute; left:15em">'.__('créer ce modèle').'</span></a></li>';
                        } else {
                            echo '<li class="listfile">'.$modelNameTmp.' <span style="color:red; position:absolute; left:15em">'.__('pas de fichier model').'</span></a></li>';
                        }
                    } ?>
                </ul>
            </li>
        </ul>


        <ul>
            <li><?php echo __('Informations techniques sur le modèle d\'affichage en cours de création :') ?>
                <ul>
                    <li class='listfile'><?php echo __('Modèle d\'affichage') ?> :  <span><?php echo $model ?></span></li>
                    <li class='listfile'><?php echo __('Collection de type') ?> :   <span><?php echo $collec['type'] ?></span></li>
                    <li class='listfile'><?php echo __('Basé sur le modèle') ?> :   <span><?php echo $generatorfile  ?></span></li>
                    <li class='listfile'><?php echo __('À enregistrer dans') ?> :   <span><?php echo dirname(__FILE__).'/../templates/'.$conf['template'].'/'.$model;  ?></span></li>
                </ul>
            </li>
        </ul>

        <hr />

        <?php

        //Partie question du fichier de thème
        $start = strpos($generator,'<generatorask>')+14;
        $end = strpos($generator,'</generatorask>');
        $askSource = substr($generator, $start, $end-$start);


    echo '<h3>'.sprintf(__('Création du model "%s"'),$modelname).'</h3>';

    if (isset($_POST['preview']))
        echo '<p style="color:red">'.__('Attention ! prévisualisation de la page. Elle n\'est pas encore enregistrée.').'</p>';


    //Construction du code de la page

    //Recherche des boucles
    $endTagLoop = 0;
    $startTagLoop = strpos($askSource,'<loop ');
    $askWithLoop = ''; //code de la page question avec les boucles
    $codeLoop = array();
    while ($startTagLoop) {
        $askWithLoop .= substr($askSource,$endTagLoop,$startTagLoop-$endTagLoop);
        $startLoop = strpos($askSource,'>',$startTagLoop) + 1;
        $nameLoop = substr($askSource, $startTagLoop + 6, $startLoop-$startTagLoop - 7);
        $endLoop = strpos($askSource, '</loop>', $startLoop);
        $codeLoop[$nameLoop] = substr($askSource, $startLoop, $endLoop-$startLoop);
        if (isset($_POST["addloop_$nameLoop"])) {
            $nbtotloop = $_POST["nbloop_$nameLoop"] + 5; //si le input hidden du nombre n'existe pas affiche 5 champs
        } else {
            if (isset($_POST["nbloop_$nameLoop"]))
                $nbtotloop = $_POST["nbloop_$nameLoop"];
            else
                $nbtotloop = 5;
        }
        for ($nbloop = 0; $nbloop < $nbtotloop; $nbloop++) {
            $askWithLoop .= generatorTag2inputHtml($codeLoop[$nameLoop],$nbloop);
        }
        $nblooparray[$nameLoop] = $nbloop;

        $endTagLoop = $endLoop + 7;
        $startTagLoop = strpos($askSource,'<loop ', $endTagLoop);
    }
    $askWithLoop .= substr($askSource,$endTagLoop);

    //Recherche des boutons d'ajout de boucle
    $endTagAddLoop = 0;
    $startTagAddLoop = strpos($askWithLoop,"<addloop ");
    $askWithAddLoop = ''; //code de la div question avec les boutons d'ajout de boucle
    while ($startTagAddLoop) {
        $askWithAddLoop .= substr($askWithLoop,$endTagAddLoop,$startTagAddLoop-$endTagAddLoop);
        $endTagAddLoop = strpos($askWithLoop,'>',$startTagAddLoop) + 1;
        $nameLoop = substr($askWithLoop,$startTagAddLoop+9, $endTagAddLoop-$startTagAddLoop-10);
        $askWithAddLoop .= "
            <input type='hidden' id='nbloop_$nameLoop' name='nbloop_$nameLoop' value='".$nblooparray[$nameLoop]."' />
            <input type='submit' value='Ajouter 5 champs' name='addloop_$nameLoop' title='".__('Les champs vide seront ignoré lors de la génération de la page')."' />";
        $startTagAddLoop = strpos($askWithLoop,"<addloop ", $endTagAddLoop);
    }

    $askWithAddLoop .= substr($askWithLoop,$endTagAddLoop);


    $askWithAddLoop = generatorTag2inputHtml($askWithAddLoop);
    //Recharche des boutons d'ajout de boucle

    echo eval('?>'.$askWithAddLoop.'<?php ;');
    //fermeture div*/
    ?>
        </div>
        <div id="savepreview">
            <p><input style="width:7em" type="submit" name="preview" value="Prévisualiser" />
            ou <input style="width:7em" type="submit" name="save" value="Enregistrer" />
            (Utilisateur <input style="width:5em" type="text" name="user" />
            Mot de passe <input style="width:5em" type="password" name="pswd" />)
    <?php if (isset($php_errormsg)) { ?>
            <span style="color:red"><?php echo __('Attention la page contient des erreurs') ?></span>
    <?php } ?>
            </p>
        </div>
        <div id="hide">
            <p>[<a href='javascript:document.getElementById("generator").style.visibility="hidden";exit()'><?php echo __('masquer le générateur') ?></a>]</p>
        </div>
    </form>
    </div>
    <?php
}
?>
