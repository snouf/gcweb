<?php
/*
 *      This file is a part of GCstarWeb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Création de image
 *   $_GET['collec'] :      Id de la collection
 *   $_GET['img'] :         Nom de l'image de l'élément (si incorrect renvera l'image de couverture absente)
 *   $_GET['x'] :           Largeur max  de l'image (si non précisé, taille original)
 *   $_GET['y'] :           Hauteur max de l'image (si non précisé, taille original)
 *   $_GET['cache'] :       Mettre en cache (True pas défaut)
 *   $_GET['purge'] :       Si True purge l'ancienne image
 */


include (dirname(__FILE__).'/../conf/config.php');


//Liste de extentions possible
$jpg = array('jpg','JPG','jpeg','JPEG');
$gif = array('gif','GIF');
$png = array('png','PNG');

#recupération id collec
if (!isset($_GET['collec']))    $collecid = 0;
if ($_GET['collec'] == '')      $collecid = 0;
else                            $collecid = intval($_GET['collec']);
#recup config collection
$collec = $conf['collections'][$collecid];

#récupération nom de l'image
if (!isset($_GET['img']))       $img = '';
else                            $img = htmlspecialchars_decode(urldecode($_GET['img']));
if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
                                $img = stripslashes($img);

#récupération de la largeur max
if (!isset($_GET['x']))         $Xtb = 0;
elseif ($_GET['x'] == '')       $Xtb = 0;
else                            $Xtb = intval($_GET['x']);

#récupération de la hauteur max
if (!isset($_GET['y']))         $Ytb = 0;
elseif ($_GET['y'] == '')       $Ytb = 0;
else                            $Ytb = intval($_GET['y']);

#récupération du chemain de l'image
$pathImg = PATH_GCWEB.'/collections/'.$collec['picturesdir'].'/'.$img;

if ($img == '' || !file_exists($pathImg)) {
    #si n'existe pas image de remplacement
    $img='nocover';
    $dirtemplate = PATH_GCWEB.'/templates/'.$conf['template'].'/';
    if      (file_exists($dirtemplate.'img/'.$collec['type'].'_'.'nocover.png'))   $pathImg = $dirtemplate.'img/'.$collec['type'].'_'.'nocover.png';
    elseif  (file_exists($dirtemplate.'img/'.$collec['type'].'_'.'nocover.jpg'))   $pathImg = $dirtemplate.'img/'.$collec['type'].'_'.'nocover.jpg';
    elseif  (file_exists($dirtemplate.'img/'.$collec['type'].'_'.'nocover.gif'))   $pathImg = $dirtemplate.'img/'.$collec['type'].'_'.'nocover.gif';
    elseif  (file_exists($dirtemplate.'img/nocover.png'))   $pathImg = $dirtemplate.'img/nocover.png';
    elseif  (file_exists($dirtemplate.'img/nocover.jpg'))   $pathImg = $dirtemplate.'img/nocover.jpg';
    elseif  (file_exists($dirtemplate.'img/nocover.gif'))   $pathImg = $dirtemplate.'img/nocover.gif';
    elseif  (file_exists(PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.png'))   $pathImg = PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.png';
    elseif  (file_exists(PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.jpg'))   $pathImg = PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.jpg';
    elseif  (file_exists(PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.gif'))   $pathImg = PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.gif';
    elseif  (file_exists(PATH_GCWEB.'/templates/default/img/nocover.png'))   $pathImg = PATH_GCWEB.'/templates/default/img/nocover.png';
    elseif  (file_exists(PATH_GCWEB.'/templates/default/img/nocover.jpg'))   $pathImg = PATH_GCWEB.'/templates/default/img/nocover.jpg';
    elseif  (file_exists(PATH_GCWEB.'/templates/default/img/nocover.gif'))   $pathImg = PATH_GCWEB.'/templates/default/img/nocover.gif';
    else    exit('Error : image for remplacement non found');
}

//Dimention et type de l'image
$reso = getimagesize($pathImg);
$Ximg = $reso[0];
$Yimg = $reso[1];
$mine = $reso['mime'];
$minePng = image_type_to_mime_type(IMAGETYPE_PNG);
$mineJpg = image_type_to_mime_type(IMAGETYPE_JPEG);
$mineGif = image_type_to_mime_type(IMAGETYPE_GIF);

if ($mine == $mineJpg)      $ext='.jpg';
elseif ($mine == $minePng)  $ext='.png';
else                        $ext='.gif';

if (!$Xtb && !$Ytb) {
    #si aucun redimmentionnement préciser, retrourne l'original
    header("Content-type: $mine");
    fpassthru(fopen($pathImg, "rb"));
    return;
}

#Dimension pour pas de deformation de la miniatures
if (!$Xtb)
    $Xtb = intval($Ytb*$Ximg/$Yimg);
elseif (!$Ytb)
    $Ytb = intval($Xtb*$Yimg/$Ximg);
elseif ($Xtb*$Yimg < $Ytb*$Ximg)
    $Ytb = intval($Xtb*$Yimg/$Ximg);
else
    $Xtb = intval($Ytb*$Ximg/$Yimg);

#Regarder si l'image se trouve en cache
$nameCache = $collecid.'_'.$img.'_'.$Xtb.'x'.$Ytb.$ext;
$pathCache = dirname(__FILE__).'/../cache/images/'.$nameCache;

if (isset($_GET['purge']))   @unlink($pathCache);

if (file_exists($pathCache)) {
    //L'image se trouve en cache, l'affiche
    //si aucun redimmentionnement préciser, retrourne l'original
    header("Content-type: $mine");
    fpassthru(fopen($pathCache, "rb"));
    return;
} else {
    //L'image ne se trouve pas en cache, charge l'original, créer la minature et l'ecrit
    if     ($mine == $mineJpg)     $GDimg = ImageCreateFromJpeg($pathImg);
    elseif ($mine == $minePng)      $GDimg = ImageCreateFromPng($pathImg);
    elseif ($mine == $mineGif )     $GDimg = ImageCreateFromGif($pathImg);
    else    return "Error_:_image_'$pathImg',_'$type'_not_supported";

    $GDtb = ImageCreateTrueColor($Xtb, $Ytb);

    //Redimm de l'image
    if ($conf['GDresampled'])
        ImageCopyResampled($GDtb, $GDimg, 0, 0, 0, 0, $Xtb, $Ytb, $Ximg, $Yimg);
    else
        ImageCopyResized($GDtb, $GDimg, 0, 0, 0, 0, $Xtb, $Ytb, $Ximg, $Yimg);

    header("Content-type: $mine");

    if ($conf['noCacheImg']) {
        //Pas de mise en cache de image, le retoune au navigateur
        if ($mine == $mineJpg)
            ImageJpeg($GDtb, NULL, $conf['jpgQuality']);
        elseif ($mine == $minePng)
            ImagePng($GDtb);
        else
            ImageGif($GDtb);
        return;
    } else {
        //ecriture de l'image
        if ($mine == $mineJpg)
            ImageJpeg($GDtb, $pathCache, $conf['jpgQuality']);
        elseif ($mine == $minePng)
            ImagePng($GDtb, $pathCache);
        else
            ImageGif($GDtb, $pathCache);

        fpassthru(fopen($pathCache, "rb"));
        return;
    }
}
