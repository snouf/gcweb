<?php
/*
 *      xml.php is a part of GCweb (unofficial web interface for gcstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


class bdd
{
    public static function xml2array($xmlfile)
    {
        /*
         * Charge le fichier xml dans un tableau
         */

        global $collec, $conf, $fieldstypes;

        $cacheFullBDD = PATH_GCWEB.'/cache/bdd/_'.$collec['id'].'_fullBDD.static.gz';
        if (!file_exists($cacheFullBDD))
            $cacheFullBDD_mtime = 0;
        else
            $cacheFullBDD_mtime = filemtime($cacheFullBDD);

        if (  $conf['noCacheBDD']
            || isset($_GET['purge'])
            || $cacheFullBDD_mtime < filemtime($xmlfile)
        ) {
            // On charge le fichier xml pour créer le cache
            if (!$xml = simplexml_load_file($xmlfile))
                return sprintf(__('erreur : impossible de charger le xml %s.'),$xmlfile);


            // Parcourir les items
            $id = 0;
            $offset4duplicateID = 0;
            $bdd = array();
            foreach($xml->item as $itemxml) {
                $id ++;
                $item = array();

                //Dans le fork de Kerenoc le champs inutilisé sont absent du xml
                foreach($fieldstypes[$collec['type']] as $type => $champs) {
                    foreach ($champs as $champ) {
                        if ($type == 'list')
                            $item[$champ] = array();
                        elseif (($type == 'numeric') && ($champ == 'id'))
                            // use case: gcs created by manual cvs import => id field is not set
                            // but its usage is mandatory in gcsweb.
                            $item[$champ] = $id;
                        else
                            $item[$champ] = $conf['champVide'];
                    }

                }

                //A partir de GCweb 1.3 certaines données sont enregistrées dans les attributs de item
                foreach ($itemxml->attributes() as $champ => $value) {
                    if ($value != "")
                        $item[$champ] = convstr::xml2motor(nl2br(htmlspecialchars(trim($value))));
                }

                //Parcourir les champs de l'item
                foreach ($itemxml->children() as $childxml) {

                    $champ = $childxml->getName();
                    //echo " || $champ = $childxml";

                    if ($childxml->line->col) {
                        //echo "<br /> $champ : ";
                        //Si le champ est un liste (genre, auteurs ...) parcours cette liste
                        foreach ($childxml->children() as $linexml) {
                            //Stocker le champ de type liste
                            if (count($linexml->col) == 1) {
                                if ($linexml->col != "")
                                    $item[$champ][] = convstr::xml2motor(nl2br(htmlspecialchars(trim($linexml->col))));
                                else
                                    $item[$champ][] = $conf['champVide'];
                            } else {
                                $line = array();
                                foreach ($linexml->children() as $colxml) {
                                    if ($colxml != "")
                                        $line[] = convstr::xml2motor(nl2br(htmlspecialchars(trim($colxml))));
                                    else
                                        $line[] = $conf['champVide'];
                                }
                                $item[$champ][] = $line;
                            }
                        }
                    } else {
                        //Sinon stocker le champ de type str, int ...
                        if ($childxml != "")
                            $item[$champ] = convstr::xml2motor(nl2br(htmlspecialchars(trim($childxml))));
                    }
                }

                //Stocker l'item "tableau php" dans un monstre tableau contenant tout le items
                if (!isset($item['id'])) {
                    if (isset($item['gcsautoid']))
                        $item['id'] = $item['gcsautoid'];
                    else
                        $item['id'] = $id;
                }

                //Si un décalage d'id existe à cause du dupliqué, l'applique
                if ($offset4duplicateID != 0)
                    $item['id'] = strval($item['id'] + $offset4duplicateID);

                while (array_key_exists($item['id'],$bdd)) {
                    //Si il y a une duplication d'un id, le majorer jusqu'à trouver une place libre
                    $offset4duplicateID ++;
                    $item['id'] = strval($item['id'] + $offset4duplicateID);
                }

                $item['array_add_to_all_pages'] = array();
                $item['array_add_to_page_detail'] = array();

                include PATH_GCWEB.'/conf/plugins4item.php';

                $bdd[$item['id']] = $item;

                unset($item);
            }

            include PATH_GCWEB.'/conf/plugins4bdd.php';


            if (!$conf['noCacheBDD']) {
                $fp = gzopen($cacheFullBDD,'w');
                gzwrite($fp,(serialize($bdd)));
                gzclose($fp);
            }
        }  else {
            //lecture cache de la BDD complète non triée
            $bdd = unserialize(implode("", gzfile($cacheFullBDD)));
        }

        return $bdd;
    }


    public static function tri($array, $sortStr)
    {
        /*
         * Trier en tableau selon 1 à 6 clefs.
         * $array : tableau à trier
         * $sortStr : Chaine de tri
         *       ex : "serieDSC,titleASC" = tri décroissant selon les séries
         *                                  puis croissant selon les titres
         * Retourne le tableau trié
         */

        global $msg, $conf;

        //Transformation de la chaine de tri en tableau de tri
        $tmpSorts = explode(',',$sortStr);
        if (count($tmpSorts) > 6) {
            $msg .= '<p class="warning">'.__('Attention ! Il est impossible de trier un tableau
                selon plus de 6 clefs. Le tableau a été trié selon les 6 premières clefs').'</p>';
            array_splice ($tmpSorts, 6);
        }


        foreach ($tmpSorts as $sort) {
            if (substr($sort,-3) == 'DSC')
                $sorts[] = array(substr($sort,0,-3),SORT_DESC);
            elseif (substr($sort,-3) == 'ASC')
                $sorts[] = array(substr($sort,0,-3),SORT_ASC);
            else
                $sorts[] = array($sort,SORT_ASC);
        }


        //Pour l'utilisation de la fonction array_multisort création des tableaux
        //de tri.
        //Les valeurs (qui servent au tri) sont débarassées des majuscules et des
        //accents pour un tri insensible à ceux-ci.
        foreach ($array as $key => $row) {
            foreach ($sorts as $sort) {
                if (count($sort)) {
                    $value = $row[$sort[0]];
                    if (is_array($value) && count($value) ) #Si le champ est de type tableau, tri selon sa 1ère clef
                        $value = $value[0];

                    $value = bdd::evalChar($value,$sort[0]);

                    $tmparray[$sort[0]][$key] = $value;
                }
            }
        }

        //Tri selon 1 à 6 clefs (la case correspond au nombre de clefs)
        switch (count($sorts))
        {
            case 1:
                array_multisort(
                    $tmparray[$sorts[0][0]],$sorts[0][1],SORT_NATURAL|SORT_FLAG_CASE,$array
                );
                break;
            case 2:
                array_multisort(
                    $tmparray[$sorts[0][0]],$sorts[0][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[1][0]],$sorts[1][1],SORT_NATURAL|SORT_FLAG_CASE,$array
                );
                break;
            case 3:
                array_multisort(
                    $tmparray[$sorts[0][0]],$sorts[0][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[1][0]],$sorts[1][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[2][0]],$sorts[2][1],SORT_NATURAL|SORT_FLAG_CASE,$array
                );
                break;
            case 4:
                array_multisort(
                    $tmparray[$sorts[0][0]],$sorts[0][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[1][0]],$sorts[1][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[2][0]],$sorts[2][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[3][0]],$sorts[3][1],SORT_NATURAL|SORT_FLAG_CASE,$array
                );
                break;
            case 5:
                array_multisort(
                    $tmparray[$sorts[0][0]],$sorts[0][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[1][0]],$sorts[1][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[2][0]],$sorts[2][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[3][0]],$sorts[3][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[4][0]],$sorts[4][1],SORT_NATURAL|SORT_FLAG_CASE,$array
                );
                break;
            default:
                array_multisort(
                    $tmparray[$sorts[0][0]],$sorts[0][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[1][0]],$sorts[1][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[2][0]],$sorts[2][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[3][0]],$sorts[3][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[4][0]],$sorts[4][1],SORT_NATURAL|SORT_FLAG_CASE,
                    $tmparray[$sorts[5][0]],$sorts[5][1],SORT_NATURAL|SORT_FLAG_CASE,$array
                );
                break;
        }
        return $array;
    }


    public static function filtre($array, $filtreStr)
    {
        /*
         * Flitre un tableau selon des conditions "contient", "plus petit"
         * "égal" ... qui peuvent se combiner avec de "et" ou des "ou"
         * $array : tableau à filtrer
         * $filtreStr : chaîne de filtres
         *     ex : title=Sillage,authors=Buchet
         *
         * Retourne le tableau filtré
         */

        global $msg, $conf;

        //Transforme les contitions sous forme de chaine en tableau de conditions
        $conditions = array();
        $start = 0;
        $end = strpos($filtreStr,',');
        while ($end !== false) {
            $tmpfiltreStr = substr($filtreStr,$start,($end-$start));
            if (substr(str_replace('\\\\','',$tmpfiltreStr),-1) != '\\') {
                $conditions[] = substr($filtreStr,$start,($end-$start));
                $start = $end + 1;
            }
            $end = strpos($filtreStr,',',$end+1);
        }
        $conditions[] = substr($filtreStr,$start);

        $newarray = array();

        // Parcours les conditions
        $isFirstCondition = True;
        foreach ($conditions as $condition) {
            $tmparray = array();

            if (strpos($condition,'!=='))
                $cond = '!=='; //Ne contient pas la chaine exact
            elseif (strpos($condition,'!='))
                $cond = '!='; //Ne contient pas
            elseif (strpos($condition,'=='))
                $cond = '=='; //Strictement =
            elseif (strpos($condition,'<='))
                $cond = '<='; //Plus petit ou =
            elseif (strpos($condition,'>='))
                $cond = '>='; //Plus grand ou =
            elseif (strpos($condition,'='))
                $cond = '='; //contient
            elseif (strpos($condition,'<'))
                $cond = '<'; //plus petit
            elseif (strpos($condition,'>'))
                $cond = '>'; //plus grand
            else {
                $msg .= '<p class="error">'.sprintf(__('Erreur de syntaxe sur la condition "%s"'),$condition).'</p>';
                $conf['noCacheBDD'] = True; //Y a une erreur; ne pas mettre en cache
                break;
            }

            list($and_or_key,$value) = explode ($cond,$condition,2);
            $and_or_key = trim($and_or_key);
            if ($and_or_key[0] == '|' || $and_or_key[0] == '&') //Si un signe de condition se trouve
                $key =substr($and_or_key,1); //devant la clef, le supprime.
            else
                $key = $and_or_key;

            $value = trim(bdd::evalChar($value,$key));
            if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
                $value = stripslashes($value);

            if ($and_or_key[0] == '&' && !$isFirstCondition)
                $searchinarray = $newarray;
            else
                $searchinarray = $array;
            $isFirstCondition = False;

            foreach ($searchinarray as $item) {
                // Parcourt le tableau principal et stock dans un tableau
                // temporaire les items aux conditions correctes
                $id = $item['id'];

                if (!isset($item[$key])) {
                    $msg .= '<p>'.sprintf(__('Le champs "%s" n\'existe pas, filtrage avec celui-ci impossible'),$key).'</p>';
                    $conf['noCacheBDD'] = True; //Y a une erreur; ne pas mettre en cache
                    break;
                }

                //itemKews doit être un tableau 1D
                if (is_array($item[$key])) {
                    if (count($item[$key]) && is_array($item[$key][0])) {
                        //Pour le support des éléments 2D (plusieurs lignes et plusieurs colonnes)
                        //transformation en tableau 1D contenant toutes les données
                        $itemKeys = array();
                        foreach ($item[$key] as $keys)
                            $itemKeys = array_merge($itemKeys,$keys);
                    } else {
                        //On balance la clef sans la transformer
                        $itemKeys = $item[$key];
                    }
                } else {
                    //La clef n'est pas un tableau, on met sa valeur dans la 1ère case
                    $itemKeys[] = $item[$key];
                }


                $ok = False;
                $ko = False;
                foreach ($itemKeys as $itemKey) {
                    $itemKey = trim(bdd::evalChar($itemKey,$key));
                    //~ echo "<pre>DEBUG : \"$value\" $cond \"$itemKey\" ???</pre>";
                    switch ($cond)
                    {
                        case '!==':
                            if ($itemKey != $value)
                                $ok = True;
                            else
                                $ko = True;
                            break;
                        case '!=':
                            if ((strnatcasecmp($itemKey, $value)) != 0)
                                $ok = True;
                            else
                                $ko = True;
                            break;
                        case '==':
                            if ((strnatcasecmp($itemKey, $value)) == 0)
                                $ok = True;
                            break;
                        case '<=':
                            if ((strnatcasecmp($itemKey, $value)) <= 0)
                                $ok = True;
                            break;
                        case '>=':
                            if ((strnatcasecmp($itemKey, $value)) >= 0)
                                $ok = True;
                            break;
                        case '=':
                            if ((stripos($itemKey, $value)) !== False)
                                $ok = True;
                            break;
                        case '<':
                            if ((strnatcasecmp($itemKey, $value)) < 0)
                                $ok = True;
                            break;
                        case '>':
                            if ((strnatcasecmp($itemKey, $value)) > 0)
                                $ok = True;
                            break;
                    }
                }
                if ($ok && !$ko) {
                    //~ echo "<pre>oui</pre>";
                    $tmparray["$id"] = $item;
                }
                unset($itemKeys);
            }

            switch ($and_or_key[0])
            {
                case '&':
                    $newarray = $tmparray;
                    break;
                case '|':
                default:
                    $newarray += $tmparray;
                    break;
            }
        }
        return $newarray;
    }

    public static function filterSortSliceAndCache($filter='none',$sort='none',$start=False,$end=False,$noPurgeCache=False)
    {
        /*
         * Tri, filtre, extrait un portion et met en cache le résultat et le retourne
         */

        global $conf,$collec,$fieldstypes, $msg;

        include PATH_GCWEB.'/conf/fieldstypes.php';

        $cacheFileName = md5($collec['id'].$filter.$sort);

        if ($start) $cacheFileName .= '_'.$start;
        else        $cacheFileName .= '_0';
        if ($end)   $cacheFileName .= '-'.$end;
        else        $cacheFileName .= '-end';

        $cacheFileStatic = PATH_GCWEB.'/cache/bdd/'.$cacheFileName.'.static.gz';
        $cacheFile = PATH_GCWEB.'/cache/bdd/'.$cacheFileName.'.gz';

        if ( $noPurgeCache || in_array($cacheFileName, $conf['nopurgeCacheBDD']) || file_exists($cacheFileStatic) )
            //Fichier cache "static"
            $cacheFile = $cacheFileStatic;
        //else Fichier cache purgeable

        $xmlfile = dirname(__FILE__).'/../collections/'.$collec['dir'].'/'.$collec['xml'];

        if ($conf['noCacheBDD'] || isset($_GET['purge']) || !file_exists($cacheFile)) {
            //Pas de cache spécifié en config ou purge demandée via url
            $createCache = True;
        } else {
            //date des différents fichier
            $dateCache  = filemtime($cacheFile);
            $dateXml    = filemtime($xmlfile);

            if ($dateCache < $dateXml) {
                //Si la date du fichier cache est plus vieille que celle du xml
                $createCache = True;
            } else {
                $createCache = False;
            }
        }

        if ($createCache) {
            $bdd = bdd::xml2array($xmlfile);

            $collec['nbItemsBDD'] = count($bdd);

            //Application des filtres
            if ($filter != 'none')
                $bdd = bdd::filtre($bdd,$filter);
            //Tri
            if ($sort != 'none' && count($bdd) != 0)
                $bdd = bdd::tri($bdd,$sort);

            if ($start || $end) {
                if (!$start)    $start = 0;
                if ($end)       $bdd = array_slice($bdd,$start,$end);
                else            $bdd = array_slice($bdd,$start);
            }

            // écriture du cache
            if (!$conf['noCacheBDD']) {
                $fp = gzopen($cacheFile,'w');
                gzwrite($fp,(serialize(array($collec['nbItemsBDD'],$bdd))));
                gzclose($fp);
            }
        } else {
            // lecture du cache
            $cache = unserialize(implode('', gzfile($cacheFile)));
            $collec['nbItemsBDD'] = $cache[0];
            $bdd = $cache[1];
        }

        if (empty($bdd))
            $msg .= '<p class="error">'.__('Aucun élément n\'a été trouvé.').'</p>';

        include PATH_GCWEB.'/conf/plugins4aftercache.php';

        return $bdd;
    }

    public static function evalChar ($value,$fieldname)
    {
        global $conf, $fieldstypes, $collec;

        if (in_array($fieldname, $fieldstypes[$collec['type']]['numeric'])) {
            return $value;
        }

        if (is_array($value)) {
            // Not sure of purpose of this leg except that added to avoid
            // TypeError on html_entity_decode() for php 8.0
            $value = trim(html_entity_decode(join(', ',
                                             convstr::motor2xml($value))));
        } else {
            $value = trim(html_entity_decode(convstr::motor2xml($value)));
        }

        if (in_array($fieldname, $fieldstypes[$collec['type']]['date'])) {
            $exploders = array('/','-',' ','.');
            $monthtranslater = array(
                    'Jan' => array('01','1',__('jan'),__('janvier')),
                    'Feb' => array('02','2',__('fev'),__('fevrier')),
                    'Mar' => array('03','3',__('mar'),__('mars')),
                    'Apr' => array('04','4',__('avr'),__('avril')),
                    'May' => array('05','5',__('mai'),__('mai')),
                    'Jun' => array('06','6',__('juin'),__('juin')),
                    'Jul' => array('07','7',__('juil'),__('juillet')),
                    'Aug' => array('08','8',__('aout'),__('aout')),
                    'Sep' => array('09','9',__('sept'),__('septembre')),
                    'Oct' => array('10',__('oct'),__('octobre')),
                    'Nov' => array('11',__('nov'),__('novembre')),
                    'Dec' => array('12',__('dec'),__('decembre'))
                );

            if (!isset($dateformatorder)) {
                $dateformatexplode = explode('/',$conf['fomatDate']);
                static $dateformatorder = array();
                foreach ($dateformatexplode as $pos => $YorMorD)
                    $dateformatorder[$YorMorD] = $pos;
            }


            foreach ($exploders as $exploder) {
                $dateexplode = explode($exploder,$value);
                if (count($dateexplode) >= 2)
                    break;
            }

            switch (count($dateexplode))
            {
                case 3 :
                    //La date précise J,M,A
                    $year = $dateexplode[$dateformatorder['Y']];
                    $month = $dateexplode[$dateformatorder['M']];
                    $day = $dateexplode[$dateformatorder['D']];
                    $sec = 0;
                    break;
                case 2 :
                    //Seuls M et A sont précisés
                    if ($dateformatorder['D'] == 0) {
                        $dateformatorder['Y'] = $dateformatorder['Y']-1;
                        $dateformatorder['M'] = $dateformatorder['M']-1;
                    }
                    $year = $dateexplode[$dateformatorder['Y']];
                    $month = $dateexplode[$dateformatorder['M']];
                    $day = 1;
                    $sec = -10; //Pour qu'il soit avant J+M+A
                    break;
                case 1 :
                    //Seul A est précisé
                    $year = $dateexplode[0];
                    $month = 1;
                    $day = 1;
                    $sec = -100; //Pour qu'il soit avant M+A
                    break;
                default :
                    //Ce n'est en tous cas pas une date
                    return $value;
                    break;
            }

            //Verification des types
            if (!is_numeric($year))
                return $value;

            //Transalte month
            foreach ($monthtranslater as $name_english => $names) {
                if (substr($month,-1,1) == '.')
                    $month = substr($month,0,-1);//suppresion du . s'il y'en a un
                if (in_array($month,$names))
                    $month = $name_english;
            }

            $timestamp = strtotime($day.' '.$month.' '.$year) + $sec;

            if ($timestamp)
                return $timestamp;
            else
                return $value;
        } else {
            $value = bdd::unaccent($value);
            foreach ($conf['ignoreString4sort'] as $ignoreStr) {
                if (stripos($value, $ignoreStr) === 0) //StartWith
                    $value = substr($value, strlen($ignoreStr));
            }
            return $value;
        }
    }


    public static function unaccent($value)
    {
        /*
         * Supprimer les accents d'une chaîne
         */

        return iconv("UTF-8", "ASCII//TRANSLIT", $value);
    }
}

?>
