<?php
/*
 *      This file is a part of GCweb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#l10n
load_l10n('func4tpl');

/*** NAVIGATION ******************************************************/


/* Pages */

function chooseCollec($nameButton='ok')
{
    /*
     * Retourne une liste pour selectionner la collection. $nameButton est la valeur
     * du bouton de confirmation (ok si non spécifié).
     */

    global $conf, $collec;
    $return = '';

    $return.='
        <form method="get" action="'.$_SERVER['PHP_SELF'].'">
        <div>
            <select name="collec" onchange="window.location.href=\''.$_SERVER['PHP_SELF'].'?model=list&amp;collec=\'+this.value">';
    foreach ($conf['collections'] as $key => $value) {
        if (!$value['private']) {
            $return.='
                <option value="'.$key.'"';
            if ($key == $collec['id'])
                $return.=' selected="selected"';
            $return.='>'.$value['title'].'</option>';
        }
    }
    $return.='
            </select>
            <noscript>
                <div class="noscript">
                    <input type="hidden" name="model" value="list" />
                    <input type="submit" value="'.$nameButton.'" />
                </div>
            </noscript>
        </div>
        </form>';

    return $return;
}

function substr_parent($parentmodel)
{
    global $collec;
    if (!isset($collec['model']))
        $collec['model'] = 'list';

    if ($parentmodel == true) {
        if (($collec['model'] == 'item')  ||
            ($collec['model'] == 'cloud') ||
            ($collec['model'] == 'search'))
            $parentmodel = ($collec['parentmodel'] ? $collec['parentmodel'] : false);
        else
            $parentmodel = ($collec['model'] ? $collec['model'] : false);
    } elseif (!$parentmodel || $parentmodel == '' || $parentmodel == 'list' ) {
        $parentmodel = false;
    } //else $parentmodel = $parentmodel
    if ($parentmodel)
        return '&amp;parentmodel='.$parentmodel;
    else
        return '';
}

function substr_model($parentmodel)
{
    global $collec;

    if ($parentmodel == true) {
        $model = ($collec['parentmodel'] ? $collec['parentmodel'] : $collec['model']);
    } elseif (!$parentmodel || $parentmodel == '' || $parentmodel == 'list' ) {
        $model = $collec['model'];
    } else $model = $parentmodel;

    return '&amp;model='.$model;
}

function hrefPage($page = 1, $parentmodel=true)
{
    /*
     * Retoure un lien vers une page.
     *    * $page   : [integer] no de la page (si non précisé, 1ère)
     *    * $parentmodel : True transmet le model parent; false ne transmet pas le model parent; [str] force le model parent
     */

    global $collec;
    $substrmodel = substr_model($parentmodel);

    if (!isset($collec['filter']))
        $collec['filter'] = 'none';
    if (!isset($collec['sort']))
        $collec['sort'] = 'none';

    $page = round($page);
    return '?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p='.$page;
}

function prevPage($nb=1,$sep=' ', $parentmodel=true)
{
    /*
     * Retoure le/les lien(s) vers la/les page(s) précédente(s).
     *    * $nb     : [integer] nombre de page à afficher (une par défaut)
     *    * $sep    : [string]  séparateur de page (espace par défaut)
     *    * $parentmodel : True transmet le model parent; false ne transmet pas le model parent; [str] force le model parent
     */

    global $collec;
    $return = '';
    $substrmodel = substr_model($parentmodel);

    if (!$collec['pages'] == 0) {
        if ($collec['page'] != 1) {
            $return.='<a class="page_prev" title="'.__('Page précédente').'" href="?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p='.($collec['page']-1).'"> ← </a>'."\n";
            $return.='<a class="page_first" href="?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p=1">1</a>'.$sep."\n";
        }
        if ($collec['page'] > $nb+2)
            $return.='...'.$sep;
        for($i=-$nb;$i<0;$i++) {
            $page = $collec['page'] + $i;
            if ($page > 1)
                $return.='<a class="pages_prev" href="?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p='.$page.'">'.$page.'</a>'.$sep."\n";
        }
    }

    return $return;
}


function currentPage()
{
    /*
     * Retourne la page courante
     */
    global $collec;

    return $collec['page'];
}


/* Items */

function nextPage($nb=1,$sep=' ', $parentmodel=true)
{
    /*
     * Retourne la/les page(s) suivante(s).
     *    * $nb     : [integer] nombre de page à afficher (une par défaut)
     *    * $sep    : [string]  séparateur de page (espace par défaut)
     *    * $parentmodel : True transmet le model parent; false ne transmet pas le model parent; [str] force le model parent
     */

    global $collec;
    $return = '';
    $substrmodel = substr_model($parentmodel);

    if (!$collec['pages'] == 0) {
        for($i=1;$i<$nb+1;$i++) {
            $page = $collec['page'] + $i;
            if ($page < $collec['pages'])
                $return.= $sep.'<a class="pages_next" href="?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p='.$page.'">'.$page."</a>\n";
        }
        if ($page < $collec['pages']-1)
            $return.="$sep...";
        if ($collec['page'] != $collec['pages']) {
            $return.= $sep.'<a class="page_last" href="?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p='.$collec['pages'].'">'.$collec['pages']."</a>\n";
            $return.= ' <a class="page_next" title="'.__('Page suivante').'" href="?collec='.$collec['id'].$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;p='.($collec['page']+1).'"> → </a>'."\n";
        }
    }
    return $return;
}

function prevItem($nb=1,$sep=' ', $parentmodel=true)
{
    /*
     * Afficher le/les lien(s) des/du item(s) précédent(s).
     *    * $nb     : [integer] nombre de page à afficher (une par défaut)
     *    * $sep    : [string]  séparateur de page (espace par défaut)
     *    * $parentmodel : True transmet le model parent; false ne transmet pas le model parent; [str] force le model parent
     */

    global $collec, $item, $bdd;
    $return = '';

    $keymax = count($bdd);
    $nb = min($nb,$item['key']-1);
    $firstItem = reset($bdd);
    $substrparent = substr_parent($parentmodel);

    switch ($item['key']) {
        case 0:
            $return.='';
            break;
        case 1:
            $return.= '<a class="item_prev" title="'.__('Précédent').'" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$firstItem['id'].'">←</a>'."\n";
            $return.= '<a class="item_first" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$firstItem['id'].'">1</a>'.$sep."\n";
            break;
        default:
            $partbdd = array_slice($bdd, max($item['key']-5,1),$nb);
            $prevItem = end($partbdd);
            $return.= '<a class="item_prev" title="'.__('Précédent').'" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$partbdd[0]['id'].'">←</a>'."\n";
            $return.= '<a class="item_first" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$firstItem['id'].'">1</a>'.$sep."\n";
            if ($item['key']-$nb > 1)
                $return.= '...'.$sep;
            $i = $item['key']-$nb;
            foreach($partbdd as $tmpitem) {
                $i ++;
                $return.= '<a class="items_prev" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$tmpitem['id'].'">'.$i.'</a>'.$sep."\n";
            }
    }
    return $return;
}


function currentItem()
{
    /*
     * retourne l'item courant
     */
    global $item;
    if (isset($item['key']))
        return $item['key']+1;
    else
        return False;
}


function nextItem($nb=1,$sep=' ', $parentmodel=true)
{
    /*
     * Afficher le/les lien(s) des/du item(s) suivant(s).
     *    * $nb     : [integer] nombre de page à afficher (une par défaut)
     *    * $sep    : [string]  séparateur de page (espace par défaut)
     *    * $parentmodel : True transmet le model parent; false ne transmet pas le model parent; [str] force le model parent
     */

    global $collec, $item, $bdd;
    $return = '';
    $substrparent = substr_parent($parentmodel);

    $keymax = count($bdd)- 1;
    if ($item['key'] == $keymax - 1) {
        $lastitem = end($bdd);
        $return.= $sep.'<a class="item_last" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$lastitem['id'].'">'.($keymax + 1)."</a>\n";
        $return.= ' <a class="item_next" title="'.__('Suivant').'" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$lastitem['id'].'">→</a>'."\n";
    } else {
        $partbdd = array_slice($bdd, $item['key']+1, min($nb,$keymax-$item['key']-1));
        $i = $item['key']+1;
        foreach($partbdd as $tmpitem) {
            $i ++;
            $return.= $sep.'<a class="items_next" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$tmpitem['id'].'">'.$i."</a>\n";
        }
        if ($item['key']+$nb < $keymax)
            $return.=$sep.'...';
        if ($item['key'] != $keymax) {
            $lastitem = end($bdd);
            $return.= $sep.'<a class="item_last" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$lastitem['id'].'">'.($keymax + 1)."</a>\n";
            $return.= ' <a class="item_next" title="'.__('Suivant').'" href="?collec='.$collec['id'].$substrparent.'&amp;model=item&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].'&amp;item='.$partbdd[0]['id'].'">→</a>'."\n";
        }
    }
    return $return;
}

function hrefItem($id, $parentmodel=true)
{
    /*
     * Retourne le lien vers l'item. Si l'item n'existe pas retourne False
     *   - $id : tableau item ou id de l'item
     *   - $parentmodel : True transmet le model parent; false ne transmet pas le model parent; [str] force le model parent
     */

    global $collec;

    if (is_array($id))
        $id = $id['id'];

    if (isset($collec['id'])) {
        $substrparent = substr_parent($parentmodel);
        return '?collec='.$collec['id'].'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$collec['sort'].$substrparent.'&amp;model=item&amp;item='.$id;
    } else
        return False;
}


/*** CHAMPS **********************************************************/

function aff($var,$separatorCol=', ', $separatorLine=', ')
{
    /*
     * Affiche un champ unique ou un tableau de champs
     *   - $vat : variable à afficher
     *   - $separatorCol : séparateur entre les "colonnes" de l'élément de type tableau
     *   - $separatorLine : séparateur entre les "lignes" de l'élément de type tableau
     */

    if (isset($var)) {
        if (is_array($var)) {
            if (is_array($var[0])) {
                //le tableau à plusieur ligne
                $varlines = array();
                foreach ($var as $varline)
                    $varlines[] = convstr::motor2xml(join($separatorCol, $varline));
                echo convstr::motor2xml(join($separatorLine, $varlines));
            } else {
                echo convstr::motor2xml(join($separatorLine, $var));
            }
        } else {
            echo convstr::motor2xml($var);
        }
        return True;
    } else {
        echo convstr::motor2xml(sprintf(__('Erreur : champ "%s" introuvable'),$var));
        return False;
    }
}

function convert($var,$separatorCol=', ', $separatorLine=', ')
{
    /*
     * convert to xml input string|array
     * and return a string
     */

    if (isset($var)) {
        if (is_array($var)) {
            if (isset($var[0]) && is_array($var[0])) {
                //le tableau à plusieur ligne
                $varlines = array();
                foreach ($var as $varline)
                    $varlines[] = convstr::motor2xml(join($separatorCol, $varline));
                return convstr::motor2xml(join($separatorLine, $varlines));
            } else {
                return convstr::motor2xml(join($separatorLine, $var));
            }
        } else {
            return convstr::motor2xml($var);
        }
    } else {
        return convstr::motor2xml(sprintf(__('Erreur : champ "%s" introuvable'),$var));
    }
}



function hrefModel ($model,$sortANDfilter=False,$parentmodel=True)
{
    /*
     * Retourne le lien vers un type de page (lister dans model_add + les
     * standard)
     *    - $model : nom de la page
     *    - $sortANDfilter : est ce qu'on transmet les filtres et l'ordre
     *      du tri
     *    - keep the parentmodel information in generated link.
     */
    global $collec;
    $substrmodel = '&amp;model='.$model;
    $substrparent = substr_parent($parentmodel);

    if ($sortANDfilter) {
        if (isset($_GET['item']))
            return '?collec='.$collec['id'].$substrparent.$substrmodel.'&amp;sort='.$collec['sort'].'&amp;filter='.rawurlencode($collec['filter']).'&amp;item='.addslashes($_GET['item']);
        else
            return '?collec='.$collec['id'].$substrparent.$substrmodel.'&amp;sort='.$collec['sort'].'&amp;filter='.rawurlencode($collec['filter']);
    } else {
        return '?collec='.$collec['id'].$substrparent.$substrmodel;
    }
}

function filter($prefix,$var,$text=False,$separator=', ',$optionBalise='',$model=False,$parentmodel=True)
{
    /*
     * Retourne un lien d'un champ unique ou d'un tableau de champs avec
     * un lien vers une la page listant les item contenant ce champs
     * keep the parentmodel information in generated link.
     */

    global $collec;

    $return = '';

    if (!$text || $text == '')
        //Si $text n'a pas été défini alors prend la valeur de $var
        $text=$var;

    if (!$model || $model == '') {
       if (isset($collec['parentmodel']) && ($collec['parentmodel']))
           $model = $collec['parentmodel'];
       else
           $model = 'list';
    }
    $substrmodel = '&amp;model='.$model;
    $substrparent = substr_parent($parentmodel);

    if (isset($var)) {
        if (is_array($var)) {
            if (count($var) && is_array($var[0])) {
                if (count($var) == 1) {
                    $var = $var[0];
                    $text = $text[0];
                } else {
                    return __('Erreur : ce champs comporte plusieurs lignes ET plusieurs colonnes. Le filtrage sur ce type éléments est impossible');
/*
                    $tmpvars = array();
                    foreach ($var as $tmpvar)
                        //$tmpvars[] = join($separator,$tmpvar);
                        $tmpvars[] = $tmpvar[0];
                    $var = $tmpvars;

                    $tmptexts = array();
                    foreach ($text as $tmptext)
                        //$tmptexts[] = join($separator,$tmptext);
                        $tmptexts[] = $tmptext[0];
                    $text = $tmptexts;
*/
                }
            }
            //c'est un tabeau retourne un liste de lien séparée par $separator
            if (count($var) != count($text)) {
                return sprintf(__('Erreur : le tableau des liens et le tableau des textes à afficher ne comporte pas le même nombre d\'élément  (%1$d contre %2$d)'),count($var),count($text));
            } else {
                $i = 0;
                $partlink = array();
                foreach ($var as $line) {
                    $partlink[] = '<a href="?collec='.$collec['id'].$substrparent.$substrmodel.'&amp;filter='.$prefix.$line.'" '.$optionBalise.'>'.convstr::motor2xml($text[$i]).'</a>';
                    $i++;
                }
                $link = join($separator, $partlink);
            }
        } else {
            //ce n'est pas un tableau
            $link = '<a href="?collec='.$collec['id'].$substrparent.$substrmodel.'&amp;filter='.$prefix.$var.'" '.$optionBalise.'>'.convstr::motor2xml($text).'</a>';
        }
        return $link;
    } else {
        return __('Erreur : champ introuvable');
    }
}

function isSortKey ($sort)
{
    /*
     * Retroune true si c'est la clef de tri
     *    - $sort : [string] idem  hrefSortBy()
     */
    global $collec;

    if ($sort == $collec['sort'])
        return True;
    else
        return False;
}

function hrefSortBy($sort,$parentmodel=True)
{
    /*
     * Retourne le lien qui permettra de classé la collection par $sort
     *    - $sort : [string] champ ou champs séparer par "," selon lequel/lesquels
     *      il faut trier la liste affiché courament
     *    - keep the parentmodel information in generated link.
     */
    global $collec;
    $substrmodel = '&amp;model='.$collec['model'];
    $substrparent = substr_parent($parentmodel);

    return '?collec='.$collec['id'].$substrparent.$substrmodel.'&amp;filter='.rawurlencode($collec['filter']).'&amp;sort='.$sort;
}

function star($note,$max=10)
{
    /*
     * Exprimer une note sous fourme de 5 images (typiquement des étoiles
     * ou coeurs).
     * Il est possible d'exprimé la note avec un nombre différent d'image
     * en changeant $max, (nb étoile = $max/2).
     * Les images ont 3 valeurs possible : pleine, demi, vide. ces images
     * doivent se situer dans le repertoire "img" du thème utilisé et se
     * nommer "star_full", "star_half" et "star_empty". Les extentions supportés
     * sont "png", "gif" et "jpg".
     */

    global $conf, $dirGCweb;

    $return = array();

    $note = intval($note);
    $nb['star_full'] = floor($note/2);
    if ($nb['star_full'] - $note/2 == 0)
        $nb['star_half'] = 0;
    else
        $nb['star_half'] = 1;
    $nb['star_empty'] = round($max/2) - $nb['star_full'] - $nb['star_half'];

    for ($i = 0; $i < $nb['star_full'] ; $i++)
        $return[] = '<span class="star_full"></span>';
    for ($i = 0; $i < $nb['star_half'] ; $i++)
        $return[] = '<span class="star_half"></span>';
    for ($i = 0; $i < $nb['star_empty'] ; $i++)
        $return[] = '<span class="star_empty"></span>';

    return join("",$return);
}


function image($img='',$X='',$Y='')
{
    /*
     * Redimentionnement des images des couvertures et affiche leur lien
     *  - $img :    chemin vers l'image
     *  - $X, $Y :  resolution de l'image de sortie (si non précisé retoune $big)
     */
    global $collec, $dirGCweb;

    $img = urlencode(substr(strrchr(convstr::motor2xml($img), '/'), 1));

    if (!$X || $X == 'auto')    $X = '';
    if (!$Y || $Y == 'auto')    $Y = '';

    return $dirGCweb.'?image=1&amp;collec='.$collec['id'].'&amp;img='.$img.'&amp;x='.$X.'&amp;y='.$Y;
}

function attrsize_image($img, $Xtb=False,$Ytb=False)
{
    /*
     * Retourne les atributs height="..." width="..." rapport concerné de l'image "img"
     */
    global $collec,$conf;

    if ($Xtb == '' || $Xtb == 'auto')    $Xtb = False;
    if ($Ytb == '' || $Ytb == 'auto')    $Ytb = False;

    $img = substr(strrchr(convstr::motor2xml($img), '/'), 1);

    $pathImg = PATH_GCWEB.'/collections/'.$collec['picturesdir'].'/'.$img;
    if ($img == '' || !file_exists($pathImg)) {
        #si n'existe pas image de remplacement
        $img='nocover';
        $dirtemplate = PATH_GCWEB.'/templates/'.$conf['template'].'/';
        if      (file_exists($dirtemplate.'img/'.$collec['type'].'_'.'nocover.png'))   $pathImg = $dirtemplate.'img/'.$collec['type'].'_'.'nocover.png';
        elseif  (file_exists($dirtemplate.'img/'.$collec['type'].'_'.'nocover.jpg'))   $pathImg = $dirtemplate.'img/'.$collec['type'].'_'.'nocover.jpg';
        elseif  (file_exists($dirtemplate.'img/'.$collec['type'].'_'.'nocover.gif'))   $pathImg = $dirtemplate.'img/'.$collec['type'].'_'.'nocover.gif';
        elseif  (file_exists($dirtemplate.'img/nocover.png'))   $pathImg = $dirtemplate.'img/nocover.png';
        elseif  (file_exists($dirtemplate.'img/nocover.jpg'))   $pathImg = $dirtemplate.'img/nocover.jpg';
        elseif  (file_exists($dirtemplate.'img/nocover.gif'))   $pathImg = $dirtemplate.'img/nocover.gif';
        elseif  (file_exists(PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.png'))   $pathImg = PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.png';
        elseif  (file_exists(PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.jpg'))   $pathImg = PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.jpg';
        elseif  (file_exists(PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.gif'))   $pathImg = PATH_GCWEB.'/templates/default/img/'.$collec['type'].'_'.'nocover.gif';
        elseif  (file_exists(PATH_GCWEB.'/templates/default/img/nocover.png'))   $pathImg = PATH_GCWEB.'/templates/default/img/nocover.png';
        elseif  (file_exists(PATH_GCWEB.'/templates/default/img/nocover.jpg'))   $pathImg = PATH_GCWEB.'/templates/default/img/nocover.jpg';
        elseif  (file_exists(PATH_GCWEB.'/templates/default/img/nocover.gif'))   $pathImg = PATH_GCWEB.'/templates/default/img/nocover.gif';
        else    exit (__('Erreur : image de remplacement non trouvée').'.');
    }

    $reso = getimagesize($pathImg);
    $Ximg = $reso[0];
    $Yimg = $reso[1];

    if (!$Xtb && !$Ytb) {
        $Xtb = $Ximg;
        $Ytb = $Yimg;
    } elseif (!$Xtb) {
        $Xtb = intval($Ytb*$Ximg/$Yimg);
    } elseif (!$Ytb) {
        $Ytb = intval($Xtb*$Yimg/$Ximg);
    } elseif ($Xtb*$Yimg < $Ytb*$Ximg) {
        $Ytb = intval($Xtb*$Yimg/$Ximg);
    } else
        $Xtb = intval($Ytb*$Ximg/$Yimg);

    return 'width="'.$Xtb.'" height="'.$Ytb.'"';

}




/*** FORMULAIRE DE TRI ***/

/*function order($champs,$labels=False,$optionBalise='')
{
    /* formulaires permettant de trier les éléments de la collection
     *    - $champs : tableau de champ de recherche ou 'start', 'order',
     *      'submit', 'end' (voire exemple)
     *    - $labels : tableau des labels si non préciser ou False prendra
     *      le nom des champs.
     *    - $optionBalise : permet d'ajouter des options dans les balises
     *
     * Note: il est important que le nombre d'élement soit identique dans
     *      les 2 tableaux ou que $label ne sois pas précisé (= False)
     *
     * Exemple:
     * <?php
     * order('start');  //obligatoire
     * order(array('title', 'publisher'), array('Titre', 'Éditeur'); //Champs selon leque tier
     * order('order');  //optionnel affiche un champ selon lequel trié
     * order('submit'); //obligatoire, valide les donnée
     * order('end');    //obligatoire
     */

//}



/*** FORMULAIRE POUR LA RECHECHE *************************************/



function search($champ, $type='', $optionBalise='', $default='', $parentmodel=True)
{
    /*
     * Aide a la conception des formulaire de recherche. Un petit peut commpliqué
     * à comprendre, le plus simple est de reprendre le code du thème par defaut
     *
     * Plusieurs façons de s'en servir, voire exemple ci-dessous.
     *    - Le 1èr argument de la fonction spécifie le type de champs ('start',
     *      'request', ..., 'submit', 'end') voir exemples.
     *    - Le 2ème sert à mettre une/des valeur(s) dans les champs de type
     *      textes et listes.
     *    - Le 3ème sert a ajouter de option à la balise html (class, id, style,
     *      width ...)
     *
     *
     * Exemple (Les champs entre [] sont optionnels)
     *
     *   1) Afficher un petit formulaire avec un champs pour mettre le mot à
     *      chercher et une liste des champs où chercher (comme dans la barre
     *      de menu du thème par défaut).
     *          search('start');    // Début du formulaire (ouverture de la balise <form>)
     *          search('keyword');  // Zone où l'utilisateur pourra entré ce
     *                  // qu'il cherche (<input type="text" ...)
     *          search('in_champs',array(
     *              array(<cond1>,<label1>),
     *              [array(<cond2>,<label2>),
     *              [array(<cond3>,<label3>)]]
     *          ));
     *                  // Affiche une liste à options (<select><option></option></select>)
     *                  // pour séléction dans quel champ il faut chercher.
     *                  //  <cond> : La condition de recherche du type <champ>
     *                  //          contient %s :
     *                  //              <champ>=%s //
     *                  //          ou encore <champ1> ou <champ2> contient %s :
     *                  //              <champ1>=%s,|<champ2>=%s
     *                  //      <champ> : est le nom du champ du fichier xml dans
     *                  //              lequel il faut chercher
     *                  //      = :     Un signe de condition¹ les autres peuvent
     *                  //              aussi être utilisé
     *                  //      %s :    Serra remplacé par la chaine à chercher
     *                  //  <label> : étiquette dans la liste à options
     *          [search('in_collec');]
     *                  // Liste les collections, si non spécifier untilise la
     *                  // collection courantes.
     *          search('submit');   // Bouton de soumission² (<input type="submit" ...)
     *          search('end');      // fin du formulaire (</form>)
     *
     *   2) Afficher un formulaire de recherche multi-critères (comme dans la
     *      page recherche avancée du thème pas défaut)
     *          search('start');    // Début du formulaire
     *          [search(<nom du champs>, <type>);]
     *                  // Affiche une ligne de recherche pour un des critères.
     *                  // Réutiliser cette fonction pour les autres critères.
     *                  // <Nom du champs>  ben ... le champs ou chercher
     *                  // <type> : Type de champs (affiche le choix des signes
     *                  //          de condition en conséquence.
     *                  //      'str':  Chaine, on peut donc faire des "contient",
     *                  //              "ne contient pas", ...
     *                  //      'num':  Valeur numérique, on peut donc faire des "<", ">", ...
     *                  //      'bool': Oui/non
     *                  //      'list': Affiche un liste des valeur que peut prendre le champ
     *                  //      '':     Signe de condition à taper
     *                  //      <other>:<other> sera le signe de condition¹
     *          search('submit');   // Bouton de soumission (il est possible de
     *                  // spécifier le label avec le 2ème arguments
     *          search('end');      // Fin du formulaire
     *
     *   3) Afficher une zone de texte pour taper une requête manuellement
     *      (comme la 2ème et dernière partie de la recharche avancée du thème
     *      par défaut)
     *          search('start');    // Début du formulaire
     *          search('request');  // Zone pour taper la requète, utilisé le
     *                  // 3ème argument de la fonction pour spécifier la taille
     *                  // de cette zonne (voir options de textarea en html)
     *          search('submit');   // Bouton de soumission il est possible de
     *                  // spécifier le label avec le 2ème arguments
     *          search('end');      // Fin du formulaire
     *
     *
     * Note
     *
     * (¹)  Les signes de conditions (insensible à la case)
     *          =   contient
     *          !=  ne contient pas
     *          ==  est exactement
     *          ==! ne contient pas l'expression exacte
     *          <   plus petit que (valeur numérique uniquement)
     *          >   plus grand que (idem)
     *          <=  plus petit ou égal que (idem)
     *          >=  plus grand ou égal que (idem)
     */



    global $collec;
    $substrmodel = substr_model($parentmodel);
    $substrparent = substr_parent($parentmodel);

    $return = '';

    switch ($champ) {
        case 'start':
            $return .= '<form method="post" action="'.$_SERVER['PHP_SELF'].'?collec='.$collec['id'].$substrmodel.$substrparent.'" '.$optionBalise.'>';
            break;

        case 'submit':
            if ($type == '')
                $type = __('chercher');
            $return .= '<input type="submit" name="search" value="'.$type.'" '.$optionBalise.' />';
            break;

        case 'end':
            $return .= '</form>';
            break;

        case 'keyword':
            if (isset($_POST['keyword'])) {
                if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
                    $value = htmlspecialchars(stripslashes($_POST['keyword']));
                else
                    $value = htmlspecialchars($_POST['keyword']);
            } else {
                $value = $default;
            }
            $return .= '<input type="text" name="keyword" value="'.$value.'" '.$optionBalise.' />';
            break;

        case 'in_champs':
            if (!is_array($type))
                $return .= __('Le 2ème argument de search doit être un array(...)');

            if (isset($_POST['in_champs'])) {
                if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
                    $value = htmlspecialchars(stripslashes($_POST['in_champs']));
                else
                    $value = htmlspecialchars($_POST['in_champs']);
            } else {
                $value = $default;
            }

            $return .= "\n".'<select name="in_champs" '.$optionBalise.">\n";
            foreach ($type as $option )
                $return .= '    <option value="'.$option[0].'" '.$optionBalise.($option[0] == $value ? ' selected="selected"' : '').'>'.$option[1]."</option>\n";

            $return .="</select>\n";
            break;

        case 'request' :
            $return .= '
                <textarea name="request" '.$optionBalise.'></textarea>';
            break;

        default:

            switch ($type) {
                case 'str':
                    $return .= '
                        <select name="'.$champ.'Cond" '.$optionBalise.'>
                            <option value="=">'.__('contient').'</option>
                            <option value="!=">'.__('ne contient pas').'</option>
                            <option value="==">'.__('est').'</option>
                            <option value="!==">'.__('n\'est pas').'</option>
                        </select>
                        <input type="text" name="'.$champ.'Word" '.($default != '' ? 'value="'.$default.'" ' : '').$optionBalise.' />';

                    break;

                case 'num':
                    $return .= '
                        <select name="'.$champ.'Cond" '.$optionBalise.'>
                            <option value="&lt;">&lt;</option>
                            <option value="&lt;=">&le;</option>
                            <option value="==">=</option>
                            <option value=">=">&ge;</option>
                            <option value=">">&gt;</option>
                        </select>
                        <input type="text" name="'.$champ.'Word" '.($default != '' ? 'value="'.$default.'" ' : '').$optionBalise.' />';
                    break;

                case 'bool':
                    $return .= '
                        <input type="hidden" name="'.$champ.'Cond" value="==" />
                        <select name="'.$champ.'Word" '.$optionBalise.'>
                            <option value="">-</option>
                            <option value="0"'.(!$default ? ' selected="selected"' : '').'>'.__('Non').'</option>
                            <option value="1"'.($default  ? ' selected="selected"' : '').'>'.__('Oui').'</option>
                        </select>';
                    break;


                case 'list':
                case 'listmultiple':
                    if (substr($champ,0,5) == '&amp;') {
                        $and_or = '&amp;';
                        $champ = substr($champ,5);
                    } elseif ($champ[0] == '&') {
                        $and_or = '&amp;';
                        $champ = substr($champ,1);
                    } elseif ($champ[0] == '|') {
                        $and_or = '|';
                        $champ = substr($champ,1);
                    } else {
                        $and_or = '';
                    }
                    global $bdd;
                    $values = array();
                    foreach ($bdd as $item) {
                        if (is_array($item[$champ])) {
                            foreach ($item[$champ] as $value) {
                                if (test($value))
                                    $values[bdd::evalChar($value,$champ)] = $value;
                            }
                            unset ($item[$champ]);
                        } else {
                            if (test($item[$champ]))
                                $values[bdd::evalChar($item[$champ],$champ)] = $item[$champ];
                        }
                    }

                    ksort($values);

                    $return .= '
                        <select name="'.$and_or.$champ.'Cond" '.$optionBalise.'>
                            <option value="==">'.__('est').'</option>
                            <option value="!==">'.__('n\'est pas').'</option>
                        </select>
                        <select '.($type == 'listmultiple' ? 'name="'.$and_or.$champ.'Word[]" multiple="multiple" class="multiple" ' : 'name="'.$champ.'Word" ').$optionBalise.'>
                            '.($type != 'listmultiple' ? '<option value=""></option>' : '');

                    $letter = '';
                    $prevletter = '';
                    foreach ($values as $value4sort => $value) {
                        $letter = strtoupper(substr($value4sort,0,1));
                        if ($letter != $prevletter) {
                            if ($prevletter != '')
                                $return .= '
                                    </optgroup>';
                            $prevletter = $letter;
                            $return .= '
                                    <optgroup label="'.$letter.'">';
                        }
                        $return .= '
                            <option value="'.$value.'"'.($default == $value  ? ' selected="selected"' : '').'>'.convstr::motor2xml($value).'</option>';
                    }
                    if ($letter != '')
                        $return .= '
                                    </optgroup>';
                    $return .= '</select>';
                    break;

                case '':
                    $return .= '
                        <input type="text" name="'.$champ.'Cond" '.$optionBalise.' />
                        <input type="text" name="'.$champ.'Word" '.($default != '' ? 'value="'.$default.'" ' : '').$optionBalise.' />';
                    break;


                default :
                    $return .= '
                        <input type="hidden" name="'.$champ.'Cond" value="'.$type.'" />';
                    if (is_array($default)) {
                        $return .= '
                            <select name="'.$champ.'Word" '.$optionBalise.'>';
                        foreach ($default as $option) {
                            if (substr($option, -1) == '*') {
                                $option = substr($option, 0, -1);
                                $return .=
                                    '<option value="'.$option.'" selected="selected">'.$option.'</option>';
                            } else {
                                $return .=
                                    '<option value="'.$option.'">'.$option.'</option>';
                            }
                        }
                        $return .= '
                            </select>';
                    } else {
                        $return .= '
                            <input type="text" name="'.$champ.'Word" value="'.$default.'" '.$optionBalise.' />';
                    }
                    break;
            }
            break;
    }
    return $return;
}


/*** Nuage ***********************************************************/
function cloud($champ, $pas=10, $parentmodel=True)
{
    /*
     * Créer un nuage avec des champs de type tableau (autheur, genre ...)
     *  - $champs : nom du champs (genre, autors ...)
     *  - $pas : Nombre de style différent (10 par defaut) il seront à definire
     *    dans la feuille de style avec les class tag1 à tag10.
     *  - keep the parentmodel information in generated link.
     */

    global $bdd,$dirGCweb,$collec;
    $substrmodel = substr_model($parentmodel);
    $substrparent = substr_parent($parentmodel);

    $return = '';
    $max = 0;
    $min = 0;

    foreach ($bdd as $item) {
        if (isset($item[$champ])) {
            if (is_array($item[$champ])) {
                //A chaque fois qu'une valeur est rencontrée, elle est incrémentée de 1
                foreach ($item["$champ"] as $value) {
                    if (is_array($value)) {
                        foreach ($value as $col) {
                            if ($col != '') {
                                $k4sort = strval(bdd::evalChar($col,$champ));
                                if (!isset($cloud[$k4sort]))
                                    $cloud[$k4sort] = array($col,1);
                                else
                                    $cloud[$k4sort][1] ++;

                                if ($cloud[$k4sort][1] < $min || $min == 0)
                                    $min = $cloud[$k4sort][1];
                                if ($cloud[$k4sort][1] > $max)
                                    $max = $cloud[$k4sort][1];
                            }
                        }
                    } elseif ($value != '') {
                        $k4sort = strval(bdd::evalChar($value,$champ));
                        if (!isset($cloud[$k4sort]))
                           $cloud[$k4sort] = array($value,1);
                        else
                            $cloud[$k4sort][1] ++;

                        if ($cloud[$k4sort][1] < $min || $min == 0)
                            $min = $cloud[$k4sort][1];
                        if ($cloud[$k4sort][1] > $max)
                            $max = $cloud[$k4sort][1];
                    }
                }
            } else {
                $value = $item[$champ];
                if ($value != '') {
                    $k4sort = strval(bdd::evalChar($value,$champ));
                    if (!isset($cloud[$k4sort]))
                       $cloud[$k4sort] = array($value,1);
                    else
                        $cloud[$k4sort][1] ++;

                    if ($cloud[$k4sort][1] < $min || $min == 0)
                        $min = $cloud[$k4sort][1];
                    if ($cloud[$k4sort][1] > $max)
                        $max = $cloud[$k4sort][1];
                }
            }
        }
    }

    $rap = ($max-$min) / ($pas-1);
    if ($rap == 0)
        $rap = 1;

    $count = count($cloud);
    if ($count>10000) {
        $grpLettres = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        $prevLettres = '';
        $MaxPosGrpLettres = 0;
    } elseif ($count > 400) {
        if ($count>1000)
            $grpLettres = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        else
            $grpLettres = array('01234','56789','ABC','DEF','GHI','JKL','MNO','PQR','STUV','WXYZ');
        $prevLettres = '';
        $PosGrpLettres = 0;
        $MaxPosGrpLettres = count($grpLettres) - 1;
    }

    $sort = ksort($cloud);

    foreach ($cloud as $key => $array) {
        list($value, $nb) = $array;
        $valueConv = convstr::motor2xml($value);

        if (($count > 400) && ($key)) {
            //Si le liste dépace le nombre indiqué on la séparre un plusieur sous liste

            $lettres = $grpLettres[$PosGrpLettres]; //Lettre a rechercher
            $prevPosGrpLettres = $PosGrpLettres;    //Stock ou on est dans le tableau pour éviter de rechercher à partir du début lorce qu'on rencontre des caractère spéciaux

            while (!stristr($lettres,$key[0]) && $lettres !== False) {
                if ($PosGrpLettres < $MaxPosGrpLettres) {
                    $PosGrpLettres ++;
                    $lettres = $grpLettres[$PosGrpLettres];
                } else {
                    $lettres = False;
                    $PosGrpLettres = $prevPosGrpLettres;
                }
            }
            if ($lettres != $prevLettres) {
                if ($lettres === False)
                    $return .= '
                        <h4 class="cloudTitle">'.__('Autres').'</h4>';
                else
                    $return .= '
                        <h4 class="cloudTitle">'.$lettres.'</h4>';
                $prevLettres = $lettres;
            }
        }

        $cloudnb = round(1 + ($nb - $min) / $rap);
        $return .= '
            <a class="cloud'.$cloudnb.'" href="?collec='.$collec['id'].$substrparent.$substrmodel.'&amp;filter='.$champ.'=='.$value.'" title="'.$nb.'">'.$valueConv.'</a>';
    }
    return $return;
}


function test($value)
{
    /*
     * Test si le champ était plein dans l'xml de gcstar. Retourne
     *   True si le champs est plein
     *   False si un champs est vide
     */

    global $conf;

    if (is_array($value))
        if (count($value) == 0 || $value[0] == $conf['champVide'])
            return False;
        else
            return True;
    if ($value == $conf['champVide'])
        return False;
    else
        return True;

}


/*** Gadjet **********************************************************/
function noIE()
{
    /*
     * Si comme moi vous en avez marre que les gens utilise le navigateur
     * qui respecte très mal les standards du web et nous oblige a bricoller
     * nos feuille de style utilsez cette fonction ;).
     *
     * Elle affiche un message qui informe les visiteur qui'il utilise un
     * navigateur respectant très mal les standards du web. L'utilisateur de
     * GCweb ayant télécharger votre thème pourra via le fichier de config :
     *   - Désactivé ce message ('noIE' => False)
     *   - Activé le message    ('noIE' => True)
     *   - Ou le personnaliser  ('noIE' => 'blabla de l'utilisateur')
     */

    global $conf;

    if (preg_match('/msie/i', GetEnv('HTTP_USER_AGENT')))
    {
        if ($conf['noIE']) {
            if ($conf['noIEtext'])
                return $conf['noIEtext'];
            else
                return '
                    <div id="noIE" class="box">
                        '.__('Vous semblez utiliser Internet Explorer. Ce navigateur respectant
                        très mal les standards du web il est possible que les pages de ce site
                        ne s’affichent pas correctement.<br />
                        Merci de prendre le temps d\'essayer
                        <a href="http://fr.wikipedia.org/wiki/Liste_de_navigateurs_web">un autre navigateur</a>
                        (par exemple <a href="http://www.mozilla-europe.org/fr/products/firefox/">firefox</a>).').'
                    </div>';
        } else {
            return False;
        }
    } else {
        return False;
    }
}



/*** Fonction d'affichage ********************************************/

/*
 * Les functions ci dessus on la même utilité que les fonctions du même nom
 * (sans le prefix aff_ ci-dessus. Mais à la place de retourné le resultat elle
 *  l'affiche (identique à echo nom_de_la_fonction();)
 */

function aff_chooseCollec($nameButton='ok')     { echo chooseCollec($nameButton); }
function aff_hrefPage($page = 1,$parentmodel=True) { echo hrefPage($page,$parentmodel); }
function aff_prevPage($nb=1,$sep=' ',$parentmodel=True) { echo prevPage($nb,$sep,$parentmodel); }
function aff_currentPage()                      { echo currentPage() ;}
function aff_nextPage($nb=1,$sep=' ',$parentmodel=True) { echo nextPage($nb,$sep,$parentmodel); }
function aff_prevItem($nb=1,$sep=' ',$parentmodel=True) { echo prevItem($nb,$sep,$parentmodel); }
function aff_currentItem()                      { echo currentItem();}
function aff_nextItem($nb=1,$sep=' ',$parentmodel=True) { echo nextItem($nb,$sep,$parentmodel); }
function aff_hrefItem($id,$parentmodel=True)   { echo hrefItem($id,$parentmodel); }
function aff_hrefModel($model,$sortANDfilter=False,$parentmodel=true) { echo hrefModel ($model,$sortANDfilter,$parentmodel); }
function aff_filter($prefix,$var,$text=False,$separator=", ",$optionBalise="",$model=False,$parentmodel=True) { echo filter($prefix,$var,$text,$separator,$optionBalise,$model,$parentmodel); }
function aff_hrefSortBy($sort)                  { echo hrefSortBy($sort); }
function aff_star($note,$max=10)                { echo star($note,$max); }
function aff_image($inputImg='',$X=False,$Y=False) { echo image($inputImg,$X,$Y);}
function aff_attrsize_image($img, $Xtb=False,$Ytb=False) {echo attrsize_image($img, $Xtb,$Ytb); }
function aff_search($champ, $type='', $optionBalise='', $default='',$parentmodel=True) { echo search($champ, $type, $optionBalise, $default, $parentmodel); }
function aff_cloud($champ, $pas=10,$parentmodel=True) { echo cloud ($champ, $pas, $parentmodel); }
function aff_test($value)                       { echo test($value); }
function aff_noIE()                             { echo noIE(); }
?>
