<?php
/*
 *      This fileis a part of GCweb (unofficial web interface for gcstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

$model_path = PATH_GCWEB.'/conf/GCModels/';
$cacheFile = PATH_GCWEB.'/conf/fieldstypes.php';

include $cacheFile;

foreach (scandir($model_path) as $xmlfile)
{
    if (substr($xmlfile,-4) == '.gcm')
    {
        $xml = simplexml_load_file($model_path.$xmlfile);
        $collec = substr($xmlfile,0,-4);
        $fieldstypes[$collec] = array(
                    'numeric'   => array(),
                    'date'      => array(),
                    'string'    => array(),
                    'bool'      => array(),
                    'list'      => array(),
                    'image'     => array(),
                    'unknow'    => array()
            );


        foreach($xml->fields->field as $xmlfield) {
            switch ($xmlfield['type'])
            {
                case 'number':
                    $fieldstypes[$collec]['numeric'][] = trim($xmlfield['value']);
                    break;
                case 'date':
                    $fieldstypes[$collec]['date'][] = trim($xmlfield['value']);
                    break;
                case 'checked text':
                case 'long text':
                case 'short text':
                case 'formatted':
                    $fieldstypes[$collec]['string'][] = trim($xmlfield['value']);
                    break;
                case 'yesno':
                    $fieldstypes[$collec]['bool'][] = trim($xmlfield['value']);
                    break;
                case 'single list' :
                case 'history text':
                case 'options':
                case 'tracks':
                    $fieldstypes[$collec]['list'][] = trim($xmlfield['value']);
                    break;
                case 'double list':
                case 'triple list':
                    $fieldstypes[$collec]['multilist'][] = trim($xmlfield['value']);
                    break;
                case 'image':
                    $fieldstypes[$collec]['image'][] = trim($xmlfield['value']);
                    break;
                case 'file':
                case 'button':
                case 'url':
                    $fieldstypes[$collec]['url'][] = trim($xmlfield['value']);
                    break;
                default :
                    $fieldstypes[$collec]['unknow'][] = trim($xmlfield['value']);
                    break;
            }
        }
    }
}

//Construction et écriture du fichier /conf/fieldstypes.php
$str_collec = array();
foreach ($fieldstypes as $collec => $typearrayfield)
{
    $str_types = array();
    foreach ($typearrayfield as $type => $fields)
        $str_types[] = "\t\t\t\t'$type' => array('".join("','",$fields)."')";
    $str_collec[] = "\t\t'$collec' => array(\n".join(",\n",$str_types)."\n\t\t\t)";
}
$str = "<?php\n//Fichier généré par convGCmodels.php\n\$fieldstypes = array (\n".join(",\n",$str_collec)."\n\t);\n?>";
$fp = fopen($cacheFile,'w');
fwrite($fp,$str);
fclose($fp);

eval("?>$str"); //Pour une prise en compe immédiat dans la page de config
?>