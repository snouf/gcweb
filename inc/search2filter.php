<?php
/*
 *      This file is a part of GCweb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

if (isset ($_POST['request'])) {
    //requet
    if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
        $_GET['filter'] = stripslashes($_POST['request']);
    else
        $_GET['filter'] = $_POST['request'];
} elseif (isset ($_POST['in_champs'])) {
    //recherche rapide
    if ($_POST['keyword'] != '')
        if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
            $_GET['filter'] = str_replace("%s",convstr::xml2motor(stripslashes($_POST['keyword'])),stripslashes($_POST['in_champs']));
        else
            $_GET['filter'] = str_replace("%s",convstr::xml2motor($_POST['keyword']),$_POST['in_champs']);
} else {
    //recherche avancé
    $listkey = array();
    $filters = array();
    foreach ($_POST as $key => $value) {
        //Detremine si c'est la condition ou le mot
        if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc()) {
            $key = stripslashes($key);
            if (is_array($value)) {
                $tmpvalue = array();
                foreach ($value as $ivalue)
                    $tmpvalue[] = stripslashes($ivalue);
                $value = $tmpvalue;
            } else {
                $value = stripslashes($value);
            }
        }

        switch (substr($key,-4))
        {
            case 'Cond':
                $listkey[substr($key,0,-4)]['cond'] = $value;
                break;
            case 'Word':
                $listkey[substr($key,0,-4)]['word'] = $value;
                break;
        }
    }

    foreach ($listkey as $key => $value) {
        if (isset($value['word'])) {
            if (is_array($value['word'])) {
                foreach ($value['word'] as $word) {
                    if ($word != '') {
                        if (substr($key,0,1) == '|' || substr($key,0,1) == '&')
                            $key = $key;
                        elseif (in_array(substr($value['cond'],0,1), array('!','<','>')))
                            $key = '&'.$key;
                        else //($key == '!==')
                            $key = '|'.$key;

                        if (is_array($value['cond'])) {
                            foreach ($value['cond'] as $icond)
                                $filters[] = $key.$icond.$word;
                        } else {
                            $filters[] = $key.$value['cond'].$word;
                        }
                    }
                }
            } else {
                if ($value['word'] != '') {
                    if (substr($key,0,1) == '|' || substr($key,0,1) == '&')
                        $key = $key;
                    elseif (in_array(substr($value['cond'],0,1), array('!','<','>')))
                        $key = '&'.$key;
                    else
                        $key = '|'.$key;

                    if (is_array($value['cond'])) {
                        foreach ($value['cond'] as $icond)
                            $filters[] = $key.$icond.$value['word'];
                    } else {
                        $filters[] = $key.$value['cond'].$value['word'];
                    }
                }
            }
        }
    }

    $_GET['filter'] = join(',',$filters);
}
?>
