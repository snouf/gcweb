<?php
/*
 *      This file is a part of GCWeb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


class convstr {
    /*
     * Converti les chaines contenue dans le xml en chaine pour le moteur.
     * et vice versa.
     *
     * Contrairement au chaine contenue de la xml les chaines pour le moteur
     * Ne sont pas :
     *   * humainement lisible
     * par contre :
     *   * elle peuvent transité par l'url d'une page sans problème !
     *
     * NB concernant le chaine pour le moteur :
     * elles sont encodé pour les urls et les "," sont doublement encodée
     * (rawurlencode(rawurlencode(',')) car se sont de caractères reservée
     * pour la séparation des conditions).
     */


    public static function xml2motor($str) {
        /*
         * Convertir des chaine de xml en chaine pour le moteur
         */
        $ret = str_replace(',','%2C',$str);
        if (is_array($ret)) {
            $array_ret = array();
            foreach ($ret as $value)
                $array_ret[] = rawurlencode($value);
            return $array_ret;
        } else {
            return  rawurlencode($ret);
        }
    }

    public static function motor2xml($str) {
        /*
         * Convertir des chaine de xml en chaine pour le moteur
         */
        if (is_array($str)) {
            $ret = array();
            foreach ($str as $value)
                $ret[] = rawurldecode($value);
        } else {
            $ret = rawurldecode($str);
        }
        return str_replace('%2C',',',$ret);
    }
}
?>
