<?php
/*
 *      xml.php is a part of GCweb (unofficial web interface for gcstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

class auth
{
    public static function check_password ($postPaswd, $confPaswd) {
        //Verif de password
        if (!(version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc()))
            $postPaswd = addslashes($postPaswd);

        if (sha1($postPaswd) == $confPaswd)
            return True;
        else
            return False;
    }

    public static function check_user ($postUser, $confUser) {
        //Verif de l'utilisateur
        if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
            $postUser = stripcslashes($postUser);

        if ($postUser == $confUser)
            return True;
        else
            return False;
    }

    public static function set_password ($postPaswd) {
        //hasher le mot de passe
        if (!(version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc()))
            return sha1(addslashes($postPaswd));
        else
            return sha1($postPaswd);
    }

    public static function set_user ($postUser) {
        //addslashes du user
        if (!(version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc()))
            return addslashes($postUser);
        else
            return $postUser;
    }
}
