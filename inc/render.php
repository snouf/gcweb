<?php
/*
 *      This file is a part of GCstarWeb (unoffical web interface for GCstar)
 *
 *      Copyright 2007 Jonas Fourquier <http://jonas.tuxfamily.org>
 *
 *      GCstarWeb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

if (isset($_GET['redirect'])) {
    //redirection cachant le referer
    if (substr($_GET['redirect'],0,4) == 'http')
        header('refresh:1;'.$_GET['redirect']);
    else
        header('refresh:1;'.URL_GCWEB.$_GET['redirect']);
    exit('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
        <head>
            <title>Redirect</title>
            <meta http-equiv="Content-Type" content="application/x-php;charset=UTF-8" />
        </head>
        <body>
            Redirect '.$_GET['redirect'].'
        </body></html>');
} elseif (!file_exists(dirname(__FILE__).'/../conf/config.php')) {
    $listoptionlang = array();
    foreach (scandir(dirname(__FILE__).'/../locales') as $file)
        if ($file[0] != '.' && $file != 'potfiles') {
            if (is_dir(dirname(__FILE__).'/../locales/'.$file))
                $listoptionlang[] = '<option value="'.$file.'">'.$file.'</option>';
        }

    exit('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
        <head><title>GCweb</title><meta http-equiv="Content-Type" content="application/x-php;charset=UTF-8" /></head>
        <body><div style="border:red 1px solid; background:#FFDBDB; padding: 1em; width:80%; margin:auto">
            <p><strong>English :</strong> GCweb is not configure. Select your country code.</p>
            <p><strong>Français :</strong> GCweb n\'est pas configuré. Selectionnez les abréviations de votre langue.</p>
            <form method="POST" action="'.URL_GCWEB.'config.php">
                <select name="lang">
                '.join("\n",$listoptionlang).'
                </select>
                <input type="submit" value="OK"/>
            </form>
        </div></body></html>');
} elseif (!file_exists(dirname(__FILE__).'/../conf/iagree.txt')) {
    exit('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
        <head><title>Erreur / Error</title><meta http-equiv="Content-Type" content="application/x-php;charset=UTF-8" /></head>
        <body><div style="border:red 1px solid; background:#FFDBDB; padding: 1em; width:80%; margin:auto">
            <h4>Error :</h4>
            <p>Read "idontagree.txt" or visit your configuration page.</p>
        </div></body></html>');
}


require PATH_GCWEB.'/conf/config.php';
require PATH_GCWEB.'/inc/l10n.php';
require PATH_GCWEB.'/inc/convstr.php';

load_l10n('default');

if (isset($_GET['image'])) {
    //image
    include (PATH_GCWEB.'inc/image.php');
    return;
} else {
    if (isset($_POST['search'])) {
        //Conversion d'une recherche en requète
        include (PATH_GCWEB.'inc/search2filter.php');
    }
    //Affichage
    render::affPage();
}


class render
{
    public static function affPage()
    {
        /*
         * Créateur de page
         * $model : modèle de la page
         *  - main : page acceuil
         *  - list : page affichant une liste d'item
         *  - item : page affichant un item
         *  - search : page de recherche
         *  - cloud : nuage (d'auteur ou de genre)
         */

        global $conf,$info,$msg,$collec,$generator, $nohead, $nofoot, $lang;

        $startTime = microtime(True);

        # id de la collection active
        if (isset($_GET['collec'])) {
            $IDcollection = addslashes($_GET['collec']);
            if (intval($IDcollection) > count($conf['collections'])) {
                $msg .= '<p class="warning">'.sprintf(__('La collection %d n\'existe pas'),$IDcollection).'</p>';
                $IDcollection = '0';
            }
        } else {
            $IDcollection = '0';
        }

        $collec = $conf['collections'][$IDcollection];
        $collec['id'] = $IDcollection;

        # Liste des types de model de page possible
        $listModelName = array('list','item'); //liste des model de collection par defaut
        $model_add_file = PATH_GCWEB.'/templates/'.$conf['template'].'/model_add';
        if (!file_exists($model_add_file))
            $model_add_file = PATH_GCWEB.'/templates/default/model_add';
        if (file_exists($model_add_file)) {
            //Si un fichier indiquant qu'il y a des fichier model supplémentaire pour le thème alors le charge
            $fp = fopen($model_add_file, "r");
            while (!feof($fp)) {
                $modelname = fgets($fp, 255);
                if (strstr($modelname,'#'))
                    $modelname = trim(substr($modelname, 0, strpos($modelname, '#')));
                else
                    $modelname = trim($modelname);

                if ($modelname != '')
                    $listModelName[] = $modelname;
            }
            fclose($fp);
        }

        # Type de page (acceuil, tag, liste, item)
        if (isset($_GET['model'])) {
            if (in_array($_GET['model'],$listModelName)) {
                $modelname = $_GET['model'];
                $model = $modelname.'_'.$collec['type'].'.php';
            } else {
                $modelname = 'error';
                $model = 'error.php';
            }
        } else {
            $modelname = 'main';
            $model = 'main.php';
        }
        $collec['model'] = $modelname ;

        # Type de page parente
        if (isset($_GET['parentmodel']))
            if (in_array($_GET['parentmodel'], $listModelName))
                $collec['parentmodel'] = $_GET['parentmodel'];
            elseif (($modelname == 'list') || ($modelname == 'listall') || ($modelname == 'onlydesc') || ($modelname == 'mosaique'))
                $collec['parentmodel'] = $modelname;
            else
                $collec['parentmodel'] = 'list' ;
        else
            $collec['parentmodel'] = $modelname;

        # Filtre (champs=value)
        if (isset($_GET['filter']))
            $filter = $_GET['filter'];
        else
            $filter = 'none';

        # Tri : trier par ? (sert aussi pour le tag à afficher)
        if ($modelname == 'rss') {
            $sort = 'idDSC';
        } elseif (isset($_GET['sort'])) {
            $sort = addslashes($_GET['sort']);
            if ($sort == 'none')    $sort = $collec['sortBy'];
        } else {
            $sort = $collec['sortBy'];
        }

        # No de page
        if (isset($_GET['p'])) {
            if (ctype_digit($_GET['p']))    $page = intval($_GET['p']);
            else                            $page = 1;
        } else {
            $page = 1;
        }


        include (dirname(__FILE__).'/func4tpl.php');
        load_template_l10n('default',$conf['lang']);

        $info = $conf;
        $info['array_add_header'] = array();
        include PATH_GCWEB.'/conf/plugins4render.php';

        //Tableau ou sont stocker les informations général
        foreach (array_keys($info['collections']) as $id)
            $info['collections'][$id]['id'] = $id;


        //Chemin vers le fichier cache et le xml de la collection active
        $cacheFile   = PATH_GCWEB.'/cache/bdd/'.md5($collec['id'].$filter.$sort);


        //Recherche head et foot du thème ou si il n'existe pas celui du thème par défaut
        $headfile = PATH_GCWEB.'/templates/'.$conf['template'].'/head.php';
        if (!file_exists($headfile))
            $headfile = PATH_GCWEB.'/templates/default/head.php';
        $footfile = PATH_GCWEB.'/templates/'.$conf['template'].'/foot.php';
        if (!file_exists($footfile))
            $footfile = PATH_GCWEB.'/templates/default/foot.php';

        //création de la page (donne aussi des info a propos de la présence ou non des header footer
        $html = render::createPage($model,$modelname,$listModelName,$collec,$filter,$sort,$page,$cacheFile); // => Création

        /*** AFFICHAGE HEADER **************************************************/
        if (!$nohead) {
            if ($generator) {
                $fp = fopen($headfile, 'r');
                $headfile = fread($fp,filesize($headfile));
                fclose($fp);
                #suppression entete
                $startbody = strpos($headfile,'<body');
                //echo '$startbody : '.$startbody;
                $startbody = strpos($headfile,'>',$startbody)+1;
                eval('?>'.substr($headfile, $startbody).'<?php ;');
            } else {
                include ($headfile);
            }
        }

        /*** AFFICHAGE BODY **************************************************/
        eval('?>'.$html.'<?php ');


        /*** AFFICHAGE FOOTER **************************************************/
        if (!$nofoot) include ($footfile);

        //purge des vieux cache
        include (dirname(__FILE__).'/purgecache.php');

        return True;
    }




    public static function createPage($model,$modelname,$listModelName,$collec,$filter='',$sort='',$page=1,$cacheName="inconnu")
    {
        /*
         * Création de la page qui sera mise en cache
         * $model : modèle pour la création de la page (voir affPage)
         */

        global $conf,$info,$collec,$bdd,$msg,$item,$nohead, $nofoot, $lang;

        $startTime = microtime(True);

        //lecture bdd
        include (dirname(__FILE__).'/bdd.php');

        $noloadxml = PATH_GCWEB.'/templates/'.$conf['template'].'/noloadxml';
        $listnoloadxml = array();
        if (!file_exists($noloadxml))
            $noloadxml = PATH_GCWEB.'/templates/default/noloadxml';
        if (file_exists($noloadxml)) {
            //Si un fichier indiquant qu'il y a des fichier model supplémentaire pour le thème alors le charge
            $fp = fopen($noloadxml, "r");
            while (!feof($fp)) {
                $modelname_noloadxml = fgets($fp, 255);
                if (strstr($modelname,'#')) #FIXME FS#8
                    $modelname_noloadxml = trim(substr($modelname_noloadxml, 0, strpos($modelname_noloadxml, '#')));
                else
                    $modelname_noloadxml = trim($modelname_noloadxml);

                if ($modelname_noloadxml != '')
                    $listnoloadxml[] = $modelname_noloadxml;
            }
            fclose($fp);
        }

        $collec['filter']       = $filter;
        $collec['sort']         = $sort;

        if (!in_array($modelname,$listnoloadxml)) {
            #charge le fichier xml
            $bdd = bdd::filterSortSliceAndCache($filter,$sort);

            if (isset($_GET['item'])) {
                //Si l'id est définit sortie de celui-ci de la base de donnée
                $i = 0;
                foreach ($bdd as $item) {
                    if ($item['id'] == $_GET['item'])
                        break;
                    $i ++;
                }
                $item['key'] = $i;
            } elseif (count($bdd) != 0) {
                //sinon on prend le 1er
                $item = $bdd[0];
                $item['key'] = 0;
            }
        }



        // Tableau des valeur des info sur la collections
        if (is_array($bdd))
            $collec['nbItems'] = count($bdd);
        else
            $collec['nbItems'] = 0;
        $collec['pages']        = round(($collec['nbItems'] - 1) / $conf['itemsPage'] + 0.5);
        if ($page > $collec['pages'])   $collec['page'] = $collec['pages'];
        else                            $collec['page'] = $page;
        $collec['firstItemPage']= ($collec['page'] - 1) * $conf['itemsPage'];
        $collec['lastItemPage'] = $collec['page'] * $conf['itemsPage'];
        if ($collec['lastItemPage'] > $collec['nbItems'])
            $collec['lastItemPage'] = $collec['nbItems'];


        //sortie de item utile (si pas page item unique)
        if (!isset($_GET['item']) && isset($bdd))
            $items = array_slice($bdd,intval($collec['firstItemPage']),intval($conf['itemsPage']));

        $bodyfile = PATH_GCWEB.'/templates/'.$conf['template'].'/'.$model;
        if (!file_exists($bodyfile)) {
            //si le fichier n'existe pas charge celui du thème par défaut
            define('TEMPLATE_MODEL_PATH_GCWEB', PATH_GCWEB.'/templates/default/');
            define('TEMPLATE_MODEL_URL_GCWEB', DIR_GCWEB.'/templates/default/');
            $bodyfile = TEMPLATE_MODEL_PATH_GCWEB.$model;
        } else {
            define('TEMPLATE_MODEL_PATH_GCWEB', PATH_GCWEB.'/templates/'.$conf['template'].'/');
            define('TEMPLATE_MODEL_URL_GCWEB', DIR_GCWEB.'/templates/'.$conf['template'].'/');
        }

        //evaluation de la page de thème
        if (file_exists($bodyfile) && !isset($_GET['generator'])) {
            if (!isset($bdd))
                $bdd = bdd::filterSortSliceAndCache($filter,$sort);
            ob_start();
            include ($bodyfile);
            $html = ob_get_clean();
        } else {
            $themegenerator = True;
            if (empty($bdd))
                $bdd = bdd::filterSortSliceAndCache($filter,$sort);
            include (dirname(__FILE__).'/themegenerator.php');
        }

        if (stristr($html, '~~ NOHEAD ~~'))     $nohead = True;
        else                                    $nohead = False;
        if (stristr($html, '~~ NOFOOT ~~'))     $nofoot = True;
        else                                    $nofoot = False;


        return str_replace(
                array('<php>', '</php>', '~~ NOCACHE ~~', '~~ PURGECACHEALLXML ~~', '~~ NOHEAD ~~', '~~ NOFOOT ~~'),
                array('<?php ', ' ?>'  , ''             , ''                      , ''            , ''            ),
                $html
            );
    }
}
?>
