<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * GCstar 1.4.3 ajoute une <col> pour le rôle des acteurs ce qui rend le champs
 * incompatible avec les fonctions de filtre.
 * Ce plugin créer donc un équivalent du champs des version < 1.4.3
 */

$comma = convstr::xml2motor(', ');

switch ($collec['type']) {
    case 'GCfilms' :
        $fieldstypes['GCfilms']['list'] += array('actors_without_roles','roles');
        if (!is_array($item['actors']))
            $item['actors'] = explode($comma,$item['actors']); //GCweb < 1.4.3 sockait l'info sous la forme d'une chaine
        $item['actors_without_roles'] = array();
        $item['roles'] = array();
        foreach ($item['actors'] as $actor) {
            if (is_array($actor)) {
                $item['actors_without_roles'][] = $actor[0];
                $item['roles'][] = $actor[1];
            } else {
                $item['actors_without_roles'][] = $actor;
                $item['roles'][] = '';
            }
        }
        //Décommentez cette ligne pour retrouver le l'apparence de la liste des acteurs de GCweb 1.0
        //$item['actors'] = $item['actors_without_roles'];
        break;

    case 'GCTVepisodes' :
        $fieldstypes['GCTVepisodes']['list'] += array('audio_without_format');
        $item['audio_without_format'] = array();
        foreach ($item['audio'] as $audio) {
            if (is_array($audio)) {
                $item['audio_without_format'][] = $audio[0];
            } else {
                $item['audio_without_format'][] = $audio;
            }
        }
        break;
}
?>