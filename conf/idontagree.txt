ENGLISH

GCstar retrieves information from many Internet sites. For a personal use, it
is not an issue. However, by displaying this data on your website, GCweb makes
it public.

The person installing GCweb must check information put online are not protected
by any intellectual property laws which would keep from this way of use.

Note: The information sources are being linked with the officials themes. However,
on alternative themes, this information may be hidden.

By renaming this file "iagree.txt", the person installing GCweb attests
to be accountable for the content retrieved.

----------

FRANCAIS

GCstar récupère des informations sur des sites internet divers ce qui pour une
utilisation pivée ne pose généralement pas de problème. Cependant, en affichant
ces informations sur votre site web, GCwebrend publique ces informations.

La personne installant GCweb doit vérifier que les informations mises en ligne
ne sont pas protégées par des règles sur la propriété intellectuelle interdisant
ce type d'utilisation.

Note : La source des informations est mise en lien avec les thèmes officiels.
Cependant, avec des thèmes alternatifs, cette information peut être masquée.

En renommant ce fichier en "iagree.txt", vous attestez être responsable du
contenu mis en ligne grâce à GCweb.

----------