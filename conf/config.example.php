<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 *******************************************
 *  - FICHIER DE CONFIGURATION DE GCWEB -  *
 *******************************************
 *
 * Ce fichier vous permet de configurer GCWeb (nom et description du site, collections,
 * apparence du site ...)
 *
 * En cas de problème :
 *   1) Vérifiez que vous n'avez pas supprimé une virgule (les éléments d'un "array"
 *      sont séparés par une virgule).
 *   2) Postez un messages sur le forum de http://jonas.tuxfamily.org/ en précisant :
 *        * Quand est-ce que l'erreur s'affiche (la page, le lien ...)
 *        * Le message d'erreur exact (entre balises [code] et [/code] svp)
 *        * Eventuellement le contenu de ce fichier (idem).
 */


$conf = array(
    /*** CONFIGURATION DU SITE ***/
    'title'         => 'GCweb - demo',  //[string]  Titre du site
    'user'          => 'user',          //[string]  Nom d'utilisateur (utile pour le créateur de thème)
    'password'      => '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684' ,
                                        //[string]  Mot de passe hasher sha1 ("pass" par defaut)
                                        //          TRES IMPORTANT CHANGEZ-LE (OU ECRIVEZ N'IMPORTE QUOI À LA PLACE)
                                        //          Pour créer votre mot de passe :
                                        //            * Sous GNU/Linux $ sha1sum [entrer] mot_de_pass [crtl+D]
                                        //            * Générateur via page web : http://jonas.tuxfamily.org/wiki/sha
    'description'   =>                  //[string]  description du site, html accepté
        'Demo du moteur de rendu web pour les collections GCweb',
    'template'      => 'default',       //[string]  Nom du dossier de thème
    'lang'          => 'eng',           //[string]  Langue de l'interface
    'itemsPage'      => 10,             //[integer] Nombre d'éléments par page
    'champVide'     => '-',             //[string,bolenan ...] Valeur de remplacement quand les champs de l'xml
                                        //          sont vides
    'noIE'          => True,            //[boolean] Afficher un message comme quoi le site risque de mal s'afficher
                                        //          avec le navigateur respectant mal les standards HTML.
    'noIEtext'      => '',              //[string]  Complétez pour personnaliser le texte à l'intention de utilisateur
                                        //          de MSIE.
    'generator'     => False,           //[bolenan] Active ou désactive le générateur de thème. A activer uniquement quand
                                        //          on en a besoin !


    /*** CONFIGURATION DES COLLECTION ***/
    'collections' => array(
        /*1ère collection*/ array(
            'title'       => 'Collection 1',    //[string]  Nom de la collection
            'dir'         => '',                //[string]  Dossier où se trouve la collection. '' si il se trouve
                                                //          dans /collections. Mettre / à la fin
            'xml'         => 'BD',              //[string]  Nom du fichier xml
            'picturesdir' => 'BD_pictures',     //[string]  Nom du dossier contenant les images de la collections
            'description' => 'blablabla',       //[string]  Description
            'type'        => 'GCbooks',         //[string]  Type de collection (les types supportés dépendent du thème utilisé).
            'sortBy'      => 'addedDSC',           //[string]  Ordre de Tri (ex idDSC → dernier au 1er id, titleASC par ordre alphabétique)
            'private'     => False              //[boolean] Cache les liens vers la galerie (il se peut qu'un thème ne prenne pas en compte cette information)
        ),
        /*2ème collection (ajouter * / coller après cette parenthèse) array(
            'title'       => '',
            'dir'         => '',
            'xml'         => '',
            'picturesdir' => '',
            'description' => '',
            'type'        => '',
            'sortBy'      => '',
            'private'     => False
        )
        /*Collections suivantes selon le même modèle*/
    ),


/*** PARAMETRES AVANCÉS ***/
                'noCacheBDD'        => False,   //[bollean] Permet de ne pas utilier le système interne de mise en cache
                'purgeCacheBDDDelay'=> 60,      //[integer] Intervale en minutes entre chaque vérification de l'âge des fichiers de cache de la basse de donnée
                'purgeCacheBDDAge'  => 60,      //[integer] Durée de vie en minutes avant la supression d'un fichier de cache de base de donnée
                'nopurgeCacheBDD'   => array(), //[strings] Liste des noms des fichiers caches de base de donnée ne devant jamais être purgés (pour trouver le nom voir /cache/bdd/)
                'jpgQuality'        => 80,      //[integer] Qualitée jpg des images générées
                'GDresampled'       => True,    //[bollean] Utiliser le rééchantillonnement des image lors du redimensionnement
                'noCacheImg'        => False,   //[bollean] Ne pas mettre en cache les images redimensionnées
                'purgeCacheImgDelay'=> 30,      //[integer] Intervale en jours entre chaque vérification de l'âge des images redimensionnées
                'purgeCacheImgAge'  => 60,      //[integer] Durée de vie en jours avant la supression d'une image redimensionnée
                'ignoreString4sort' => array('le ','la ','l\'','les ','un ','une ','des ','a ','the ','der ','die ','das ','ein ','eine ','el ','los ','una ',',','tome ','et ','- ',': '), //Liste des mot à ignorer pour les filtres et tris
                'fomatDate'        => 'D/M/Y'   //[string]  Format des dates dans vos collections
            );

@include 'config.complement.php';

setlocale (LC_ALL,'fr_FR.UTF8');   //Date en français + UTF8
date_default_timezone_set('Europe/Paris'); //Décalage horaire
?>
