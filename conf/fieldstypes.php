<?php
//Fichier généré par convGCmodels.php
$fieldstypes = array (
		'GCTVepisodes' => array(
				'numeric' => array('id','season','episode','rating'),
				'date' => array('added'),
				'string' => array('title','name','firstaired','time','director','writer','music','synopsis','comment'),
				'bool' => array('seen'),
				'list' => array('series','country','age','genre','subt','format','location'),
				'image' => array('image'),
				'unknow' => array(''),
				'multilist' => array('actors','audio'),
				'url' => array('videofile','webPage')
			),
		'GCboardgames' => array(
				'numeric' => array('id','rating','timesplayed','copies'),
				'date' => array('released','added'),
				'string' => array('name','original','players','playingtime','suggestedage','description','expansionfor','gamefamily','comments'),
				'bool' => array('completecontents'),
				'list' => array('designedby','illustratedby','publishedby','category','mechanics','expandedby','condition'),
				'image' => array('boxpic','backpic','photo1','photo2','photo3','photo4'),
				'unknow' => array(''),
				'url' => array('web')
			),
		'GCbooks' => array(
				'numeric' => array('id','rank','pages','rating'),
				'date' => array('publication','added','acquisition'),
				'string' => array('isbn','title','edition','description','comments','translator','artist'),
				'bool' => array('read'),
				'list' => array('authors','publisher','language','serie','format','genre','location'),
				'image' => array('cover','backpic'),
				'unknow' => array(''),
				'url' => array('web','digitalfile')
			),
		'GCcoins' => array(
				'numeric' => array('gcsautoid','value','year','diameter','estimate'),
				'date' => array('added'),
				'string' => array('name','comments','edge','head','tail','history','references'),
				'bool' => array(''),
				'list' => array('currency','country','condition','metal','location','type'),
				'image' => array('picture','front','back','edge1','edge2','edge3','edge4'),
				'unknow' => array(''),
				'url' => array('website')
			),
		'GCcomics' => array(
				'numeric' => array('id','volume','numberboards','cost','rating'),
				'date' => array('publishdate','printdate','added'),
				'string' => array('name','title','synopsis','isbn','comment'),
				'bool' => array('signing'),
				'list' => array('series','writer','illustrator','colourist','publisher','collection','type','category','format'),
				'image' => array('image','backpic'),
				'unknow' => array(''),
				'url' => array('webPage','file')
			),
		'GCfilms' => array(
				'numeric' => array('id','number','identifier','rating','ratingpress','rank'),
				'date' => array('date','added'),
				'string' => array('title','time','director','original','synopsis','comment'),
				'bool' => array('seen'),
				'list' => array('country','genre','region','format','place','subt','age','video','serie'),
				'image' => array('image','backpic'),
				'unknow' => array(''),
				'multilist' => array('actors','audio'),
				'url' => array('webPage','trailer')
			),
		'GCgames' => array(
				'numeric' => array('id','ean','rating','ratingpress','completion','installationsize'),
				'date' => array('released','added'),
				'string' => array('name','players','editor','developer','description','comments','secrets','serialnumber'),
				'bool' => array('case','manual','exclusive'),
				'list' => array('platform','genre','resolutions','location','installationsizeunit','region'),
				'image' => array('boxpic','backpic','screenshot1','screenshot2'),
				'unknow' => array(''),
				'url' => array('web','executable'),
				'multilist' => array('code','unlockable')
			),
		'GCmusics' => array(
				'numeric' => array('gcsautoid','rating','ratingpress'),
				'date' => array('release','added'),
				'string' => array('unique','title','artist','running','composer','producer','comments'),
				'bool' => array(''),
				'list' => array('label','genre','location','origin','format'),
				'image' => array('cover','backpic'),
				'unknow' => array(''),
				'multilist' => array('tracks'),
				'url' => array('playlist','web')
			),
		'GCwines' => array(
				'numeric' => array('id','vintage','volume','alcohol','quantity','purchaseprice','rating'),
				'date' => array('purchasedate'),
				'string' => array('name','medal','shelfindex','gift','comments','serving','tasting'),
				'bool' => array('tasted'),
				'list' => array('designation','vineyard','type','grapes','soil','producer','country','location'),
				'image' => array('bottlelabel'),
				'unknow' => array(''),
				'url' => array('website')
			),
		'GCminicars' => array(
				'numeric' => array('gcsautoid','rating1','year','rating2','rating3','buyprice','finishline','carnumber','estimate','parts','decorations','barcode','catalogprice'),
				'date' => array('added','acquisition','courseyear','catalogyear','year3'),
				'string' => array('name','version','comments1','reference','color','comments2','location','pilots','copilots','pub2','weight','personalref','interior','serial','serialnumber','lwh','containbox','framecar','bodycar','registrationnumber1','registrationnumber2','wheels','characters','filmcar','filmpart','decorationset','molding','detailsparts','detailsdecorations','colormirror','steeringwheel','catalogpage','catalogedition','comments3','referencemirror','otherscomments','othersdetails','comments4','comments5','difference'),
				'bool' => array('exchange','wanted','transformation','kit'),
				'list' => array('scale','type1','manufacturer','modele','constructor','pub','material','courselocation','box1','box2','condition','course','edition','team','madein','designed','collectiontype'),
				'image' => array('picture','logo1','logo2','picture2','catalog1','catalog2','top','back','edge1','edge2','edge3','edge4','edge5','edge6','edge7','edge8','edge9','front2','back2','edge10','edge20','edge30','edge40'),
				'unknow' => array(''),
				'url' => array('playlist','websitec','websitem','websiteo')
			),
		'GCperiodicals' => array(
				'numeric' => array('gcsautoid'),
				'date' => array('gcsfield4'),
				'string' => array('gcsfield1','gcsfield3'),
				'bool' => array(''),
				'list' => array('gcsfield2','gcsfield5'),
				'image' => array('gcsfield6'),
				'unknow' => array(''),
				'multilist' => array('gcsfield7')
			),
		'GCstamps' => array(
				'numeric' => array('gcsfield42','gcsfield34','gcsautoid'),
				'date' => array('gcsfield81','gcsfield82','gcsfield33'),
				'string' => array('gcsfield1','gcsfield5','gcsfield15','gcsfield17','gcsfield20','gcsfield21','gcsfield8','gcsfield11','gcsfield80','gcsfield41','gcsfield49','gcsfield45','gcsfield35','gcsfield36','gcsfield46','gcsfield31','gcsfield32'),
				'bool' => array('gcsfield22','gcsfield13','gcsfield44'),
				'list' => array('gcsfield2','gcsfield4','gcsfield6','gcsfield7','gcsfield10','gcsfield12','gcsfield28','gcsfield29','gcsfield43','gcsfield83','gcsfield23','gcsfield16','gcsfield27','gcsfield25','gcsfield14'),
				'image' => array('gcsfield38','gcsfield39','gcsfield47','gcsfield48'),
				'unknow' => array('')
			),
		'GCsmartcards' => array(
				'numeric' => array('gcsautoid','rating1'),
				'date' => array('year1','year2','added','acquisition'),
				'string' => array('name','title1','serial','comments1','color','catalog3','reference1','reference2','reference3','quotationnew10','quotationnew20','quotationnew30','quotationold10','quotationold20','quotationold30'),
				'bool' => array('exchange','wanted','condition','variety','charge'),
				'list' => array('chip1','type1','type2','dimension','location','pressed','edition','theme','serie','weight','country','unit'),
				'image' => array('cover','backpic','chip','boxed'),
				'unknow' => array(''),
				'url' => array('playlist')
			),
		'GCsoftware' => array(
				'numeric' => array('id','ean','numberOfCopies','price','rating','ratingpress'),
				'date' => array('released','added'),
				'string' => array('name','description','comments'),
				'bool' => array('case','manual'),
				'list' => array('platform','editor','developer','license','category','location'),
				'image' => array('boxpic','backpic','screenshot1','screenshot2'),
				'unknow' => array(''),
				'url' => array('homepage','web','executable'),
				'multilist' => array('regInfo')
			),
		'GCTVseries' => array(
				'numeric' => array('id','season','part','rating','ratingpress'),
				'date' => array('added'),
				'string' => array('name','series','title','firstaired','time','director','producer','music','synopsis','comment'),
				'bool' => array('specialep','seen'),
				'list' => array('country','age','genre','subt','format','location'),
				'image' => array('image'),
				'unknow' => array(''),
				'multilist' => array('actors','episodes','audio'),
				'url' => array('videofile','webPage')
			)
	);
?>