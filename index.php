<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_runtime())
    set_magic_quotes_runtime(0);

//$temps_debut = microtime(true);

//Chemin relatif à ce fichier vers la racine de GCWeb. (pas de "/" au début, un à la fin)
define('DIR_GCWEB', './');

define('PATH_GCWEB', dirname(__FILE__).'/'.DIR_GCWEB);

if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')
    define('URLRACINE_GCWEB','https://'.$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'],0,strrpos($_SERVER['REQUEST_URI'],'/')));
else
    define('URLRACINE_GCWEB','http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'],0,strrpos($_SERVER['REQUEST_URI'],'/')));

define('URL_GCWEB',URLRACINE_GCWEB.'/'.DIR_GCWEB);

include (PATH_GCWEB.'inc/render.php');
?>
