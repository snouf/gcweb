README - GCWEB
========================

INSTALLATION
------------------------

Voir http://jonas.tuxfamily.org/wiki/gcweb


ASTUCE
-------------------------

Sous GNU/Linux pour synchroniser votre collection GCstar avec celle sous
GCWeb utilisez lftp. En ligne de commande :
  # lftp ftp://identifiant:mot_de_passe@site_de_connexion -e "mirror -e -R -x /emplacement_collection_local /emplacement_GCweb/collections ; quit"

Vous pouvez également programmer un cron ("crontab -e"), qui se chargera de mettre à jour (par exemple) quotidiennement votre liste sur votre site web. Et de vous débarasser ainsi de fastidieuses manipulations.
