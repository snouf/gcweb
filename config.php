<?php
/*
 *      this file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_runtime())
    set_magic_quotes_runtime(0);

define('PATH_GCWEB', dirname(__FILE__));
include PATH_GCWEB.'/inc/auth.php';
include PATH_GCWEB.'/conf/fieldstypes.php';

//Charger ancienne config
$fileconfig = dirname(__FILE__).'/conf/config.php';
$oldConf = readconfig($fileconfig);

//l10n
include dirname(__FILE__).'/inc/l10n.php';
if (isset($_POST['lang']))
    $conf['lang'] = $_POST['lang'];
elseif (isset($oldConf['lang']))
    $conf['lang'] = $oldConf['lang'];
else
    $conf['lang'] = 'eng';
load_l10n('config');

$error = '';
$warning = '';
$ok = "<code style='color:green'>ok</code>";
$ko = "<code style='color:red'>ko</code>";


/*** FONCTION ***/

function checkUserPasswd() {
    /*
     * Vérification du mot de passe
     */

    global $oldconf, $error;

    if ($oldconf) {
        //Verif user/pass
        if ( !auth::check_user($_POST['user4save'],$oldconf['user'])
           | !auth::check_password($_POST['password4save'],$oldconf['password'])) {
                $error .= '<p class="ko">'.__('Utilisateur ou mot de passe incorrect').'</p>';
                return False;
        } else {
            return True;
        }
    } elseif ($_POST['user'] == '' | $_POST['password'] == '') {
        //Création initiale du fichier de config, vérifie que l'utilisateur ait entré un user et un pass
        $error .= '<p class="ko">'.__('Le nom d\'utilisateur et le mot de passe sont obligatoires').'</p>';
        return False;
    } else {
        return True;
    }
}

function readconfig($fileconfig) {
    /*
     * Lit le fichier de config et le retourne. Si le fichier de config
     * n'existe pas retourne False
     */
    global $oldconf;

    if (!file_exists($fileconfig)) {
        $oldconf = False;
        return False;
    } else {
        include $fileconfig;
        $oldconf = $conf;
        return $conf;
    }
}

function writeconfig($fileconfig) {
    /*
     * Ecrit le fichier de config.
     */

    global $error, $oldconf, $nbcollec;

    if ((file_exists($fileconfig) & !is_writable($fileconfig)) | !is_writable(dirname(__FILE__).'/conf/')) {
        $error .= '<p class="ko">'.__('Le fichier <code>/conf/config.php</code> et/ou son
            dossier <code>/conf</code> n\'est pas accessible en écriture. Changez les droits
            ou créez manuellement ce fichier en vous basant sur <code>/conf/config.example.php</code>').'</p>';
        return False;
    } else {
       //mot de passe
        if (!isset($_POST['password']))     $password = $oldconf['password'];
        elseif ($_POST['password'] == '')   $password = $oldconf['password'];
        else                                $password = auth::set_password($_POST['password']);


        $newfileconfig = "<?php
            /*****************************************************
             * Fichier de configuration généré automatiquement *
             *****************************************************/

            \$conf = array(
                /*** CONFIGURATION DU SITE ***/
                'title'         => ".checktype($_POST['title']).",
                'user'          => ".checktype($_POST['user']).",
                'password'      => '$password',
                'description'   => ".checktype($_POST['description']).",
                'template'      => ".checktype($_POST['template']).",
                'lang'          => ".checktype($_POST['lang'],'string').",
                'itemsPage'     => ".checktype($_POST['itemsPage'],'integer').",
                'champVide'     => ".checktype($_POST['champVide']).",
                'noIE'          => ".checktype($_POST['noIE'],'boolean').",
                'noIEtext'      => ".checktype($_POST['noIEtext'],'string').",
                'generator'     => ".checktype($_POST['generator'],'boolean').",

                /*** CONFIGURATION DES COLLECTIONS ***/
                'collections' => array(
        ";

        $collections = array();
        $nopurgeCacheBDD = array();
        for ($i = 1; $i <= $_POST['nbcollec']; $i++) {
            if (!isset($_POST["isdel_collec$i"])) {
                //Type de collection
                $xmlfile = dirname(__FILE__).'/collections/'.$_POST["xml_collec$i"];
                if (!$xml = simplexml_load_file($xmlfile)) {
                    $error .= "Impossible de charger le fichier de sauvegarde $xmlfile.";
                    return False;
                }
                $attrs = $xml->attributes();
                $type = $attrs['type'];

                $collections[] = "
                    /* collection $i */ array(
                        'title'       => ".checktype($_POST["title_collec$i"],'string').",
                        'dir'         => '', //non utilisable avec le générateur automatique
                        'xml'         => ".checktype($_POST["xml_collec$i"],'string').",
                        'picturesdir' => ".checktype($_POST["picturesdir_collec$i"],'string').",
                        'description' => ".checktype($_POST["description_collec$i"],'string').",
                        'type'        => '$type',
                        'sortBy'      => ".checktype($_POST["sortBy_collec$i"],'string').",
                        'private'     => ".checktype($_POST["private_collec$i"],'boolean')."
                    )";
                array_push($nopurgeCacheBDD,
                        md5(($i-1).'none'.substr(checktype($_POST["sortBy_collec$i"],'string'),1,-1)).'_0-end' #tri par defaut mis en cache sans purge
                    );
            }
        }
        $newfileconfig .= join(",\n", $collections)."
                ),";

        $newfileconfig .= "
                /*** PARAMETRES AVANCÉS ***/
                'noCacheBDD'        => ".checktype($_POST['noCacheBDD'],'boolean').",
                'purgeCacheBDDDelay'=> ".checktype($_POST['purgeCacheBDDDelay'],'integer').",
                'purgeCacheBDDAge'  => ".checktype($_POST['purgeCacheBDDAge'],'integer').",
                'nopurgeCacheBDD'   => array('".join("','",$nopurgeCacheBDD)."'),
                'jpgQuality'        => ".checktype($_POST['jpgQuality'],'integer').",
                'GDresampled'       => ".checktype($_POST['GDresampled'],'boolean').",
                'noCacheImg'        => ".checktype($_POST['noCacheImg'],'boolean').",
                'purgeCacheImgDelay'=> ".checktype($_POST['purgeCacheImgDelay'],'integer').",
                'purgeCacheImgAge'  => ".checktype($_POST['purgeCacheImgAge'],'integer').",
                'ignoreString4sort' => ".checktype($_POST['ignoreString4sort'],'array').",
                'fomatDate'        => ".checktype($_POST['fomatDate'],'string')."
            );

            @include 'config.complement.php';

            setlocale (LC_ALL,".checktype($_POST['local'],'string').");
            date_default_timezone_set(".checktype($_POST['timezone'],'string').");
            ?>";

        if ($error == '') {
            ini_set('track_errors',True);
            eval('$newfileconfig;');
            if (isset($php_errormsg)) {
                $error = '<p class="ko">'.__('Erreur dans le fichier de configuration').' :<br />
                    <code>'.$php_errormsg.'</code><br />'.
                    __('Enregistrement impossible')."</p>\n";
                return False;
            }
        }


        if ($error == '') {
            //pas d'erreur, on ecrit le fichier
            $fp = fopen($fileconfig,'w');
            fwrite($fp, /*str_replace('\\','\\',*/$newfileconfig);
            fclose($fp);
            $error = '<p class="ok">'.__('Le fichier de configuration a été enregistré').'</p>';
            return True;
        } else {
            //erreur, on écrit pas le fichier et signale l'erreur
            $error .= '<p class="ko">'.__('Le fichier de configuration n\'a pas été enregistré').'</p>';
            return False;
        }
    }
}

function checktype($var,$type='string') {
    /*
     * Verifie si le type des variables est correct.
     *    * Si correct retourne la chaîne (sans ajout de backslach
     *    * Si il n'est pas correct retourne une chaîne vide et ajoute l'erreur
     *      dans la variable global $error
     */
    global $error;

    if (!(version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc()))
        $var = addslashes($var);

    switch ($type) {
        case 'string' :
            if (!is_string($var)) {
                $error .= '<p class="ko">"<code>'.$var.'</code>" '.__('n\'est pas une chaîne')."</p>\n";
                return "''";
            } else {
                $var = str_replace('\"','"',"'$var'");
            }
            break;
        case 'integer' :
            if (!is_numeric($var)) {
                $error .= '<p class="ko">"<code>'.$var.'</code>" '.__('n\'est pas un chiffre')."</p>\n";
                return '1';
            }
            break;
        case 'boolean' :
            if (!in_array($var,array('True','False'))) {
                $error .= '<p class="ko">"<code>'.$var.'</code>" '.__('n\'est pas un vrai/faux')."</p>\n";
                return 'False';
            }
            break;
        case 'array' :
            $var = stripslashes($var);
            eval('$array = array($var);');
            if (!is_array($array)) {
                $error .= '<p class="ko">"<code>'.$var.'</code>" '.__('n\'est pas une liste au correctement formatée')."</p>\n";
                return 'array()';
            } else {
                $var = "array($var)";
            }
            break;
        default :
             $error .= '<p class="ko">"<code>'.$var.'</code>" '.__('n\'est pas d\'un type connu')."</p>\n";
             return '""';
    }
    return $var;
}

function input_type_texte ($name, $label='', $default='') {
    /*
     * Afficher un champ de type texte
     */
    global $oldConf;

    $return = '';

    if (isset($_POST[$name])) {
        if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
             $value = stripslashes($_POST[$name]);
        else $value = $_POST[$name];
    } elseif (isset($oldConf[$name])) {
        if (is_array($oldConf[$name]))  {
            $tmparrayvalue = array();
            foreach ($oldConf[$name] as $partvalue)
                $tmparrayvalue[] = '\''.addslashes($partvalue).'\'';
            $value = join(',',$tmparrayvalue);
        }
        else {
            $value = $oldConf[$name];
        }
    } else {
        $value = $default;
    }


    if ($label != '')
        $return .= "<label for='$name'>$label</label>";
    $return .=  "<input id='$name' style='width:None' type='text' size='16' value=\"".htmlspecialchars($value)."\" name='$name' />";

    return $return;
}

function input_select($name, $options, $labels=False, $label=' ', $value='') {
    /*
     * Affiche un liste de selection
     */
    global $oldConf;

    if (isset($_POST[$name]))       $value = $_POST[$name];
    elseif (isset($oldConf[$name])) $value = $oldConf[$name];

    $return = "\n<label for='$name'>$label</label><select id='$name' name='$name'>\n";
    foreach ($options as $option) {
        if ($option == $value)  $selected = " selected='selected'";
        else                    $selected = "";
        if ($labels == False)   $label = $option;
        $return .= "    <option value='$option'$selected>".$label."</option>\n";
    }
    $return .="</select>";
    return $return;
}

function input_checkbox($name, $label, $default=False) {
    /*
     * case à choché
     */
    global $oldConf;

    if (isset($_POST[$name])) {
        if ($_POST[$name]=='True')  $value = True;
        else                        $value = False;
    }
    elseif (isset($oldConf[$name])) $value = $oldConf[$name];
    else                            $value = $default;

    if ($value)
        return "<label for='$name'>$label</label><input type='hidden' name='$name' value='False' /><input id='$name' type='checkbox' name='$name' value='True' checked='checked' />";
    else
        return "<label for='$name'>$label</label><input type='hidden' name='$name' value='False' /><input id='$name' type='checkbox' name='$name' value='True' />";
}

function textarea($name, $label='') {
    global $oldConf;
    $return = '';
    if (isset($_POST[$name]))       $value = $_POST[$name];
    elseif (isset($oldConf[$name])) $value = $oldConf[$name];
    else                            $value = '';

    $return .= "<textarea id='$name' name='$name' cols='40' rows='10'>".htmlspecialchars($value)."</textarea>";

    if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_gpc())
        $return = stripslashes($return);
    return $return;
}

function majplugins() {
    $render_plugins = array();
    $bdd_plugins = array();
    $item_plugins = array();
    $aftercache_plugins = array();
    $config_plugins = array();

    foreach (scandir(dirname(__FILE__).'/plugins') as $plugin) {
        if (substr($plugin,0,6) == 'render')
            $render_plugins[] = 'include PATH_GCWEB.\'/plugins/'.$plugin.'\';';
        elseif (substr($plugin,0,3) == 'bdd')
            $bdd_plugins[] = 'include PATH_GCWEB.\'/plugins/'.$plugin.'\';';
        elseif (substr($plugin,0,4) == 'item')
            $item_plugins[] = 'include PATH_GCWEB.\'/plugins/'.$plugin.'\';';
        elseif (substr($plugin,0,10) == 'aftercache')
            $aftercache_plugins[] = 'include PATH_GCWEB.\'/plugins/'.$plugin.'\';';
        elseif (substr($plugin,0,6) == 'config')
            $config_plugins[] = 'include PATH_GCWEB.\'/plugins/'.$plugin.'\';';
    }

    $fp = fopen(dirname(__FILE__).'/conf/plugins4render.php','w');
    fwrite($fp, "<?php\n".join("\n",$render_plugins)."\n?>");
    fclose($fp);

    $fp = fopen(dirname(__FILE__).'/conf/plugins4bdd.php','w');
    fwrite($fp, "<?php\n".join("\n",$bdd_plugins)."\n?>");
    fclose($fp);

    $fp = fopen(dirname(__FILE__).'/conf/plugins4item.php','w');
    fwrite($fp, "<?php\n".join("\n",$item_plugins)."\n?>");
    fclose($fp);

    $fp = fopen(dirname(__FILE__).'/conf/plugins4aftercache.php','w');
    fwrite($fp, "<?php\n".join("\n",$aftercache_plugins)."\n?>");
    fclose($fp);

    $fp = fopen(dirname(__FILE__).'/conf/plugins4config.php','w');
    fwrite($fp, "<?php\n".join("\n",$config_plugins)."\n?>");
    fclose($fp);
}


if (!function_exists('scandir')) {
    function scandir($dir) {return array();}
    $php5 = False;
} else {
    $php5 = True;
}

/*** end function ***/

/*** ACTIONS *********************************************************/

//Condition d'utilisation
if (isset($_POST['iagree'])) {
    if ($_POST['iagree'] == 'i_am_responsible_for_information_put_online')
        rename(dirname(__FILE__).'/conf/idontagree.txt',dirname(__FILE__).'/conf/iagree.txt');
}

//Sauvegarder le fichier
$checkUserPasswd = False;
if (isset($_POST['save'])) {
    $checkUserPasswd = checkUserPasswd();
    if ($checkUserPasswd) {
        //Sauvegarde fichier config
        $writeconfigOK = writeconfig($fileconfig);
        //Gestion purge fichier cache
        if ($_POST['purgeCacheBDD'] == 'True' | $_POST['purgeCacheImg'] == 'True') {
            if ($_POST['purgeCacheBDD'] == 'True') {
                $error .= '<p class="ok">'.__('Fichiers de mise en cache des pages suprimées');
                $forcePurgeCacheBDD = True;
            }
            if ($_POST['purgeCacheImg'] == 'True') {
                $error .= '<p class="ok">'.__('Fichiers de mise en cache des images suprimées');
                $forcePurgeCacheImg = True;
            }
            $conf = $oldConf;
            include dirname(__FILE__).'/inc/purgecache.php';
            unset($conf);
        }

        include PATH_GCWEB.'/inc/convGCmodels.php';
        majplugins(); // majplugin
        #renome la page
        if (isset($_POST['new_name_page'])) {
            if (!is_writable(dirname(__FILE__))) { echo '
                    <li class="ko">'.__('Le dossier racine n\'est pas inscriptible le renomage de
                    <code>config.php</code> ne peux être effectué. Renommez ce fichier à l\'aide de
                    votre client ftp.').'
                    </li>';
            } elseif (preg_match('`^[-a-zA-Z0-9_]{1,}$`i',$_POST['new_name_page'])) {
                rename(dirname(__FILE__).'/config.php',dirname(__FILE__).'/'.$_POST['new_name_page'].'.php');
                sleep(1);
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                $extra = $_POST['new_name_page'].'.php';
                header("Location: http://$host$uri/$extra");
                exit;
            } else {
                $warning .= '<p class="warning">'.__('Attention : nom de page invalide, renommage abandonné').'</p>';
            }
        }

        if ($writeconfigOK) {
            //L'écriture du fichier de config est ok, lecture de la config
            $oldConf = readconfig($fileconfig);
            unset ($_POST); // suppression des données post pour que ce soit celle de oldConf qui soit utilisée
        }
    }
}


//liste des thèmes
$templates = array();
foreach (scandir(dirname(__FILE__).'/templates') as $file) {
    if ($file[0] != '.') {
        if (is_dir(dirname(__FILE__).'/templates/'.$file))
            $templates[] = $file;
    }
}

//liste dans langues
$langs = array();
foreach (scandir(dirname(__FILE__).'/locales') as $file) {
    if ($file[0] != '.' && $file != 'potfiles') {
        if (is_dir(dirname(__FILE__).'/locales/'.$file) )
            $langs[] = $file;
    }
}

//liste des dossiers image
$picturesdirs = array();
foreach (scandir(dirname(__FILE__).'/collections') as $file) {
    if ($file != '.' & $file != '..' & is_dir(dirname(__FILE__).'/collections/'.$file))
            $picturesdirs[] = $file;
}

//liste des fichiers de sauvegarde
$xmlFiles = array();
foreach (scandir(dirname(__FILE__).'/collections') as $file) {
    if ($file != '.' & $file != '..' & is_file(dirname(__FILE__).'/collections/'.$file))
            $xmlFiles[] = $file;
}

//nombre de collection
if (isset($_POST['nbcollec'])) {
    if (isset($_POST['addcollec']))
        $nbcollec = $_POST['nbcollec'] + 1;
    else
        $nbcollec = $_POST['nbcollec'];
} elseif ($oldConf) {
    $nbcollec = count($oldConf['collections']);
} else {
    $nbcollec = 1;
}


/*** AFFICHAGE ***/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
    <title><?php echo __('Configuration de GCWeb') ?></title>
    <meta http-equiv="Content-Type" content="application/x-php;charset=UTF-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <style type="text/css">
        body {
            font-family: sans-serif;
            font-size: 9pt;
        }
        fieldset {
            clear: both;
            margin-top : 2em;
        }
        label {
            clear: both;
            display:block;
            float:left;
            min-width:17em;
            text-align:right;
            padding-right : .5em;
            margin-top : .1em;
        }
        input, select {
            font-family: sans-serif;
            font-size: 9pt;
            float:left;
            margin: -.2em 0 1em 0em;
        }
        textarea {
            display:block;
            margin: 0 0 1.5em 0em;
            width: 40em;
            height : 7em;
        }
        span {
            min-height: 1.5em;
            margin: .6em 0 1.5em 31em;
            display:block;
        }
        .ok {   color: green;   }
        .ko {   color: red;     }
        .warning {   color: orange;     }
    </style>
    <script type="text/javascript">
        function stopRKey(evt) {
          var evt = (evt) ? evt : ((event) ? event : null);
          var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
          if ((evt.keyCode == 13) && ((node.type=="text") || (node.type=="password")))  {return false;}
        }

        document.onkeypress = stopRKey;
    </script>
</head>
<body onload="stopRKey()">
    <div>
        <h1><?php echo __('Configuration de GCWeb') ?></h1>

        <p>
        [ <a href="./?redirect=index.php"><?php echo __('Vos collections') ?></a>
        | <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb"><?php echo __('wiki de GCweb') ?></a>
        | <a href="./?redirect=http%3A%2F%2Fgcweb.web4me.fr%2Fdemo%3Fcollec%3D0%26model%3Dlist"><?php echo __('thèmes pour GCweb','Site en français uniquement') ?></a>
        | <a href="./?redirect=http%3A%2F%2Fgcweb.web4me.fr%2Fdemo%3Fcollec%3D1%26model%3Dlist"><?php echo __('plugins pour GCweb','Site en français uniquement') ?></a>
        | <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fblog%2Findex.php%3Ftag%2FGCweb"><?php echo __('actualités de GCweb','Site en français uniquement') ?></a>
        ]</p>

        <?php echo $error.$warning ?>


        <form method='post' action='<?php echo $_SERVER['PHP_SELF']; ?>'>

        <p><?php echo __('Bienvenue dans la page de configuration de GCweb.') ?></p>

        <?php
        if (function_exists('gd_info')) {
            $gd_info = gd_info();
            $gdsupportjpg = ((isset($gd_info['JPEG Support']) ? $gd_info['JPEG Support'] : $gd_info['JPG Support']) ? $ok : $ko );
            $gdsupportpng = ($gd_info['PNG Support'] ? $ok : $ko);
            $gdsupportgif = (($gd_info['GIF Read Support']) && ($gd_info['GIF Create Support']) ? $ok : $ko );
        } else {
            $gd_info = False;
        }

        if (!file_exists(dirname(__FILE__).'/conf/iagree.txt' ))
            echo '
            <div style="border:red 1px solid; background:#FFDBDB; padding: 1em; width:80%; margin:auto">
                '.__('<h3>Avertissement</h3>

                <p>GCstar récupère des informations sur des sites internet divers, ce qui pour une
                utilisation privée ne pose généralement pas de problème. Cependant, en affichant
                ces informations sur votre site web, GCweb rend publiques ces informations.</p>

                <p>La personne installant GCweb doit vérifier que les informations mises en ligne
                ne sont pas protégées par des règles sur la propriété intellectuelle interdisant
                ce type d\'utilisation.</p>

                <p>Note : avec les thèmes officiels les sources sont citées par l\'intermédiaire du lien "sources".
                Cependant, avec des thèmes alternatifs, cette information peut être masquée.</p>').'

                <p style="position:relative">
                <input style="display:inline; float: none;" type="checkbox" name="iagree" id="iagree" value="i_am_responsible_for_information_put_online" />
                <label style="display:inline; float: none;" for="iagree">
                '.__('En cochant cette case vous attestez avoir pris connaissance de ces informations').'.</label>
                </p>
            </div>';

        echo '
            <fieldset><legend>'.__('Prérequis').'</legend>
                <ul>';
        if ($php5) echo '
                    <li>'.__('Version de PHP (5 ou plus)').' : '.$ok.' ('.__('votre version est').' "'.phpversion().'")</li>';
        else '
                    <li>'.__('Version de PHP (5 ou plus)').' : '.$ok.' ('.__('votre version est').' "'.phpversion().'"). '.__('Consultez
                    la documentation de votre hébergeur, vous pouvez probablement
                    activer PHP5').'</li>';

        if (version_compare(PHP_VERSION, '7.4.0', '<') && get_magic_quotes_runtime())
                    echo '
                    <li class="ko">'.__('Les "magic_quotes_runtime()" semble être activé et leur désactivation
                    est impossible. Le générateur de fichier de configuration (cette page) et le générateur
                    de thème ne fonctionneront pas mais les autres fonctionnalités (utilisation normale) ne
                    poseront pas de problème. Pour créer vos fichier de configuration consultez
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Ffichier_de_configuration">
                        la documentation
                    </a>.').'</li>';

        if ($gd_info) echo '
                    <li>'.__('Les bibliotèques GD sont présentes et permettront de redimensionner les images de type')." :
                    JPEG $gdsupportjpg, PNG : $gdsupportpng, GIF : $gdsupportgif.</li>";
        else echo '
                    <li class="ko">'.__('Les bibliotèques GD sont absentes, GCweb ne pourra pas fonctionner').'.';

        if (!is_writable(dirname(__FILE__).'/conf')) echo '
                    <li class="ko">'.__('Le dossier "<code>/conf</code>" n\'est pas inscriptibles. Le fichier de
                    configuration ne pourra pas être enregistré').'.</li>';

        if (   !is_writable(dirname(__FILE__).'/conf/fieldstypes.php')
             | (file_exists(dirname(__FILE__).'/conf/idontagree.txt') ? !is_writable(dirname(__FILE__).'/conf/idontagree.txt') : False )
            ) echo '
                    <li class="ko">'.__('L\'un ou plusieurs de ces fichiers ne sont pas inscriptible :').'
                    <code>/conf/fieldstypes.php</code>, <code>/conf/idontagree.txt</code>.</li>';

        if (   !is_writable(dirname(__FILE__).'/cache')
             | !is_writable(dirname(__FILE__).'/cache/bdd')
             | !is_writable(dirname(__FILE__).'/cache/images')
            ) echo '
                    <li class="ko">'.__('Le dossiers "<code>/cache</code>" et ses sous-dossiers doivent être inscriptible sinon GCweb
                    ne pourra pas fonctionner (sauf si vous désactivé le complêtement le cache mais ceci est déconseillé)').'.
                    </li>';

        if (empty($templates)) echo '
                    <li class="ko">'.__('Aucun fichier de thème n\'a été trouvé').'.</li>';

        if (empty($xmlFiles)) echo '
                    <li class="ko">'.__('Aucun fichier de sauvegarde GCstar n\'a été trouvé.
                    Envoyez votre/vos fichier/s de sauvergarde dans le dossier
                    <code>/collections</code> à l\'aide de votre client ftp favori').'</li>';

        if (empty($picturesdirs)) echo '
                    <li class="ko">'.__('Aucun dossier d\'images n\'a été trouvé, envoyez votre/vos
                    <strong>dossier/s</strong> contenant les images de votre/vos collection/s
                    dans le dossier <code>/collections</code> à l\'aide de votre client ftp
                    favori').'.</li>';

        if ($oldConf) {
            foreach ($oldConf['collections'] as $collectmp) {
                if (!in_array($collectmp['type'], array_keys($fieldstypes))){
                    echo '
                        <li class="ko">';
                    printf(__('Attention : Le model de collection %s est inconnu de GCweb. Veuillez copier <a href="./?redirect=http%%3A%%2F%%2Fwiki.gcstar.org%%2Ffr%%2Fuser_models"> modèle de collection GCstar</a> dans le dossier <code>/conf/GCmodels/</code> de GCweb et réenregistrer cette page.'),$collectmp['type']);
                    echo '</li>';
                }
            }
        }

        echo '
                </ul>

                <p><a href="#" onclick="javascript:location.reload()">'.__('Revérifier').'</a></p>
            </fieldset>
            ';

        if (is_file(dirname(__FILE__).'/config.php')) echo '
            <fieldset><legend class="ko">'.__('Fichier de configuration consultable').'</legend>
                '.__('<p>Un fichier <code>config.php</code> se trouve à la racine de
                l\'installation de GCweb. Afin d\'empêcher la consultation de cette page et
                d\'augmenter la sécurité de GCweb, il est conseillé de renommer ce fichier.
                Cette page sera ensuite consultable via l\'url
                <code>http://votre_site.tdl/gcweb/nom_du_fichier.php</code>.<br />
                (Notez que pour encore plus de sécurité vous pouvez supprimer ce fichier
                une fois la configuration terminée).</p>').'

                <label for="new_name_page">'.__('Nouveau nom').'</label>'.
                (is_writable(dirname(__FILE__))
                  ? '<input id="new_name_page" type="text" size="16" name="new_name_page" />
                    <span>'.__('Entrez le nom la page <strong>sans</strong> l\'extention <code>.php</code> et
                    <strong>sans</strong> caractères spéciaux').'.</span>'
                  : '<span>'.__('Le dossier racine n\'est pas inscriptible le renomage de
                    <code>config.php</code> ne peux être effectué. Renommez ce fichier à l\'aide de
                    votre client ftp.').'</span>'
                );

        echo '
            </fieldset>


            <fieldset><legend>'.__('Général','titre de section').'</legend>
                '.input_type_texte('title',__('Titre du site')).'
                    <span></span>
                <label for="description">'.__('Description').'</label>
                <label>('.__('Code html possible').')</label>'
                    .textarea('description').'
                '.input_type_texte('user',__('Utilisateur')).'
                    <span></span>
                <label for="password">'.__('Mot de passe').'</label><input id="password" size="16" type="password" name="password" />
                    <span>'.__('laissez-le vide pour ne pas le changer').'.</span>
                '.input_select('lang',$langs,False,__('Langue de l\'interface'),$conf['lang']).'
                    <span> </span>
                '.input_select('template',$templates,False,__('Thème'),'default').'
                    <span><a href="./?redirect=http%3A%2F%2Fgcweb.web4me.fr%2Fdemo%3Fcollec%3D0%26model%3Dlist">'.__('Visionner et télécharger des thèmes','Site en français uniquement').'</a>
</span>
                '.input_type_texte('itemsPage',__('Nombre d\'éléments par page'),10).'
                    <span> </span>
                '.input_type_texte('champVide',__('Champs vide'),'-').'
                    <span>'.__('Quand un élément n\'a pas été complété dans GCstar. GCweb le remplacera
                    par cette chaîne').'.</span>
                '.input_checkbox('noIE',__('Non à MSIE'),True).'
                    <span>'.__('Affiche un message sur internet explorer précisant que
                    ce navigateur respecte très mal les standards du web et qu\'il est possible que les
                    pages du site s\'affichent mal. Vous pouvez remplacer le message par défaut en
                    rédigeant votre propre message ci-dessous').'.</span>
                <label for="noIEtext">'.__('Texte personnalisé NonMSIE').'</label>
                <label>('.__('Code html possible').')</label>
                    '.textarea('noIEtext').'
                '.input_checkbox('generator',__('Générateur'),False).'
                    <span>'.__('Permet de créer les pages de thème pour un type de collection non
                    supportée ou d\'en remplacer une qui ne vous plait pas
                    (<a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fconfiguration%23Ordre_de_tri+par_defaut">plus d\'information</a>).
                    Activez cette fonctionnalité uniquement quand vous en avez
                    l\'utilité').'.</span>
            </fieldset>
            ';


        //Liste des collections

        for ($i = 1; $i <= $nbcollec ; $i++)
        {
            if (isset($oldConf['collections'][$i-1])) {
                $collec = $oldConf['collections'][$i-1];
                $oldConf = array_merge($oldConf, array(
                    "title_collec$i"        => $collec['title'],
                    "description_collec$i"  => $collec['description'],
                    "xml_collec$i"          => $collec['xml'],
                    "private_collec$i"      => $collec['private'],
                    "picturesdir_collec$i"  => $collec['picturesdir'],
                    "sortBy_collec$i"       => $collec['sortBy']
                ));
            }
            if (    !isset($_POST['save'])
                    & (
                        isset($_POST["del_collec$i"])
                        | (isset($_POST["isdel_collec$i"]) & !isset($_POST["canceldel_collec$i"]))
                    )
                )
                $delmesg = '
                    <p class="ko">'.__('Cette collection serra supprimée lors de l\'enregistrement')."</p>
                    <input type='hidden' name='isdel_collec$i' value='delete' />
                    <input type='submit' name='canceldel_collec$i' value='".__('Annuler la suppression')."' /><span> </span>";
            else
                $delmesg = "
                    <input style='margin-right:.5em' type='submit' name='del_collec$i' value='".__('Supprimer cette collection')."' />
                        <span style='margin-left:0'>".__('La collection ne serra plus affichée par GCweb mais
                        les fichiers composant celle-ci ne seront pas supprimés').".</span>";

            echo "
                <fieldset><legend>".__('Collection')." $i/$nbcollec</legend>
                    $delmesg
                    ".input_type_texte("title_collec$i",__('Titre de la collection'))."
                        <span> </span>
                    <label for='description_collec$i'>".__('Description')."<br />(".__('Code html possible').")</label>
                        ".textarea("description_collec$i")."
                    ".input_select("xml_collec$i",$xmlFiles,False,__('Nom du fichier de sauvegarde'))."
                        <span> </span>
                    ".input_checkbox("private_collec$i",__('Collection privée'))."
                        <span>".__('Si cochée, la collection sera cachée mais toujours accessible via','...')."
                        <a href='./?redirect=index.php%3Fcollec%3D".($i-1)."%26amp%3Bmodel%3Dlist'>".__('son url direct')."</a>.
                        ".__('Attention, les visiteurs peuvent deviner très facilement cette url !')."</span>
                    ".input_select("picturesdir_collec$i",$picturesdirs,False,__('Dossier contenant les images'))."
                        <span></span>
                    ".input_type_texte("sortBy_collec$i",__('Ordre de tri par défaut'),'addedDSC')."
                        <span>".__('idASC fonctionnera avec tous les types de collection et correspond
                        à l\'ordre dans lequel vous avez ajouté les éléments. Utilisez
                        idDSC pour les avoir dans l\'ordre inverse. Il est égallement possible de les trier
                        selon <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fgenerateur_de_theme">
                        un ou plusieurs autres champs</a>').".</span>
                </fieldset>
                ";
        }
        ?>

        <div>
            <input type="hidden" name="nbcollec" value="<?php echo $nbcollec ?>" />
            <input type="submit" name="addcollec" value="<?php echo __('Ajouter une collection') ?>" />
        </div>

        <?php
        echo
            "<fieldset><legend>".__('Avancée','titre de section')."</legend>
                <p>".__('Paramètres avancés de GCweb. Si vous ne savez pas ce que vous faites n\'y touchez pas !')."</p>

                <h3>".__('Paramètres de la mise en cache de la base de donnée')."</h3>
                <p>".__('Le traitement (filtrage et organisation) du fichier d\'enregistrement
                est une opération lourde. Afin d\'éviter que GCweb  nerefasse ce travail quand l\'utilisateur
                passe à la page suivante, la recherche est mise en cache').".</p>
                ".input_type_texte('purgeCacheBDDDelay',__('Purger automatique tous les','...'),'60')."
                    <span>".__('minutes les fichiers de cache de la base de donnée qui non pas servis depuis le
                    nombre de minutes spécifié ci-dessous').".</span>
                ".input_type_texte('purgeCacheBDDAge',__('Les fichiers non utilisés depuis'),'60')."
                    <span>".__('minutes')."</span>
                ".input_checkbox('noCacheBDD',__('Ne pas utiliser de cache'),False)."
                    <span>".__('Cocher cette case est déconseillé (économise l\'espace disque
                    mais consomme plus de ressources et affiche moins rapidement les pages)').".
                    </span>

                <h3>".__('Paramètres des images')."</h3>
                ".input_type_texte('jpgQuality',__('Qualitée des jpeg'),80)."
                    <span>".__('De 0 - qualité médiocre et poids très faible - à 100 - excellente
                    qualitée, images lourdes').".</span>
                ".input_checkbox('noCacheImg',__('Ne pas mettre en cache'),False)."
                    <span>".__('Si activé, les images ne seront pas mises en cache. Il est fortement
                    déconseillé de cocher cette case. Vous économiserez de l\'espace-disque, mais
                    les ressources serveur seront très sollicitées et les images s\'afficheront très lentement,
                    voire partiellement').".</span>
                ".input_type_texte('purgeCacheImgDelay',__('Purge automatique tous les'),'30')."
                    <span>".__('jours')."</span>
                ".input_type_texte('purgeCacheImgAge',__('Les images non utilisées depuis'),'60')."
                    <span>".__('jours')."</span>
                ".input_checkbox('GDresampled',__('Rééchantillonner les images'),True)."
                    <span>".__('Si activé, crée des images de meilleure qualité.
                    Cependant cette fonction n\'est pas disponible chez tous
                    les hébergeurs. Désactivez cette option si la création des images ne
                    fonctionne pas ou est trop lente').".</span>

                <h3>".__('Paramètres divers')."</h3>
                ".input_type_texte('ignoreString4sort',__('Chaîne à ignorer'),"'le ', 'la ','l\'', 'les ', 'un ', 'une ', 'des ','a ', 'the ','der ','die ', 'das ', 'ein ','eine ','el ','los ','una ','tome ','et ','&',',','.',':',';','-','_',' '")."
                    <span>".__('Liste des chaînes à ignorer lors du tri. ATTENTION
                    respectez rigoureusement la syntaxe (chaîne entre guillemet simple
                    séparée par une virgule et n\'oubliez pas de mettre l\'espace qui suit le déterminant
                    (n\'écrivez pas "<code>le</code>" mais écrivez "<code>le </code>",
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fconfiguration_avancee%23chaines_a_ignoree">plus d\'information</a>).')."
                    </span>
                ".input_type_texte('local',__('Localisation'),__('fr_FR.UTF8','code de localisation cf http://fr.php.net/manual/fr/function.setlocale.php'))."
                    <span>".__('Tapez le code de localisation (encodé en UTF-8) de votre pays. Ce code dépend du
                    système d\'exploitation sur lequel tourne le serveur de votre site web.
                    Vous pouvez laisser ce champs vide pour utiliser la paramêtre pas défaut
                    de votre serveur (<a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ffunction.setlocale.php">plus d\'information</a>).')."</span>
                ".input_type_texte('timezone',__('Fuseau horaire'),__('Europe/Paris','fuseau horaire de votre pays cf http://fr.php.net/manual/fr/timezones.php'))."
                    <span>".__('Selon la <a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ftimezones.php">
                    liste des fuseaux horaires</a> supportée par PHP.')."</span>
                ".input_type_texte('fomatDate',__('Format des dates GCstar'),__('D/M/Y','format des dates dans GCstar D: jour, M: moi, Y: année séparateur toujours "/"'))."
                    <span>".__('GCstar entregistre la date des éléments sous forme de chaînes (jj/mm/aa par exemple).
                    Sur cette base, le tri par ordre chronologique est impossible. GCweb peut essayer d\'interpréter
                    ces dates.')."
                    </span>

                <h3>".__('Purge des fichiers de mise en cache')."</h3>
                <p>".__('Supprimera les fichiers de mise en cache lors de l\'enregistrement')."</p>
                <label for='purgeCacheBDD'>".__('De la base de donnée')."</label>
                    <input type='hidden' name='purgeCacheBDD' value='False' />
                    <input id='purgeCacheBDD' type='checkbox' name='purgeCacheBDD' value='True' />

                <label for='purgeCacheImg'>".__('Des images')."</label>
                    <input type='hidden' name='purgeCacheImg' value='False' />
                    <input id='purgeCacheImg' type='checkbox' name='purgeCacheImg' value='True' />
            </fieldset>";


        @include dirname(__FILE__).'/conf/plugins4config.php';

            ?>



        <fieldset><legend><?php echo __('Enregistrement', 'titre section') ?></legend>
            <?php
            if (file_exists($fileconfig))
               echo input_type_texte('user4save',__('Utilisateur'))."
                    <label>".__('Mot de passe')."</label><input size='16' type='password' name='password4save' />";
            ?>
            <input type="submit" name="save" value="<?php echo __('Sauver les paramètres') ?>" />
        </fieldset>
        </form>
    </div>
</body>
</html>
